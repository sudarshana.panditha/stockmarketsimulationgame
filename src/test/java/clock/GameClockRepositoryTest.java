package clock;

import clock.exception.GameClockNotFoundException;
import clock.exception.InvalidMaxNumOfTurnsException;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.Test;

import java.util.ArrayList;
import java.util.List;
import java.util.UUID;

import static org.junit.jupiter.api.Assertions.*;

class GameClockRepositoryTest {
    public static String gameId;
    public static List<String> playerIds;
    public static int maxNumOfTurns;

    @BeforeAll
    public static void setup(){
        gameId = UUID.randomUUID().toString(); // initialize pseudo game id
        playerIds = new ArrayList<>(); // initialize pseudo player ids
        playerIds.add(UUID.randomUUID().toString());
        playerIds.add(UUID.randomUUID().toString());
        playerIds.add(UUID.randomUUID().toString());
        playerIds.add(UUID.randomUUID().toString());
        maxNumOfTurns = 1;
    }

    @Test
    public void createGameClockTest() throws InvalidMaxNumOfTurnsException, GameClockNotFoundException {
        GameClockRepository gameClockRepository = new GameClockRepository();
        gameClockRepository.createGameClock(gameId,playerIds, maxNumOfTurns);
        assertNotNull(gameClockRepository.getGameClock(gameId));
    }

    @Test void getGameClockTest() throws InvalidMaxNumOfTurnsException, GameClockNotFoundException {
        GameClockRepository gameClockRepository = new GameClockRepository();
        gameClockRepository.createGameClock(gameId,playerIds, maxNumOfTurns);
        assertEquals(gameId, gameClockRepository.getGameClock(gameId).gameId);
    }

    @Test
    public void gameClockNotFoundTest(){
        GameClockRepository gameClockRepository = new GameClockRepository();
        assertThrows(
                GameClockNotFoundException.class,
                () -> gameClockRepository.getGameClock(gameId)
        );
    }
}