package clock;

import clock.exception.ExpiredTurnException;
import clock.exception.InvalidMaxNumOfTurnsException;
import clock.exception.InvalidTurnException;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.Test;

import java.util.ArrayList;
import java.util.List;
import java.util.Random;
import java.util.UUID;

import static org.junit.jupiter.api.Assertions.*;

class GameClockTest {
   public static String gameId;
   public static List<String> playerIds;
   public static int maxNumOfTurns;

   @BeforeAll
   public static void setup(){
       // set game id
       gameId = UUID.randomUUID().toString();
       // add four players
       playerIds = new ArrayList<>();
       playerIds.add(UUID.randomUUID().toString());
       playerIds.add(UUID.randomUUID().toString());
       playerIds.add(UUID.randomUUID().toString());
       playerIds.add(UUID.randomUUID().toString());
       // set max number of turns
       maxNumOfTurns = 2;
   }

   /*
   * This method tests whether when a new GameClock is created,
   * whether its initial current turn is equal to one
   * */
   @Test
   public void initialCurrentTurnTest() throws InvalidMaxNumOfTurnsException {
       GameClock gameClock = new GameClock(gameId,playerIds,maxNumOfTurns);

       assertEquals(1,gameClock.getCurrentTurn());
   }

   /*
   * This method tests whether when an illegal number of maxNumOfTurns
   * is supplied, such as a number that is less than or equal to zero,
   * then an InvalidMaxNumOfTurnsException is thrown
   * */
   @Test
   public void invalidMaxNumberOfTurnsTest(){
       assertThrows(
               InvalidMaxNumOfTurnsException.class,
               () -> {
                   new GameClock(gameId, playerIds, 0);
               }
       );
   }

   /*
   * This method tests whether when four  players complete their
   * current then the current turn is incremented by one
   * */
   @Test
   public void playerTurnsCompletedTest() throws InvalidMaxNumOfTurnsException, ExpiredTurnException, InvalidTurnException {
       GameClock gameClock = new GameClock(gameId,playerIds,maxNumOfTurns);
       int currentTurn = gameClock.getCurrentTurn();

       for (String playerId: playerIds) {
           gameClock.turnCompleted(playerId, currentTurn);
       }

       assertEquals(2, gameClock.getCurrentTurn());
   }

   /*
   * This method tests whether when a turnCompleted is called
   * for a turn that is older (i.e., less that current turn)
   * than the current turn, then an ExpiredTurnException is thrown
   * */
   @Test
   public void expiredTurnTest(){
       assertThrows(
               ExpiredTurnException.class,
               () -> {
                   GameClock gameClock = new GameClock(gameId,playerIds,maxNumOfTurns);
                   int currentTurn = gameClock.getCurrentTurn();

                   for (String playerId: playerIds) {
                       gameClock.turnCompleted(playerId, currentTurn);
                   }
                   Random random = new Random();
                   String playerId = playerIds.get(random.nextInt(playerIds.size()));
                   gameClock.turnCompleted(playerId, currentTurn);
               }
       );
   }

    /*
     * This method tests whether when a turnCompleted is called
     * for a turn that is greater than the current turn then an
     * InvalidTurnException is thrown
     * */
   @Test
   public void invalidTurnTest(){
       assertThrows(
               InvalidTurnException.class,
               () -> {
                   GameClock gameClock = new GameClock(gameId,playerIds,maxNumOfTurns);
                   int currentTurn = gameClock.getCurrentTurn();
                   Random random = new Random();
                   String playerId = playerIds.get(random.nextInt(playerIds.size()));
                   gameClock.turnCompleted(playerId, currentTurn+1);
               }
       );
   }

   /*
   * This method tests whether when all four players completes
   * the last turn of the game, then the currentTurn is not
   * incremented beyond the maxNumOfTurns
   * */
   @Test
   public void finalTurnTest() throws InvalidMaxNumOfTurnsException, ExpiredTurnException, InvalidTurnException {
       GameClock gameClock = new GameClock(gameId,playerIds,1);
       int currentTurn = gameClock.getCurrentTurn();
       for (String playerId: playerIds) {
           gameClock.turnCompleted(playerId, currentTurn);
       }
       assertEquals(1, gameClock.getCurrentTurn());
   }

    /*
     * This method tests whether when all four players completes
     * the last turn of the game, then a call to isGameClockExpired
     * returns true
     * */
   @Test
   public void gameClockIsExpiredTest() throws InvalidMaxNumOfTurnsException, ExpiredTurnException, InvalidTurnException {
       GameClock gameClock = new GameClock(gameId,playerIds,1);
       int currentTurn = gameClock.getCurrentTurn();
       for (String playerId: playerIds) {
           gameClock.turnCompleted(playerId, currentTurn);
       }
       assertEquals(true, gameClock.isGameClockExpired());
   }

    /*
     * This method tests whether when all four players completes
     * a turn that is not the last turn of the game, then a call
     * to isGameClockExpired returns false
     * */
    @Test
    public void gameClockIsNotExpiredTest() throws InvalidMaxNumOfTurnsException, ExpiredTurnException, InvalidTurnException {
        GameClock gameClock = new GameClock(gameId,playerIds,2);
        int currentTurn = gameClock.getCurrentTurn();
        for (String playerId: playerIds) {
            gameClock.turnCompleted(playerId, currentTurn);
        }
        assertEquals(false, gameClock.isGameClockExpired());
    }

    /*
     * This method tests whether when four  players complete their
     * current then call to isTurnCompleted returns true
     * */
    @Test
    public void playerTurnsCompletedIsTurnCompletedTest() throws InvalidMaxNumOfTurnsException, ExpiredTurnException, InvalidTurnException {
        GameClock gameClock = new GameClock(gameId,playerIds,maxNumOfTurns);
        int currentTurn = gameClock.getCurrentTurn();

        for (String playerId: playerIds) {
            gameClock.turnCompleted(playerId, currentTurn);
        }

        assertEquals(true, gameClock.isTurnCompleted());
    }

    /*
     * This method tests whether when four  players has completed their
     * current turn and then a player completes his/her next turn as well,
     * then a call to isTurnCompleted returns false
     * */
    @Test
    public void newTurnIsTurnCompletedTest() throws InvalidMaxNumOfTurnsException, ExpiredTurnException, InvalidTurnException {
        GameClock gameClock = new GameClock(gameId,playerIds,maxNumOfTurns);
        int currentTurn = gameClock.getCurrentTurn();

        for (String playerId: playerIds) {
            gameClock.turnCompleted(playerId, currentTurn);
        }
        // completes the next turn as well
        gameClock.turnCompleted(playerIds.get(0), gameClock.getCurrentTurn());

        assertEquals(false, gameClock.isTurnCompleted());
    }
}