package clock;

import akka.actor.ActorRef;
import akka.actor.ActorSystem;
import akka.actor.Props;
import akka.testkit.javadsl.TestKit;
import clock.exception.GameClockNotFoundException;
import clock.messages.*;
import market.messages.CurrentMarketTurn;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import java.time.Duration;
import java.util.ArrayList;
import java.util.List;
import java.util.UUID;

import static org.junit.jupiter.api.Assertions.*;

class ClockActorTest {
    public static ActorSystem system;
    public static ActorRef clockActor;
    public static TestKit marketProbe;
    public static GameClockRepository gameClockRepository;
    public static String gameId;
    public static List<String> playerIds;
    public static int oneTurn;
    public static int twoTurns;

    @BeforeAll
    public static void setup(){
        system = ActorSystem.create(); // create Actor system
        gameClockRepository = new GameClockRepository();
        marketProbe = new TestKit(system);
        Props clockActorProps = Props.create(ClockActor.class,gameClockRepository, marketProbe.getRef());
        clockActor = system.actorOf(clockActorProps);
        oneTurn = 1;
        twoTurns = 2;
    }

    @BeforeEach
    public void initGameClockData(){
        gameId = UUID.randomUUID().toString(); // initialize pseudo game id
        playerIds = new ArrayList<>(); // initialize pseudo player ids
        playerIds.add(UUID.randomUUID().toString());
        playerIds.add(UUID.randomUUID().toString());
        playerIds.add(UUID.randomUUID().toString());
        playerIds.add(UUID.randomUUID().toString());
    }

    @Test
    public void createGameClockTest() throws InterruptedException, GameClockNotFoundException {
        clockActor.tell(new CreateGameClock(gameId,playerIds, oneTurn), null);
        Thread.sleep(3000);
        assertNotNull(gameClockRepository.getGameClock(gameId));
    }

    @Test
    public void expiredTurnTest() throws GameClockNotFoundException, InterruptedException {
        TestKit probe = new TestKit(system);
        clockActor.tell(new CreateGameClock(gameId,playerIds, twoTurns), null);
        Thread.sleep(3000);
        GameClock gameClock = gameClockRepository.getGameClock(gameId);
        int currentTurn = gameClock.getCurrentTurn();
        // send four turn completions
        for(String playerId: playerIds){
            clockActor.tell(new TurnOver(gameId, playerId, currentTurn), null);
        }
        // now currentTurn equals 2
        String playerId = playerIds.get(0);
        clockActor.tell(new TurnOver(gameId, playerId, currentTurn), probe.getRef());
        probe.expectMsg(Duration.ofSeconds(1), new Ack(Ack.TURN_COMPLETE, 2));
    }

    @Test
    public void invalidTurnTest() throws InterruptedException, GameClockNotFoundException {
        TestKit probe = new TestKit(system);
        clockActor.tell(new CreateGameClock(gameId,playerIds, twoTurns), null);
        Thread.sleep(3000);
        GameClock gameClock = gameClockRepository.getGameClock(gameId);
        int currentTurn = gameClock.getCurrentTurn();
        String playerId = playerIds.get(0);
        clockActor.tell(new TurnOver(gameId, playerId, currentTurn+1), probe.getRef());
        probe.expectMsg(Duration.ofSeconds(1), new Ack(Ack.ERROR, currentTurn));
    }

    @Test
    public void validTurnClockExpiresTest() throws InterruptedException, GameClockNotFoundException {
        TestKit probe = new TestKit(system);
        clockActor.tell(new CreateGameClock(gameId,playerIds, oneTurn), null);
        Thread.sleep(3000);
        GameClock gameClock = gameClockRepository.getGameClock(gameId);
        int currentTurn = gameClock.getCurrentTurn();
        // send four turn completions
        clockActor.tell(new TurnOver(gameId, playerIds.get(0), currentTurn), null);
        clockActor.tell(new TurnOver(gameId, playerIds.get(1), currentTurn), null);
        clockActor.tell(new TurnOver(gameId, playerIds.get(2), currentTurn), null);
        // testing on fourth message
        clockActor.tell(new TurnOver(gameId, playerIds.get(3), currentTurn), probe.getRef());
        probe.expectMsg(Duration.ofSeconds(1), new Ack(Ack.GAME_COMPLETE));
    }

    @Test
    public void validTurnClockDoesNotExpireTest() throws InterruptedException, GameClockNotFoundException {
        TestKit probe = new TestKit(system);
        clockActor.tell(new CreateGameClock(gameId,playerIds, oneTurn), null);
        Thread.sleep(3000);
        GameClock gameClock = gameClockRepository.getGameClock(gameId);
        int currentTurn = gameClock.getCurrentTurn();
        // send a turn completion with the current turn
        clockActor.tell(new TurnOver(gameId, playerIds.get(0), currentTurn), probe.getRef());
        probe.expectMsg(Duration.ofSeconds(1), new Ack(Ack.WAIT, currentTurn));
    }

    /*
    * This message is used to test whether when a ClockMetaDataRequest
    * is received by the ClockActor, then a ClockMetaDataResponse is
    * sent back with GameClock's meta data
    * */
    @Test void ClockMetaDataRequestResponseTest() throws InterruptedException, GameClockNotFoundException {
        TestKit probe = new TestKit(system);
        clockActor.tell(new CreateGameClock(gameId,playerIds, twoTurns), null);
        Thread.sleep(3000);
        GameClock gameClock = gameClockRepository.getGameClock(gameId);
        String gameId = gameClock.gameId;
        // send a ClockMetaDataRequest
        clockActor.tell(new ClockMetaDataRequest(gameId), probe.getRef());
        // expect ClockMetaDataResponse
        probe.expectMsg(Duration.ofSeconds(1), new ClockMetaDataResponse(
                ClockActor.TURN_INTERVAL,
                twoTurns,
                gameClock.getCurrentTurn()
        ));
    }

    /*
    * This method tests whether when all players send
    * TurnOver messages for the current turn an that turn
    * is completed, then the MarketActor is sent with a
    * CurrentMarketTurn message
    * */
    @Test
    public void sendCurrentMarketTurnTest() throws InterruptedException, GameClockNotFoundException {
        clockActor.tell(new CreateGameClock(gameId,playerIds, twoTurns), null);
        Thread.sleep(3000);
        GameClock gameClock = gameClockRepository.getGameClock(gameId);
        int currentTurn = gameClock.getCurrentTurn();
        // send a turn completion with the current turn
        clockActor.tell(new TurnOver(gameId, playerIds.get(0), currentTurn), ActorRef.noSender());
        clockActor.tell(new TurnOver(gameId, playerIds.get(1), currentTurn), ActorRef.noSender());
        clockActor.tell(new TurnOver(gameId, playerIds.get(2), currentTurn), ActorRef.noSender());
        clockActor.tell(new TurnOver(gameId, playerIds.get(3), currentTurn), ActorRef.noSender());
        marketProbe.expectMsg(Duration.ofSeconds(3), new CurrentMarketTurn(gameClock.gameId, currentTurn+1));
    }

}