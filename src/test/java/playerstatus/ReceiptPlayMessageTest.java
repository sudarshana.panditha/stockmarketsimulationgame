/* This test is intended to test whether PlayerStatusActor receives the Playing message from the client*/

/*package playerstatus;

import akka.actor.ActorRef;
import akka.actor.ActorSystem;
import akka.actor.Props;
import akka.testkit.javadsl.TestKit;
import org.junit.Assert;
import org.junit.BeforeClass;
import org.junit.AfterClass;
import org.junit.Test;

import java.time.Duration;

public class ReceiptPlayMessageTest {
    static ActorSystem system;

    @BeforeClass
    public static void setup (){
        system = ActorSystem.create();
    }

    @AfterClass
    public static void teardown() {
        TestKit.shutdownActorSystem(system);
        system = null;
    }

	
    @Test
    public void testPlayerStatusActorReceivesPlayingMessage() {
        //final TestKit probe = new TestKit(system);
        //final ActorRef GameMakerActor = system.actorOf(playerstatus.GameMakerActor.props());
        final Props props = Props.create(PlayerStatusActor.class);
        final ActorRef playerStatusActor = system.actorOf(props);
        final TestKit probe = new TestKit(system);
        playerStatusActor.tell("stimulus", probe.getRef());
        probe.awaitCond(probe::msgAvailable);
        probe.expectMsg(Duration.ZERO, "response");
       // Assert.assertEquals(getRef(), probe.getLastSender());

    }
}*/
