package playerstatus;

import akka.actor.ActorRef;
import akka.actor.ActorSystem;
import akka.actor.Props;
import akka.testkit.javadsl.TestKit;
import org.junit.BeforeClass;
import org.junit.AfterClass;
import org.junit.Test;
import playerstatus.messages.SignInMessage;
import java.time.Duration;

public class ReceiptSignInMessageTest {
    static ActorSystem system;

    @BeforeClass
    public static void setup (){
        system = ActorSystem.create();
    }

    @AfterClass
    public static void teardown() {
        TestKit.shutdownActorSystem(system);
        system = null;
    }
    
    @Test
    public void testGameMakerActorReceivesSignInMessage() {
        //final TestKit probe = new TestKit(system);
        //final ActorRef gameMakerActor = system.actorOf(playerstatus.GameMakerActor.props(probe.getRef()));
        final Props props = Props.create(GameMakerActor.class);
        final ActorRef gameMakerActor = system.actorOf(props);
        final TestKit probe = new TestKit(system);
        //gameMakerActor.tell(new SignInMessage("user"), ActorRef.noSender());
        probe.awaitCond(probe::msgAvailable);
        probe.expectMsg(Duration.ZERO, "response");
    }

    
}
