package market;

import market.exception.CompanyNotFoundException;
import market.exception.IllegalArgumentException;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.Test;

import java.util.List;
import java.util.Map;
import java.util.UUID;

import static org.junit.jupiter.api.Assertions.*;
/*
* This class contains the unit tests on
* MarketRepository class
* */
class MarketRepositoryTest {
    private static String marketId;
    private static int maxNumOfTurns;
    private static int negativeMaxNumOfTurns;
    private static int zeroMaxNumOfTurns;
    private static MarketModel marketModel;

    @BeforeAll
    public static void setup(){
        marketId = UUID.randomUUID().toString();
        maxNumOfTurns = 100;
        negativeMaxNumOfTurns = -1;
        zeroMaxNumOfTurns = 0;
        marketModel = new MarketModel();
    }

    /* This method tests whether when a negative max num of turns
     * is passed into createMarket, then an Exception is thrown
     * */
    @Test
    public void negativeMaxNumOfTurnsTest(){
        assertThrows(
                IllegalArgumentException.class,
                () -> {
                    MarketRepository repository = new MarketRepository();
                    repository.createMarket(marketId, negativeMaxNumOfTurns);
                }
        );
    }

    /* This method tests whether when a zero max num of turns
     * is passed into createMarket, an Exception is thrown
     * */
    @Test
    public void zeroMaxNumOfTurnsTest(){
        assertThrows(
                IllegalArgumentException.class,
                () -> {
                    MarketRepository repository = new MarketRepository();
                    repository.createMarket(marketId, zeroMaxNumOfTurns);
                }
        );
    }

    /* This method tests whether when a null market id
     * is passed into createMarket method it throws an
     * IllegalArgumentException
     * */
    @Test
    public void nullMarketIdTest(){
        assertThrows(
                IllegalArgumentException.class,
                () -> {
                    MarketRepository repository = new MarketRepository();
                    repository.createMarket(null, maxNumOfTurns);
                }
        );
    }

    /* This method tests whether getMarket method
     * returns the Market object for a given market id
     * */
    @Test
    public void getMarketTest() throws IllegalArgumentException {
        MarketRepository repository = new MarketRepository();
        repository.createMarket(marketId, maxNumOfTurns);
        assertEquals(
                marketId,
                repository.getMarket(marketId).marketId
        );
    }


    /* This method tests whether getSharePrice method
     * returns the correct share price of the given Company
     * in the given Market
     * */
    @Test
    public void getSharePriceTest() throws IllegalArgumentException, CompanyNotFoundException {
        MarketRepository repository = new MarketRepository();
        repository.createMarket(marketId, maxNumOfTurns);
        Market market = repository.getMarket(marketId);
        Company company = market.getCompanies().get(0);

        assertEquals(
                repository.getSharePrice(market.marketId, company.companyId),
                company.getSharePrice()
        );
    }

    /*
    * This method tests whether setCurrentTurn sets the current
    * turn of the given market with the correct turn value
    * */
    @Test
    public void setCurrentTurnTest() throws IllegalArgumentException {
        MarketRepository repository = new MarketRepository();
        repository.createMarket(marketId, maxNumOfTurns);
        Market market = repository.getMarket(marketId);
        int oldTurn = market.getCurrentTurn();
        repository.setCurrentTurn(market.marketId,oldTurn+1);
        int currentTurn = market.getCurrentTurn();

        assertEquals(
                oldTurn+1,
                currentTurn
        );
    }

    /*
    * This method is used to test whether getCompanyDetailsList
    * returns the correct CompanyDetails for each company of a
    * given Market
    * */
    @Test
    public void getCompanyDetailsListTest() throws IllegalArgumentException {
        MarketRepository repository = new MarketRepository();
        repository.createMarket(marketId, maxNumOfTurns);
        Market market = repository.getMarket(marketId);
        List<Company> companies = market.getCompanies();
        List<CompanyDetails> companyDetails = repository.getCompanyDetailsList(marketId);
        boolean allDetailsMatch[] = new boolean[companyDetails.size()];

        for(int i = 0; i < companyDetails.size(); i++){
            for (int j = 0; j < companies.size(); j++){
                boolean isMatch = companyDetails.get(i).companyId.equals(companies.get(j).companyId)
                        && companyDetails.get(i).name.equals(companies.get(j).name);
                if (isMatch){
                    allDetailsMatch[i] = true;
                    break;
                }
            }
        }

        boolean allAreMatch = true;
        for (boolean isMatch: allDetailsMatch){
            allAreMatch = allAreMatch && isMatch;
        }

        assertEquals(
                true,
                allAreMatch
        );

    }

    /*
    * This method tests whether when getAllSharePrices is called
    * it returns the correct share prices of each company organized
    * into sectors
    * */
    @Test
    public void getAllSharePricesTest() throws IllegalArgumentException {
        MarketRepository repository = new MarketRepository();
        repository.createMarket(marketId, maxNumOfTurns);
        Market market = repository.getMarket(marketId);

        Map<String, Map<String, Double>> allSharePrices = repository.getAllSharePrices(marketId);

        List<Sector> sectors = market.getSectors();
        List<Company> companies = market.getCompanies();
        boolean allMatches[] = new boolean[companies.size()];

        for(int i = 0; i < sectors.size(); i++){
            Sector sector = sectors.get(i);
            Map<String, Double> companySharePriceMap = allSharePrices.get(sector.sectorId);
            for (int j = 0; j < companies.size(); j++){
                Company company = companies.get(j);
                if(companySharePriceMap.containsKey(company.companyId)){
                    double sharePrice = companySharePriceMap.get(company.companyId).doubleValue();
                    if(sharePrice == company.getSharePrice()){
                        allMatches[j] = true;
                    }
                }
            }
        }

        boolean allAreMatch = true;
        for (boolean isMatch: allMatches){
            allAreMatch = allAreMatch && isMatch;
        }

        assertEquals(
                true,
                allAreMatch
        );
    }

    /*
    * This method is used to test whether the MarketRepository
    * does return an array of Events but not null
    * when getEventTrend is called
    * */
    @Test
    public void getEventTrendTest() throws IllegalArgumentException {
        MarketRepository repository = new MarketRepository();
        repository.createMarket(marketId, maxNumOfTurns);

        assertNotNull(repository.getEventTrend(marketId));
    }

    /*
     * This method is used to test whether the MarketRepository
     * does return an array of integers but not null when
     * getMarketTrend is called
     * */
    @Test
    public void getMarketTrendTest() throws IllegalArgumentException {
        MarketRepository repository = new MarketRepository();
        repository.createMarket(marketId, maxNumOfTurns);

        assertNotNull(repository.getMarketTrend(marketId));
    }

    /*
     * This method is used to test whether the MarketRepository
     * does return an Map of sector ids to their corresponding trend
     * but not null when getSectorTrends is called
     * */
    @Test
    public void getSectorTrendsTest() throws IllegalArgumentException {
        MarketRepository repository = new MarketRepository();
        repository.createMarket(marketId, maxNumOfTurns);

        assertNotNull(repository.getSectorTrends(marketId));
    }

    /*
     * This method is used to test whether the MarketRepository
     * does return an Map of company ids to their corresponding trend
     * but not null when getRandomTrends is called
     * */
    @Test
    public void getRandomTrendsTest() throws IllegalArgumentException {
        MarketRepository repository = new MarketRepository();
        repository.createMarket(marketId, maxNumOfTurns);

        assertNotNull(repository.getRandomTrends(marketId));
    }

    /*
    * This method is used to test whether getSectors method
    * does return a list of sectors and not null
    * */
    @Test
    public void getSectorsTest() throws IllegalArgumentException {
        MarketRepository repository = new MarketRepository();
        repository.createMarket(marketId, maxNumOfTurns);

        assertNotNull(repository.getSectors(marketId));
    }

    /*
    * This method tests whether getSectorCompanyDetails
    * does return a Map of Sector ids and the details
    * of their corresponding Companies and not null
    * */

    @Test
    public void getSectorCompanyDetailsTest() throws IllegalArgumentException {
        MarketRepository repository = new MarketRepository();
        repository.createMarket(marketId, maxNumOfTurns);

        assertNotNull(repository.getSectorCompanyDetails(marketId));
    }

}