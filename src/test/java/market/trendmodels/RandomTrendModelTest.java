package market.trendmodels;

import market.Company;
import market.MarketModel;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.Test;

import java.util.List;

import static org.junit.jupiter.api.Assertions.*;

class RandomTrendModelTest {
    public static int maxNumOfTurns;
    public static MarketModel marketModel;
    public static double expectedIncrease;
    public static double expectedDecrease;
    public static double expectedNoValueChange;
    public static double errorMargin;
    public static int plusTwo;
    public static int minusTwo;

    @BeforeAll
    public static void setup(){
        maxNumOfTurns = 3000; // testing over 3000 turns
        marketModel = new MarketModel(); // probability of 0.25
        expectedIncrease = expectedDecrease = 0.25; // probability of 0.25
        expectedNoValueChange = 0.50; // probability of 0.50
        errorMargin = 0.05; // 5% error margin
        plusTwo = +2;
        minusTwo = -2;
    }

    /* This method tests whether the probability of increase
    * in value of a Random Trend is 0.25 within an error margin
    * of 0.05 (5% error)
    * */
    @Test
    public void valueIncreaseProbabilityTest(){
        int numOfIncreases = 0;
        int numOfDecreases = 0;
        int numOfNoChanges = 0;

        for(int round = 1; round <= 5; ++round){ // averaging over 5 rounds
            RandomTrendModel model = new RandomTrendModel(maxNumOfTurns, marketModel);
            int value;
            int previousValue;
            List<Company> companies = marketModel.getCompanies();
            for(Company company: companies){
                for(int i = 1; i < maxNumOfTurns; ++i){
                    previousValue = model.getValue(company.companyId, i-1);
                    value = model.getValue(company.companyId, i);
                    if(value - previousValue > 0){
                        numOfIncreases++;
                    }else if(value - previousValue < 0){
                        numOfDecreases++;
                    }else if(value - previousValue == 0){
                        numOfNoChanges++;
                    }
                }
            }

        }
        double actualIncrease  = 1.0*numOfIncreases/(numOfIncreases+numOfDecreases+numOfNoChanges);
        System.out.println(actualIncrease);
        assertEquals(
                true,
                Math.abs(
                        expectedIncrease-actualIncrease
                ) <= errorMargin
        );
    }

    /* This method tests whether the probability of decrease
     * in value of a Random Trend is 0.25 within an error margin
     * of 0.05 (5% error)
     * */
    @Test
    public void valueDecreaseProbabilityTest(){
        int numOfIncreases = 0;
        int numOfDecreases = 0;
        int numOfNoChanges = 0;

        for(int round = 1; round <= 5; ++round){ // averaging over 5 rounds
            RandomTrendModel model = new RandomTrendModel(maxNumOfTurns, marketModel);
            int value;
            int previousValue;
            List<Company> companies = marketModel.getCompanies();
            for(Company company: companies){
                for(int i = 1; i < maxNumOfTurns; ++i){
                    previousValue = model.getValue(company.companyId, i-1);
                    value = model.getValue(company.companyId, i);
                    if(value - previousValue > 0){
                        numOfIncreases++;
                    }else if(value - previousValue < 0){
                        numOfDecreases++;
                    }else if(value - previousValue == 0){
                        numOfNoChanges++;
                    }
                }
            }

        }
        double actualDecrease  = 1.0*numOfDecreases/(numOfIncreases+numOfDecreases+numOfNoChanges);
        System.out.println(actualDecrease);
        assertEquals(
                true,
                Math.abs(
                        expectedIncrease-actualDecrease
                ) <= errorMargin
        );
    }

    /* This method tests whether the probability of value
     * not changing in a Random Trend is 0.50 within an
     * error margin of 0.05 (5% error)
     * */
    @Test
    public void noValueChangeProbabilityTest(){
        int numOfIncreases = 0;
        int numOfDecreases = 0;
        int numOfNoChanges = 0;

        for(int round = 1; round <= 5; ++round){ // averaging over 5 rounds
            RandomTrendModel model = new RandomTrendModel(maxNumOfTurns, marketModel);
            int value;
            int previousValue;
            List<Company> companies = marketModel.getCompanies();
            for(Company company: companies){
                for(int i = 1; i < maxNumOfTurns; ++i){
                    previousValue = model.getValue(company.companyId, i-1);
                    value = model.getValue(company.companyId, i);
                    if(value - previousValue > 0){
                        numOfIncreases++;
                    }else if(value - previousValue < 0){
                        numOfDecreases++;
                    }else if(value - previousValue == 0){
                        numOfNoChanges++;
                    }
                }
            }

        }
        double actualNoValueChange  = 1.0*numOfNoChanges/(numOfIncreases+numOfDecreases+numOfNoChanges);
        System.out.println(actualNoValueChange);
        assertEquals(
                true,
                Math.abs(
                        expectedNoValueChange-actualNoValueChange
                ) <= errorMargin
        );
    }

    /* This method tests whether the value stream of a
     * Random Market Trend is within the range of -2 to +2
     * */
    @Test
    public void valueStreamBetweenPlusOrMinusTwoTest(){

        boolean isWithinPlusOrMinusThree = true;

        for(int round = 1; round <= 5; ++round){ // averaging over 5 rounds
            RandomTrendModel model = new RandomTrendModel(maxNumOfTurns, marketModel);
            int value;
            List<Company> companies = marketModel.getCompanies();
            for(Company company: companies){
                for(int i = 1; i < maxNumOfTurns; ++i){
                    value = model.getValue(company.companyId, i);
                    if(value < minusTwo || value > plusTwo){
                        isWithinPlusOrMinusThree = false;
                    }
                }
            }

        }
        assertEquals(
                true,
                isWithinPlusOrMinusThree
        );
    }
}