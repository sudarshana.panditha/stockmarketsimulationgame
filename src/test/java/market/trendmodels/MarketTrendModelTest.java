package market.trendmodels;

import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;

class MarketTrendModelTest {
    public static int maxNumberOfTurns;
    public static double expectedIncrease; // probability of increases
    public static double expectedDecrease; // probability of decreases
    public static double errorMargin;

    @BeforeAll
    public static void setup(){
        maxNumberOfTurns = 3000; // testing for 3000 turns
        expectedIncrease = 0.50;
        expectedDecrease = 0.50;
        errorMargin = 0.05; // testing within 5% error margin
    }

    @Test
    public void valueIncreaseProbabilityTest(){
        int numOfIncreases = 0;
        int numOfDecreases = 0;

        for(int round = 1; round <= 5; ++round){
            MarketTrendModel marketTrendModel = new MarketTrendModel(maxNumberOfTurns);
            int value;
            int previousValue;
            for(int i = 1; i < maxNumberOfTurns; ++i){
                previousValue = marketTrendModel.getValue(i-1);
                value = marketTrendModel.getValue(i);
                if(value - previousValue > 0){
                    numOfIncreases++;
                }else if(value - previousValue < 0){
                    numOfDecreases++;
                }
            }
        }
        double actualIncrease  = 1.0*numOfIncreases/(numOfIncreases+numOfDecreases);
        System.out.println(actualIncrease);
        assertEquals(
                true,
                Math.abs(
                        expectedIncrease-actualIncrease
                ) <= errorMargin
        );
    }

    @Test
    public void valueDecreaseProbabilityTest(){
        int numOfIncreases = 0;
        int numOfDecreases = 0;

        for(int round = 1; round <= 5; ++round){
            MarketTrendModel marketTrendModel = new MarketTrendModel(maxNumberOfTurns);
            int value;
            int previousValue;
            for(int i = 1; i < maxNumberOfTurns; ++i){
                previousValue = marketTrendModel.getValue(i-1);
                value = marketTrendModel.getValue(i);
                if(value - previousValue > 0){
                    numOfIncreases++;
                }else if(value - previousValue < 0){
                    numOfDecreases++;
                }
            }
        }
        double actualDecrease  = 1.0*numOfDecreases/(numOfIncreases+numOfDecreases);
        System.out.println(actualDecrease);
        assertEquals(
                true,
                Math.abs(
                        expectedIncrease-actualDecrease
                ) <= errorMargin
        );
    }

    @Test
    public void valueRangeBetweenPlusOrMinusThreeTest(){
        boolean isWithinPlusOrMinusThree = true;
        MarketTrendModel marketTrendModel = new MarketTrendModel(maxNumberOfTurns);
        int value;
        for(int i = 1; i < maxNumberOfTurns; ++i){
            value = marketTrendModel.getValue(i);
            if(Math.abs(value) > 3){
                isWithinPlusOrMinusThree = false;
                break;
            }
        }
        assertEquals(
                true,
                isWithinPlusOrMinusThree
        );
    }
}