package market.trendmodels;

import market.MarketModel;
import market.Sector;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.Test;

import java.util.List;

import static org.junit.jupiter.api.Assertions.*;

class SectorTrendModelTest {
    public static int maxNumOfTurns;
    public static MarketModel marketModel;
    public static double expectedIncrease;
    public static double expectedDecrease;
    public static double expectedNoValueChange;
    public static double errorMargin;

    @BeforeAll
    public static void setup(){
        maxNumOfTurns = 3000; // testing over 3000 turns
        marketModel = new MarketModel(); // probability of 0.25
        expectedIncrease = expectedDecrease = 0.25; // probability of 0.25
        expectedNoValueChange = 0.50; // probability of 0.50
        errorMargin = 0.05; // 5% error margin
    }

    @Test
    public void valueIncreaseProbabilityTest(){
        int numOfIncreases = 0;
        int numOfDecreases = 0;
        int numOfNoChanges = 0;

        for(int round = 1; round <= 5; ++round){ // averaging over 5 rounds
            SectorTrendModel model = new SectorTrendModel(maxNumOfTurns, marketModel);
            int value;
            int previousValue;
            List<Sector> sectors = marketModel.getSectors();
            for(Sector sector: sectors){
                for(int i = 1; i < maxNumOfTurns; ++i){
                    previousValue = model.getValue(sector.sectorId, i-1);
                    value = model.getValue(sector.sectorId, i);
                    if(value - previousValue > 0){
                        numOfIncreases++;
                    }else if(value - previousValue < 0){
                        numOfDecreases++;
                    }else if(value - previousValue == 0){
                        numOfNoChanges++;
                    }
                }
            }

        }
        double actualIncrease  = 1.0*numOfIncreases/(numOfIncreases+numOfDecreases+numOfNoChanges);
        System.out.println(actualIncrease);
        assertEquals(
                true,
                Math.abs(
                        expectedIncrease-actualIncrease
                ) <= errorMargin
        );
    }

    @Test
    public void valueDecreaseProbabilityTest(){
        int numOfIncreases = 0;
        int numOfDecreases = 0;
        int numOfNoChanges = 0;

        for(int round = 1; round <= 5; ++round){ // averaging over 5 rounds
            SectorTrendModel model = new SectorTrendModel(maxNumOfTurns, marketModel);
            int value;
            int previousValue;
            List<Sector> sectors = marketModel.getSectors();
            for(Sector sector: sectors){
                for(int i = 1; i < maxNumOfTurns; ++i){
                    previousValue = model.getValue(sector.sectorId, i-1);
                    value = model.getValue(sector.sectorId, i);
                    if(value - previousValue > 0){
                        numOfIncreases++;
                    }else if(value - previousValue < 0){
                        numOfDecreases++;
                    }else if(value - previousValue == 0){
                        numOfNoChanges++;
                    }
                }
            }

        }
        double actualDecrease  = 1.0*numOfDecreases/(numOfIncreases+numOfDecreases+numOfNoChanges);
        System.out.println(actualDecrease);
        assertEquals(
                true,
                Math.abs(
                        expectedIncrease-actualDecrease
                ) <= errorMargin
        );
    }

    @Test
    public void noValueChangeProbabilityTest(){
        int numOfIncreases = 0;
        int numOfDecreases = 0;
        int numOfNoChanges = 0;

        for(int round = 1; round <= 5; ++round){ // averaging over 5 rounds
            SectorTrendModel model = new SectorTrendModel(maxNumOfTurns, marketModel);
            int value;
            int previousValue;
            List<Sector> sectors = marketModel.getSectors();
            for(Sector sector: sectors){
                for(int i = 1; i < maxNumOfTurns; ++i){
                    previousValue = model.getValue(sector.sectorId, i-1);
                    value = model.getValue(sector.sectorId, i);
                    if(value - previousValue > 0){
                        numOfIncreases++;
                    }else if(value - previousValue < 0){
                        numOfDecreases++;
                    }else if(value - previousValue == 0){
                        numOfNoChanges++;
                    }
                }
            }

        }
        double actualNoValueChange  = 1.0*numOfNoChanges/(numOfIncreases+numOfDecreases+numOfNoChanges);
        System.out.println(actualNoValueChange);
        assertEquals(
                true,
                Math.abs(
                        expectedNoValueChange-actualNoValueChange
                ) <= errorMargin
        );
    }

    @Test
    public void valueStreamBetweenPlusOrMinusThreeTest(){

        boolean isWithinPlusOrMinusThree = true;

        for(int round = 1; round <= 5; ++round){ // averaging over 5 rounds
            SectorTrendModel model = new SectorTrendModel(maxNumOfTurns, marketModel);
            int value;
            List<Sector> sectors = marketModel.getSectors();
            for(Sector sector: sectors){
                for(int i = 1; i < maxNumOfTurns; ++i){
                    value = model.getValue(sector.sectorId, i);
                    if(value < -3 || value > +3){
                        isWithinPlusOrMinusThree = false;
                    }
                }
            }

        }
        assertEquals(
                true,
                isWithinPlusOrMinusThree
        );
    }
}