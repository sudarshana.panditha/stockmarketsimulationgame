package market.trendmodels;

import market.Event;
import market.MarketModel;
import market.SectorEvent;
import market.StockEvent;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;

class EventTrendModelTest {
    public static int maxNumOfTurns;
    public static MarketModel marketModel;
    public static double errorMargin;
    public static double expectedSectorEventPercentage;
    public static double expectedStockEventPercentage;

    @BeforeAll
    public static void setup(){
        maxNumOfTurns = 3000; // test on 3000 turn market
        marketModel = new MarketModel();
        errorMargin = 0.05;
        expectedSectorEventPercentage = 0.33;
        expectedStockEventPercentage = 0.67;
    }

    @Test
    public void sectorEventProbabilityTest() {
        int numOfSectorEvents = 0;
        int numOfStockEvents = 0;
        for (int round = 1; round <= 10; round++){ // averaging over 10 rounds
            EventTrendModel model = new EventTrendModel(marketModel,maxNumOfTurns);
            Event event;
            for(int i = 0; i < maxNumOfTurns; ++i){
                event = model.getEvent(i);
                if(event!= null){
                    if(event instanceof SectorEvent){
                        numOfSectorEvents++;
                    }else if(event instanceof StockEvent){
                        numOfStockEvents++;
                    }
                }
            }
        }
        double sectorEventPercentage = 1.0*numOfSectorEvents/(numOfSectorEvents+numOfStockEvents);
        System.out.println(sectorEventPercentage);
        assertEquals(
                true,
                Math.abs(
                        expectedSectorEventPercentage-sectorEventPercentage
                ) <= errorMargin
        );
    }

    @Test
    public void stockEventProbabilityTest() {
        int numOfSectorEvents = 0;
        int numOfStockEvents = 0;
        for (int round = 1; round <= 10; round++){ // averaging over 10 rounds
            EventTrendModel model = new EventTrendModel(marketModel,maxNumOfTurns);
            Event event;
            for(int i = 0; i < maxNumOfTurns; ++i){
                event = model.getEvent(i);
                if(event!= null){
                    if(event instanceof SectorEvent){
                        numOfSectorEvents++;
                    }else if(event instanceof StockEvent){
                        numOfStockEvents++;
                    }
                }
            }
        }
        double stockEventPercentage = 1.0*numOfStockEvents/(numOfSectorEvents+numOfStockEvents);
        System.out.println(stockEventPercentage);
        assertEquals(
                true,
                Math.abs(
                        expectedStockEventPercentage-stockEventPercentage
                ) <= errorMargin
        );
    }
}