package market;

import market.exception.CompanyNotFoundException;
import market.exception.IllegalArgumentException;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.Test;

import java.util.UUID;

import static org.junit.jupiter.api.Assertions.*;

/*
* This class contains the unit tests on Market class
* */
class MarketTest {
    private static String marketId;
    private static int maxNumOfTurns;
    private static int negativeMaxNumOfTurns;
    private static int zeroMaxNumOfTurns;
    private static MarketModel marketModel;

    @BeforeAll
    public static void setup(){
        marketId = UUID.randomUUID().toString();
        maxNumOfTurns = 100;
        negativeMaxNumOfTurns = -1;
        zeroMaxNumOfTurns = 0;
        marketModel = new MarketModel();
    }

    /* This method tests whether when a negative max num of turns
    * is passed into the constructor it throws an IllegalArgumentException
    * */
    @Test
    public void negativeMaxNumOfTurnsTest(){
        assertThrows(
                IllegalArgumentException.class,
                () -> {
                    Market market = new Market(marketId, negativeMaxNumOfTurns, marketModel);
                }
        );
    }

    /* This method tests whether when a zero max num of turns
     * is passed into the constructor it throws an IllegalArgumentException
     * */
    @Test
    public void zeroMaxNumOfTurnsTest(){
        assertThrows(
                IllegalArgumentException.class,
                () -> {
                    Market market = new Market(marketId, zeroMaxNumOfTurns, marketModel);
                }
        );
    }

    /* This method tests whether when a null market id
     * is passed into the constructor it throws an IllegalArgumentException
     * */
    @Test
    public void nullMarketIdTest(){
        assertThrows(
                IllegalArgumentException.class,
                () -> {
                    Market market = new Market(null, maxNumOfTurns, marketModel);
                }
        );
    }

    /* This method tests whether when a null market model
     * is passed into the constructor it throws an IllegalArgumentException
     * */
    @Test
    public void nullMarketModelTest(){
        assertThrows(
                IllegalArgumentException.class,
                () -> {
                    Market market = new Market(marketId, maxNumOfTurns, null);
                }
        );
    }

    /* This method tests whether when a Market is
     * is created, its initial current turn is equal to one
     * */
    @Test
    public void initialCurrentTurnTest() throws IllegalArgumentException {
        Market market = new Market(marketId,maxNumOfTurns, marketModel);
        assertEquals(1, market.getCurrentTurn());
    }

    /* This method tests whether when a getSharePrice is
     * called with an invalid company id it throws a
     * CompanyNotFoundException
     * */
    @Test
    public void getSharePriceTest() {
        assertThrows(
                CompanyNotFoundException.class,
                () -> {
                    Market market = new Market(marketId,maxNumOfTurns, marketModel);
                    market.getSharePrice("fakeCompanyId123");
                }
        );
    }

    /* This method tests whether when getCompanies is
     * called it does not return null but a list of
     * Companies
     * */
    @Test
    public void getCompaniesTest() throws IllegalArgumentException {
        assertNotNull(new Market(marketId,maxNumOfTurns,marketModel).getCompanies());
    }

    /* This method tests whether when setCurrentTurn is
     * called it sets the correct turn
     * */
    @Test
    public void setCurrentTurnTest() throws IllegalArgumentException {
        Market market = new Market(marketId, maxNumOfTurns, marketModel);
        int oldTurn = market.getCurrentTurn();
        market.setCurrentTurn(oldTurn+1);
        int currentTurn = market.getCurrentTurn();
        assertEquals(
                oldTurn+1,
                currentTurn
        );
    }

    /* This method tests whether when getSectors is
     * called it does not return null but a list of
     * Sectors
     * */
    @Test
    public void getSectorsTest() throws IllegalArgumentException {
        assertNotNull(new Market(marketId,maxNumOfTurns,marketModel).getSectors());
    }

    /*
    * This method tests whether the getEventTrend does
    * not return null but an array of Events
    * */
    @Test
    public void getEventTrendTest() throws IllegalArgumentException {
        Market market = new Market(marketId, maxNumOfTurns, marketModel);
        assertNotNull(market.getEventTrend());
    }

    /*
     * This method tests whether the getMarketTrend does
     * not return null but an array of integers
     * */
    @Test
    public void getMarketTrendTest() throws IllegalArgumentException {
        Market market = new Market(marketId, maxNumOfTurns, marketModel);
        assertNotNull(market.getMarketTrend());
    }

    /*
    * This method tests whether the getSectorTrends does
    * not return null but a Map of sector ids and their
    * corresponding sector trends
    * */
    @Test
    public void getSectorTrendsTest() throws IllegalArgumentException {
        Market market = new Market(marketId, maxNumOfTurns, marketModel);
        assertNotNull(market.getSectorTrends());
    }

    /*
     * This method tests whether the getRandomTrends does
     * not return null but a Map of company ids and their
     * corresponding sector trends
     * */
    @Test
    public void getRandomTrendsTest() throws IllegalArgumentException {
        Market market = new Market(marketId, maxNumOfTurns, marketModel);
        assertNotNull(market.getRandomTrends());
    }

}