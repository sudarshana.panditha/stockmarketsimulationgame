package market;

import akka.actor.ActorRef;
import akka.actor.ActorSystem;
import akka.actor.Props;
import akka.testkit.javadsl.TestKit;
import market.exception.IllegalArgumentException;
import market.messages.*;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.Test;

import java.time.Duration;
import java.util.Map;
import java.util.UUID;

import static org.junit.jupiter.api.Assertions.*;
/*
* This class contains the state and behavior
* tests on the MarketActor class
* */
class MarketActorTest {
    public static ActorSystem system;
    public static ActorRef marketActor;
    public static MarketRepository marketRepository;
    public static String marketId;
    public static int maxNumOfTurns;
    public static String requestId;

    @BeforeAll
    public static void setup(){
        system = ActorSystem.create();
        marketRepository = new MarketRepository();
        Props marketProps = Props.create(MarketActor.class, marketRepository);
        marketActor = system.actorOf(marketProps);
        marketId = UUID.randomUUID().toString();
        maxNumOfTurns = 100;
        requestId = UUID.randomUUID().toString();
    }

    /*
    * This method is used to test whether when a
    * MarketActor receives a CreateMarket message
    * then it creates a Market in the MarketRepository
    * */
    @Test
    public void createMarketTest() throws InterruptedException {
        marketActor.tell(new CreateMarket(marketId, maxNumOfTurns), ActorRef.noSender());
        Thread.sleep(3000);
        assertEquals(
                marketId,
                marketRepository.getMarket(marketId).marketId
        );
    }

    /*
     * This method is used to test whether when a
     * MarketActor receives a SharePriceRequest message
     * then it sends back a SharePriceResponse message
     * */
    @Test
    public void sharePriceRequestResponseTest() throws IllegalArgumentException {
        marketRepository.createMarket(marketId,maxNumOfTurns);
        Market market = marketRepository.getMarket(marketId);
        Company company = market.getCompanies().get(0);
        // Testing the MarketActor
        TestKit probe = new TestKit(system);
        marketActor.tell(
                new SharePriceRequest(requestId,market.marketId, company.companyId),
                probe.getRef()
        );
        probe.expectMsg(Duration.ofSeconds(5), new SharePriceResponse(requestId, company.getSharePrice()));
    }

    /*
     * This method is used to test whether when a MarketActor
     * receives a CurrentMarketTurn message then it sets the
     * current turn of the given Market correctly
     * */
    @Test
    public void setCurrentTurnTest() throws IllegalArgumentException, InterruptedException {
        marketRepository.createMarket(marketId,maxNumOfTurns);
        Market market = marketRepository.getMarket(marketId);
        int currentTurn = market.getCurrentTurn();
        marketActor.tell(
                new CurrentMarketTurn(market.marketId, currentTurn+1),
                ActorRef.noSender()
        );
        Thread.sleep(3000);
        assertEquals(
                currentTurn+1,
                market.getCurrentTurn()
        );
    }

    /*
     * This method is used to test whether when a MarketActor
     * receives a CompanyListRequest then it sends back a
     * CompanyListResponse with a List of company details
     * */
    @Test
    public void getCompanyDetailsListTest() throws IllegalArgumentException {
        marketRepository.createMarket(marketId,maxNumOfTurns);

        // Testing the MarketActor
        TestKit probe = new TestKit(system);
        marketActor.tell(
                new CompanyListRequest(marketId),
                probe.getRef()
        );

        probe.expectMsg(Duration.ofSeconds(5), new CompanyListResponse(
                marketId, marketRepository.getCompanyDetailsList(marketId))
        );
    }

    /*
    * This method tests whether when a MarketActor receives
    * a AllSharePricesRequest, then it responds with a
    * AllSharePricesResponse message
    * */
    @Test
    public void allSharePricesRequestResponseTest() throws IllegalArgumentException {
        marketRepository.createMarket(marketId,maxNumOfTurns);

        // Testing the MarketActor
        TestKit probe = new TestKit(system);
        marketActor.tell(
                new AllSharePricesRequest(marketId),
                probe.getRef()
        );

        Map<String, Map<String, Double>> allSharePrices = marketRepository.getAllSharePrices(marketId);

        probe.expectMsg(Duration.ofSeconds(5), new AllSharePricesResponse(marketId, allSharePrices));
    }

    /*
     * This method tests whether when a MarketActor receives
     * a AllTrendsRequest, then it responds with a
     * AllTrendsResponse message
     * */
    @Test
    public void allTrendsRequestResponseTest() throws IllegalArgumentException {
        marketRepository.createMarket(marketId,maxNumOfTurns);

        // Testing the MarketActor
        TestKit probe = new TestKit(system);
        marketActor.tell(
                new AllTrendsRequest(marketId),
                probe.getRef()
        );

        Map<String, int [] > sectorTrends = marketRepository.getSectorTrends(marketId);
        Map<String, int [] > randomTrends = marketRepository.getRandomTrends(marketId);
        int marketTrend []  = marketRepository.getMarketTrend(marketId);
        Event eventTrend [] = marketRepository.getEventTrend(marketId);

        probe.expectMsg(
                Duration.ofSeconds(5),
                new AllTrendsResponse(sectorTrends,randomTrends,marketTrend, eventTrend)
        );
    }

    /*
    * This method tests whether when a MarketActor receives
    * a MarketMetaDataRequest, then it responds with a
    * MarketMetaDataResponse
    * */
    @Test
    public void marketMetaDataRequestResponseTest() throws IllegalArgumentException {
        marketRepository.createMarket(marketId,maxNumOfTurns);

        // Testing the MarketActor
        TestKit probe = new TestKit(system);
        marketActor.tell(
                new MarketMetaDataRequest(marketId),
                probe.getRef()
        );



        probe.expectMsg(
                Duration.ofSeconds(5),
                new MarketMetaDataResponse(
                        marketRepository.getSectors(marketId),
                        marketRepository.getSectorCompanyDetails(marketId)
                )
        );
    }
}