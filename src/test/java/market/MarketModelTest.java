package market;

import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.Test;

import java.util.List;

import static org.junit.jupiter.api.Assertions.*;

class MarketModelTest {
    public static int minNumOfSectors; // min num of sectors required for Game
    public static int minNumOfCompanies; // min num of companies required for Game

    @BeforeAll
    public static void setup(){
        minNumOfSectors = 4; // at least 4 sectors
        minNumOfCompanies = 3*4; // at least 3 companies in 4 sectors
    }

    @Test
    public void minFourSectorsTest(){
        MarketModel marketModel = new MarketModel();
        assertEquals(minNumOfSectors,marketModel.getSectors().size());
    }

    @Test
    public void minTwelveCompaniesTest(){
        MarketModel marketModel = new MarketModel();
        assertEquals(minNumOfCompanies,marketModel.getCompanies().size());
    }

    @Test
    public void getRandomSectorNotNullTest(){
        MarketModel marketModel = new MarketModel();
        assertNotNull(marketModel.getRandomSector());
    }

    @Test
    public void getRandomCompanyNotNullTest(){
        MarketModel marketModel = new MarketModel();
        assertNotNull(marketModel.getRandomCompany());
    }

    @Test
    public void getRandomSectorIsInSectorsTest(){
        MarketModel marketModel = new MarketModel();
        assertEquals(true, marketModel.getSectors()
                .contains(
                        marketModel.getRandomSector()
                ));
    }

    @Test
    public void getRandomCompanyIsInCompaniesTest(){
        MarketModel marketModel = new MarketModel();
        assertEquals(true, marketModel.getCompanies()
                .contains(
                        marketModel.getRandomCompany()
                ));
    }
}