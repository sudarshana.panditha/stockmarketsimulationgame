package market;

import market.exception.DurationNotPositiveException;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.Test;

import java.util.UUID;

import static org.junit.jupiter.api.Assertions.*;

class StockEventTest {
    public static String companyId;
    public static int value;
    public static int negativeDuration;
    public static int zeroDuration;

    @BeforeAll
    public static void setup(){
        companyId = UUID.randomUUID().toString();
        value = 7;
        negativeDuration = -1;
        zeroDuration = 0;
    }

    @Test
    public void negativeDurationTest(){
        assertThrows(
                DurationNotPositiveException.class,
                () -> {
                    new StockEvent(companyId,value,negativeDuration);
                }
        );
    }

    @Test
    public void zeroDurationTest(){
        assertThrows(
                DurationNotPositiveException.class,
                () -> {
                    new StockEvent(companyId,value,zeroDuration);
                }
        );
    }
}