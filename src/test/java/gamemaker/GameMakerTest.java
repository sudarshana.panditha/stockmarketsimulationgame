package gamemaker;

import akka.actor.ActorRef;
import akka.actor.ActorSystem;
import akka.actor.Props;
import akka.testkit.javadsl.TestKit;
import broker.messages.CreatePortfolios;
import clock.messages.CreateGameClock;
import gamemaker.exception.GameNotFoundException;
import gamemaker.game.Game;
import gamemaker.game.GameRepository;
import gamemaker.messages.Ack;
import gamemaker.messages.GameRequest;
import gamemaker.messages.GameResponse;
import gamemaker.messages.Play;
import market.messages.CreateMarket;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Disabled;
import org.junit.jupiter.api.Test;

import java.awt.*;
import java.time.Duration;
import java.util.*;

import static org.junit.jupiter.api.Assertions.*;
/*
* This class contains the state and behavior tests
* on the GameMaker Actor class
* */
class GameMakerTest {
    public static ActorSystem system;
    public static ActorRef gameMaker;
    public static TestKit marketProbe;
    public static TestKit clockProbe;
    public static TestKit brokerProbe;
    public static GameRepository gameRepository;
    public static String userIds[];
    public static String usernames[];

    @BeforeAll
    public static void setup(){
        // data for testing
        userIds = new String[4];
        usernames = new String[4];
        for(int i = 0; i < 4; ++i){
            userIds[i] = UUID.randomUUID().toString();
            usernames[i] = "username" + (i+1);
        }
        gameRepository = new GameRepository();
        // actors for testing
        system = ActorSystem.create();
        marketProbe = new TestKit(system);
        clockProbe = new TestKit(system);
        brokerProbe = new TestKit(system);
        Props gameMakerProps  = Props.create(
                GameMaker.class,
                gameRepository,
                clockProbe.getRef(),
                marketProbe.getRef(),
                brokerProbe.getRef()
        );
        gameMaker = system.actorOf(gameMakerProps);
    }

    @BeforeEach
    public void resetAndSetup(){
        setup();
    }

    /*
    * This method tests  whether when a Play message is received
    * then an Ack message is sent back to the sender
    * */
    @Test
    public void playAckTest() throws InterruptedException, GameNotFoundException {
        // receiving message
        Play play = new Play(userIds[0], usernames[0]);
        // testing
        TestKit probe = new TestKit(system);
        gameMaker.tell(play, probe.getRef());
        Thread.sleep(3000);
        Game game = gameRepository.getNextGame();
        probe.expectMsg(Duration.ofSeconds(5), new Ack(game.gameId));

    }

    /*
     * This method tests  whether when a Play message is received
     * then an CreateMarket message is sent to the MarketActor
     * */
    @Disabled("Responsibility of sending this message is moved into GameRequest message handle")
    @Test
    public void playCreateMarketTest() throws InterruptedException, GameNotFoundException {
        // receiving message
        Play play = new Play(userIds[0], usernames[0]);
        // testing
        gameMaker.tell(play, ActorRef.noSender());
        Thread.sleep(3000);
        Game game = gameRepository.getNextGame();
        marketProbe.expectMsg(Duration.ofSeconds(3), new CreateMarket(game.gameId, GameMaker.MAX_NUM_OF_TURNS));
    }

    /*
    * This method tests whether when a GameRequest message is
    * received by the GameMaker and the Game is in NOT_READY
    * state, then a GameResponse with state WAIT is sent back
    * to the sender
    * */
    @Test
    public void gameRequestGameNotReadyTest() throws InterruptedException, GameNotFoundException {
        // receiving message
        Play play = new Play(userIds[0], usernames[0]);
        // testing
        gameMaker.tell(play, ActorRef.noSender());
        Thread.sleep(3000);
        Game game = gameRepository.getNextGame();
        TestKit probe = new TestKit(system);
        gameMaker.tell(new GameRequest(game.gameId), probe.getRef());
        probe.expectMsg(Duration.ofSeconds(3), new GameResponse(GameResponse.WAIT));
    }

    /*
     * This method tests whether when a GameRequest message is
     * received by the GameMaker and the Game is in READY
     * state, then a GameResponse with state CREATED is sent back
     * to the sender
     * */
    @Test
    public void gameRequestGameReadyTest() throws InterruptedException, GameNotFoundException {
        // receiving message
        Play play1 = new Play(userIds[0], usernames[0]);
        Play play2= new Play(userIds[1], usernames[1]);
        Play play3 = new Play(userIds[2], usernames[3]);
        Play play4 = new Play(userIds[3], usernames[3]);
        // testing
        gameMaker.tell(play1, ActorRef.noSender());
        gameMaker.tell(play2, ActorRef.noSender());
        Thread.sleep(3000);
        Game game = gameRepository.getNextGame();
        String gameId = game.gameId;
        gameMaker.tell(play3, ActorRef.noSender());
        gameMaker.tell(play4, ActorRef.noSender());
        Thread.sleep(3000);
        TestKit probe = new TestKit(system);
        gameMaker.tell(new GameRequest(gameId), probe.getRef());
        probe.expectMsg(Duration.ofSeconds(3), new GameResponse(GameResponse.CREATED));
    }

    /*
     * This method tests whether when a GameRequest message is
     * received by the GameMaker and the Game is in READY
     * state, then a Broker actor is sent with a
     * CreatePortfolios message
     * */
    @Test
    public void gameRequestGameReadyCreatePortfoliosTest() throws InterruptedException, GameNotFoundException {
        // receiving message
        Play play1 = new Play(userIds[0], usernames[0]);
        Play play2= new Play(userIds[1], usernames[1]);
        Play play3 = new Play(userIds[2], usernames[3]);
        Play play4 = new Play(userIds[3], usernames[3]);
        // testing
        gameMaker.tell(play1, ActorRef.noSender());
        gameMaker.tell(play2, ActorRef.noSender());
        Thread.sleep(3000);
        Game game = gameRepository.getNextGame();
        String gameId = game.gameId;
        gameMaker.tell(play3, ActorRef.noSender());
        gameMaker.tell(play4, ActorRef.noSender());
        Thread.sleep(3000);
        gameMaker.tell(new GameRequest(gameId), ActorRef.noSender());
        Map<String, String> marketUsers = new HashMap<>();
        for(int i = 0; i < 4; ++i){
            marketUsers.put(userIds[i],usernames[i]);
        }
        brokerProbe.expectMsg(Duration.ofSeconds(3), new CreatePortfolios(gameId,marketUsers));
    }

    /*
     * This method tests whether when a GameRequest message is
     * received by the GameMaker and the Game is in CREATED
     * state, then a GameResponse with state CREATED is sent back
     * to the sender
     * */
    @Test
    public void gameRequestGameCreatedTest() throws InterruptedException, GameNotFoundException {
        // receiving message
        Play play1 = new Play(userIds[0], usernames[0]);
        Play play2= new Play(userIds[1], usernames[1]);
        Play play3 = new Play(userIds[2], usernames[3]);
        Play play4 = new Play(userIds[3], usernames[3]);
        // testing
        gameMaker.tell(play1, ActorRef.noSender());
        gameMaker.tell(play2, ActorRef.noSender());
        Thread.sleep(1000);
        Game game = gameRepository.getNextGame();
        String gameId = game.gameId;
        gameMaker.tell(play3, ActorRef.noSender());
        gameMaker.tell(play4, ActorRef.noSender());
        Thread.sleep(1000);
        TestKit probe = new TestKit(system);
        gameMaker.tell(new GameRequest(gameId), ActorRef.noSender());
        Thread.sleep(1000);
        System.out.println(game.getState());
        gameMaker.tell(new GameRequest(gameId), probe.getRef());
        probe.expectMsg(Duration.ofSeconds(3), new GameResponse(GameResponse.CREATED));
    }

    /*
     * This method tests  whether when a GameRequest message is received
     * and the Game is in READY state, then a CreateMarket message is
     * sent to the MarketActor
     * */
    @Test
    public void gameRequestCreateMarketTest() throws InterruptedException, GameNotFoundException {
        // receiving message
        Play play1 = new Play(userIds[0], usernames[0]);
        Play play2= new Play(userIds[1], usernames[1]);
        Play play3 = new Play(userIds[2], usernames[3]);
        Play play4 = new Play(userIds[3], usernames[3]);
        // testing
        gameMaker.tell(play1, ActorRef.noSender());
        gameMaker.tell(play2, ActorRef.noSender());
        Thread.sleep(3000);
        Game game = gameRepository.getNextGame();
        String gameId = game.gameId;
        gameMaker.tell(play3, ActorRef.noSender());
        gameMaker.tell(play4, ActorRef.noSender());
        Thread.sleep(3000);
        gameMaker.tell(new GameRequest(gameId), ActorRef.noSender());
        Map<String, String> marketUsers = new HashMap<>();
        for(int i = 0; i < 4; ++i){
            marketUsers.put(userIds[i],usernames[i]);
        }
        marketProbe.expectMsg(Duration.ofSeconds(3), new CreateMarket(gameId, GameMaker.MAX_NUM_OF_TURNS));
    }

    /*
     * This method tests  whether when a GameRequest message is received
     * and the Game is in READY state, then a CreateGameClock message is
     * sent to the ClockActor
     * */
    @Test
    public void gameRequestCreateGameClockTest() throws InterruptedException, GameNotFoundException {
        // receiving message
        Play play1 = new Play(userIds[0], usernames[0]);
        Play play2= new Play(userIds[1], usernames[1]);
        Play play3 = new Play(userIds[2], usernames[3]);
        Play play4 = new Play(userIds[3], usernames[3]);
        // testing
        gameMaker.tell(play1, ActorRef.noSender());
        gameMaker.tell(play2, ActorRef.noSender());
        Thread.sleep(3000);
        Game game = gameRepository.getNextGame();
        String gameId = game.gameId;
        gameMaker.tell(play3, ActorRef.noSender());
        gameMaker.tell(play4, ActorRef.noSender());
        Thread.sleep(3000);
        gameMaker.tell(new GameRequest(gameId), ActorRef.noSender());
        Map<String, String> marketUsers = new HashMap<>();
        for(int i = 0; i < 4; ++i){
            marketUsers.put(userIds[i],usernames[i]);
        }
        clockProbe.expectMsg(
                Duration.ofSeconds(3),
                new CreateGameClock(gameId, Arrays.asList(userIds),
                        GameMaker.MAX_NUM_OF_TURNS));
    }

}