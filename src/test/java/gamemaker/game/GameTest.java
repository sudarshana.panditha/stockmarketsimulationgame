package gamemaker.game;

import gamemaker.exception.InvalidGameStateException;
import gamemaker.exception.PlayersOutOfBoundException;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.Test;

import java.util.Map;
import java.util.UUID;

import static org.junit.jupiter.api.Assertions.*;
/*
* This class contains the unit tests on
* Game class
* */
class GameTest {
    public static String userIds[];
    public static String usernames[];
    public static String extraUserId;
    public static String extraUsername;
    public static String gameId;

    @BeforeAll
    public static void setup(){
        userIds = new String[4];
        usernames = new String[4];
        for(int i = 0; i < 4; ++i){
            userIds[i] = UUID.randomUUID().toString();
            usernames[i] = "username" + (i+1);
        }
        extraUserId = UUID.randomUUID().toString();
        extraUsername = "extraUser";
        gameId = UUID.randomUUID().toString();
    }

    /*
    * This method is used to test whether when a Game
    * is just created, its initial state is Game.NOT_READY
    * */
    @Test
    public void initialStateNotReadyTest(){
        Game game = new Game(gameId);
        assertEquals(
                Game.NOT_READY,
                game.getState()
        );
    }

    /*
    * This method is used to test whether when a
    * player is added to a Game, then that player
    * is added into the set of players
    * */
    @Test
    public void addPlayerTest() throws PlayersOutOfBoundException {
        Game game = new Game(gameId);
        game.addPlayer(userIds[0], usernames[0]);

        assertEquals(
                true,
                game.getPlayers().containsKey(userIds[0])
        );
    }

    /*
    * This method tests whether when more than four
    * players are added to a Game, then it throws a
    * PlayersOutOfBoundsException
    * */
    @Test
    public void playersOutOfBoundsExceptionTest(){
        assertThrows(
                PlayersOutOfBoundException.class,
                () -> {
                    Game game = new Game(gameId);
                    for(int i = 0; i < 4; ++i){ // add four players
                        game.addPlayer(userIds[i], usernames[i]);
                    }
                    // now, add an extra player
                    game.addPlayer(extraUserId, extraUsername);
                }
        );
    }

    /*
    * This method is used to test whether when 4
    * players are added to a Game, then it state
    * transition from Game.Not_READY to Game.READY
    * */
    @Test void notReadyToReadyTest() throws PlayersOutOfBoundException {
        Game game = new Game(gameId);
        for(int i = 0; i < 4; ++i){
            game.addPlayer(userIds[i], usernames[i]);
        }

        assertEquals(
                Game.READY,
                game.getState()
        );
    }

    /*
    * This method tests whether when the Game is
    * just created in Game.NOT_READY state, and
    * setState is called with Game.CREATED, then
    * an InvalidGameStateException is thrown
    * */
    @Test void invalidStateFromNotReadyToCreatedTest(){
        assertThrows(
                InvalidGameStateException.class,
                () -> {
                    Game game = new Game(gameId);
                    game.setState(Game.CREATED);
                }
        );
    }

    /*
     * This method is used to test whether
     * when state is Game.Not_READY and setState
     * is called without adding any players
     * with Game.READY, then an InvalidGameStateException
     * is thrown
     * */
    @Test void invalidNotReadyToReadyTest(){
        assertThrows(
                InvalidGameStateException.class,
                () -> {
                    Game game = new Game(gameId);
                    game.setState(Game.READY);
                }
        );
    }

    /*
     * This method is used to test whether
     * when state is Game.READY and setState
     * is called with a previous state
     * Game.NOT_READY then an
     * InvalidGameStateException is thrown
     * */
    @Test void invalidReadyToNotReadyTest(){
        assertThrows(
                InvalidGameStateException.class,
                () -> {
                    Game game = new Game(gameId);

                    for(int i = 0; i < 4; ++i){
                        game.addPlayer(userIds[i], usernames[i]);
                    } // now state is Game.READY

                    game.setState(Game.NOT_READY);
                }
        );
    }

    /*
     * This method is used to test whether
     * when state is Game.CREATED and setState
     * is called with previous Game.READY, then an
     * InvalidGameStateException is thrown
     * */
    @Test void invalidCreatedToReadyTest(){
        assertThrows(
                InvalidGameStateException.class,
                () -> {
                    Game game = new Game(gameId);

                    for(int i = 0; i < 4; ++i){
                        game.addPlayer(userIds[i], usernames[i]);
                    } // now state is Game.READY

                    // now, state is Game.CREATED
                    game.setState(Game.CREATED);

                    // error transition
                    game.setState(Game.READY);
                }
        );
    }

    /*
     * This method is used to test whether
     * when state is Game.CREATED and setState
     * is called with previous state of Game.NOT_READY,
     * then an InvalidGameStateException
     * is thrown
     * */
    @Test void invalidCreatedToNotReadyTest(){
        assertThrows(
                InvalidGameStateException.class,
                () -> {
                    Game game = new Game(gameId);

                    for(int i = 0; i < 4; ++i){
                        game.addPlayer(userIds[i], usernames[i]);
                    } // now state is Game.READY

                    // now, state is Game.CREATED
                    game.setState(Game.CREATED);

                    // error transition
                    game.setState(Game.NOT_READY);
                }
        );
    }
}