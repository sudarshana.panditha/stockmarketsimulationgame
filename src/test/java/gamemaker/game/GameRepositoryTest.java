package gamemaker.game;

import gamemaker.exception.GameNotFoundException;
import gamemaker.exception.PlayersOutOfBoundException;
import org.junit.jupiter.api.Test;

import java.util.UUID;

import static org.junit.jupiter.api.Assertions.*;
/*
* This class contains unit tests on
* GameRepository class
* */
class GameRepositoryTest {

    /*
    * This method tests whether when createGame
    * is called on GameRepository, then it does
    * not return null
    * */
    @Test
    public void createGameTest(){
        GameRepository repository = new GameRepository();
        Game game = repository.createGame();

        assertNotNull(game);
    }

    /*
    * This method tests whether when getGame
    * is called with the gameId of an existing Game,
    * then it does not return null
    * */
    @Test
    public void getGameTest() throws GameNotFoundException {
        GameRepository repository = new GameRepository();
        Game game = repository.createGame();

        assertNotNull(repository.getGame(game.gameId));
    }

    /*
    * This method tests whether when getGame
    * is called with the gameId of a non existing
    * Game, then it throws a GameNotFoundException
    * */
    @Test
    public void getGameGameNotFoundTest(){
        assertThrows(
                GameNotFoundException.class,
                () -> {
                    GameRepository repository = new GameRepository();
                    // populate with Games
                    for(int i = 0; i < 100; ++i)
                        repository.createGame();

                    // getGame on fake gameId
                    repository.getGame("this is a fake game id");
                }
        );
    }

    /*
    * This method tests whether when nextGame is
    * called on a GameRepository that has a Game
    * in a NOT_READY state, then it does not return null
    * */
    @Test
    public void nextGameNotNullTest() throws GameNotFoundException {
        GameRepository repository = new GameRepository();
        repository.createGame(); // created a new Game, it is in NOT_READY

        assertNotNull(repository.getNextGame());
    }

    /*
    * This method tests whether when nextGame is called
    * on a GameRepository and the current nextGame
    * is in a state other than NOT_READY, then a
    * GameNotFoundException is thrown
    * */
    @Test void noNextGameTest(){
        assertThrows(
                GameNotFoundException.class,
                () -> {
                    GameRepository repository = new GameRepository();
                    Game game = repository.createGame(); // created a new Game, it is in NOT_READY

                    for(int i = 0 ; i < 4; ++i){
                        game.addPlayer(UUID.randomUUID().toString(), "user" + (i+1));
                    } // Game in READY state

                    // expect an exception here
                    Game nextGame = repository.getNextGame();
                }
        );
    }

}