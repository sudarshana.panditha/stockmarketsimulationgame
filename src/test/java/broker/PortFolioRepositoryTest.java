package broker;

import broker.exception.MarketNotFoundException;
import broker.exception.PortfolioNotFoundException;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.Test;

import java.util.UUID;

import static org.junit.jupiter.api.Assertions.*;
/*
* This class contains the unit tests
* on PortFolioRepository class
* */
class PortFolioRepositoryTest {
    public static String marketId;
    public static String userId;
    public static String name;

    @BeforeAll
    public static void setup(){
        marketId = UUID.randomUUID().toString();
        userId = UUID.randomUUID().toString();
        name = "Tony Stark";
    }

    /*
    * This method is used to test whether when
    * createPortfolio is called on PortFolioRepository
    * then a PortFolio is created with correct details
    * */
    @Test
    public void createPortfolioTest() throws PortfolioNotFoundException {
        PortFolioRepository repository = new PortFolioRepository();
        repository.createPortfolio(marketId,userId,name);

        Portfolio portfolio = repository.getPortfolio(marketId, userId);
        boolean isMatch = portfolio.marketId.equals(marketId)
                && portfolio.userId.equals(userId);
        assertEquals(
                true,
                isMatch
        );
    }

    /*
    * This method is used to test whether when
    * getPortfolio is called on PortFolioRepository
    * and a Portfolio is not found, a
    * PortfolioNotFoundException is thrown
    * */
    @Test
    public void portfolioNotFoundExceptionTest(){
        assertThrows(
                PortfolioNotFoundException.class,
                () -> {
                    PortFolioRepository repository = new PortFolioRepository();

                    Portfolio portfolio = repository.getPortfolio(marketId, userId);
                }
        );
    }

    /*
    * This method tests whether getPortfolios method does
    * not return null when there exist any Portfolios for
    * a given Market
    * */
    @Test
    public void getPortfoliosTest() throws MarketNotFoundException {
        PortFolioRepository repository = new PortFolioRepository();
        repository.createPortfolio(marketId,userId,name);

        assertNotNull(
                repository.getPortfolios(marketId)
        );
    }

    /*
     * This method tests whether getPortfolios method
     * throws MarketNotFoundException when no Portfolios
     * exists for a given Market
     * */
    @Test
    public void marketNotFoundExceptionTest(){
        assertThrows(
                MarketNotFoundException.class,
                () -> {
                    PortFolioRepository repository = new PortFolioRepository();
                    repository.getPortfolios(marketId);
                }
        );
    }
}