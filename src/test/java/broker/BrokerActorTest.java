package broker;

import akka.actor.ActorRef;
import akka.actor.ActorSystem;
import akka.actor.Props;
import akka.testkit.javadsl.TestKit;
import bank.messages.*;
import broker.exception.CompanyNotFoundException;
import broker.exception.MarketNotFoundException;
import broker.exception.PortfolioNotFoundException;
import broker.messages.*;
import broker.request.Request;
import broker.request.RequestRepository;
import market.CompanyDetails;
import market.MarketRepository;
import market.exception.IllegalArgumentException;
import market.messages.CompanyListRequest;
import market.messages.CompanyListResponse;
import market.messages.SharePriceRequest;
import market.messages.SharePriceResponse;
import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import java.time.Duration;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.UUID;
import java.util.concurrent.atomic.AtomicBoolean;
import java.util.concurrent.atomic.AtomicReference;

import static org.junit.jupiter.api.Assertions.*;
/*
* This class contains the state and behavior
* tests on the BrokerActor class
* */
class BrokerActorTest {
    public static ActorSystem system;
    public static ActorRef brokerActor;
    public static TestKit marketProbe;
    public static TestKit bankProbe;
    public static String marketId;
    public static String userIds[];
    public static String usernames[];
    public static PortFolioRepository portFolioRepository;
    public static RequestRepository requestRepository;
    public static String sectorId;
    public static String companyId;

    @BeforeAll
    public static void setup(){
        marketId = UUID.randomUUID().toString();
        sectorId = UUID.randomUUID().toString();
        companyId = UUID.randomUUID().toString();
        userIds = new String[4];
        usernames = new String[4];
        for (int i = 0; i < 4; ++i){
            userIds[i] = UUID.randomUUID().toString();
            usernames[i] = "username" + (i+1);
        }
        portFolioRepository = new PortFolioRepository();
        requestRepository = new RequestRepository();

        system = ActorSystem.create();
        marketProbe = new TestKit(system);
        bankProbe = new TestKit(system);
        Props brokerProps = Props.create(
                BrokerActor.class,
                marketProbe.getRef(),
                bankProbe.getRef(),
                portFolioRepository,
                requestRepository
        );
        brokerActor = system.actorOf(brokerProps);
    }

    @AfterEach
    public void reset(){
        TestKit.shutdownActorSystem(system);
        system = null;
    }

    @BeforeEach
    public void reSetup(){
        setup();
    }

    /*
     * This method is used to test whether when a
     * BrokerActor receives a CreatePortfolios message
     * then it sends a CompanyListRequest message to its
     * reference to the MarketActor
     * */
    @Test
    public void companyListRequestTest() throws InterruptedException {
        // message data
        Map<String, String> userDetails = new HashMap<>();
        for(int i = 0; i < 4; ++i){
            userDetails.put(userIds[i], usernames[i]);
        }
        // sending message
        CreatePortfolios msg = new CreatePortfolios(marketId, userDetails);
        // Testing BrokerActor
        brokerActor.tell(msg, ActorRef.noSender());
        marketProbe.expectMsg(Duration.ofSeconds(5), new CompanyListRequest(marketId));
    }

    /*
    * This method is used to test whether when a
    * BrokerActor receives a CreatePortfolios message
    * then it creates Portfolios for all the given
    * Investors
    * */
    @Test
    public void createPortfoliosTest() throws InterruptedException {
        // message data
        Map<String, String> userDetails = new HashMap<>();
        for(int i = 0; i < 4; ++i){
            userDetails.put(userIds[i], usernames[i]);
        }
        // sending message
        CreatePortfolios msg = new CreatePortfolios(marketId, userDetails);
        // Testing BrokerActor
        brokerActor.tell(msg, ActorRef.noSender());
        // wait for BrokerActor to process...
        Thread.sleep(5000);
        boolean allFound = true;
        for(String  userId: userIds){
            try {
                portFolioRepository.getPortfolio(marketId,userId);
            } catch (PortfolioNotFoundException e) {
                allFound = false;
            }
        }
        assertEquals(
                true,
                allFound
        );
    }

    /*
    * This method tests whether when a BrokerActor receives a
    * Sell message and there are enough shares to sell, then
    * the MarketActor is sent with a SharePriceRequest message
    * */
    @Test
    public void sharePriceRequestForSellTest() throws PortfolioNotFoundException, InterruptedException {
        portFolioRepository.createPortfolio(marketId, userIds[0],usernames[0]);
        Portfolio portfolio =portFolioRepository.getPortfolio(marketId, userIds[0]);
        portfolio.addCompany(sectorId, companyId);
        portfolio.increaseShares(companyId, 100L);
        // sell message
        Sell sell = new Sell(marketId, userIds[0],companyId, 50L);
        // testing the actor
        brokerActor.tell(sell, ActorRef.noSender());
        marketProbe.expectMsg(Duration.ofSeconds(5), new SharePriceRequest("", marketId, companyId));
    }

    /*
     * This method tests whether when a BrokerActor receives a
     * Sell message and there are enough shares to sell, then
     * the sender is sent with a Chit message with status as
     * SUCCESS
     * */
    @Test
    public void chitForSuccessSellTest() throws PortfolioNotFoundException, InterruptedException {
        portFolioRepository.createPortfolio(marketId, userIds[0],usernames[0]);
        Portfolio portfolio =portFolioRepository.getPortfolio(marketId, userIds[0]);
        portfolio.addCompany(sectorId, companyId);
        portfolio.increaseShares(companyId, 100L);
        // sell message
        Sell sell = new Sell(marketId, userIds[0],companyId, 50L);
        // testing the actor
        TestKit probe = new TestKit(system);
        brokerActor.tell(sell, probe.getRef());
        probe.expectMsg(Duration.ofSeconds(5), new Chit(Chit.SUCCESS, "", ""));
    }

    /*
     * This method tests whether when a BrokerActor receives a
     * Sell message and there are not enough shares to sell, then
     * the sender is sent with a Chit message with status as
     * Chit.ERROR
     * */
    @Test
    public void chitForErrorSellTest() throws PortfolioNotFoundException, InterruptedException {
        portFolioRepository.createPortfolio(marketId, userIds[0],usernames[0]);
        Portfolio portfolio =portFolioRepository.getPortfolio(marketId, userIds[0]);
        portfolio.addCompany(sectorId, companyId);
        portfolio.increaseShares(companyId, 100L);
        // sell message
        Sell sell = new Sell(marketId, userIds[0],companyId, 150L);
        // testing the actor
        TestKit probe = new TestKit(system);
        brokerActor.tell(sell, probe.getRef());
        probe.expectMsg(Duration.ofSeconds(5), new Chit(Chit.ERROR, "", ""));
    }

    /*
     * This method tests whether when a BrokerActor receives a
     * Buy message sender is sent with a Chit message with
     * status as SUCCESS
     * */
    @Test
    public void chitForSuccessBuyTest() throws PortfolioNotFoundException, InterruptedException {
        portFolioRepository.createPortfolio(marketId, userIds[0],usernames[0]);
        Portfolio portfolio =portFolioRepository.getPortfolio(marketId, userIds[0]);
        portfolio.addCompany(sectorId, companyId);
        portfolio.increaseShares(companyId, 100L);
        // sell message
        Buy buy = new Buy(marketId, userIds[0],companyId, 50L);
        // testing the actor
        TestKit probe = new TestKit(system);
        brokerActor.tell(buy, probe.getRef());
        probe.expectMsg(Duration.ofSeconds(5), new Chit(Chit.SUCCESS, "", ""));
    }

    /*
     * This method tests whether when a BrokerActor receives a
     * Buy message and then MarketActor is sent with a
     * SharePriceRequest message
     * */
    @Test
    public void sharePriceRequestForBuyTest() throws PortfolioNotFoundException, InterruptedException {
        portFolioRepository.createPortfolio(marketId, userIds[0],usernames[0]);
        Portfolio portfolio =portFolioRepository.getPortfolio(marketId, userIds[0]);
        portfolio.addCompany(sectorId, companyId);
        // sell message
        Buy buy = new Buy(marketId, userIds[0],companyId, 50L);
        // testing the actor
        brokerActor.tell(buy, ActorRef.noSender());
        marketProbe.expectMsg(Duration.ofSeconds(5), new SharePriceRequest("", marketId, companyId));
    }


    /*
     * This method tests whether when a BrokerActor receives a
     * SharePriceResponse message and the type of request is BUY,
     * then a Withdraw message is sent to the BankActor
     * */
    @Test
    public void sharePriceResponseForBuyWithdrawTest() throws PortfolioNotFoundException, InterruptedException {
        portFolioRepository.createPortfolio(marketId, userIds[0],usernames[0]);
        Portfolio portfolio =portFolioRepository.getPortfolio(marketId, userIds[0]);
        portfolio.addCompany(sectorId, companyId);
        // sell message
        Buy buy = new Buy(marketId, userIds[0],companyId, 50L);
        brokerActor.tell(buy, ActorRef.noSender());
        Thread.sleep(3000);
        // get the set of Requests
        Map<String,Request> requestMap = requestRepository.getRequests();
        AtomicReference<Request> request = new AtomicReference<>();
        requestMap.forEach(
                (requestId, req) -> {
                    if(req.userId.equals(userIds[0])){
                        request.set(req);
                    }
                }
        );
        // sending message
        SharePriceResponse res = new SharePriceResponse(request.get().requestId, 10);
        brokerActor.tell(res, ActorRef.noSender());
        // expecting message
        Withdraw withdraw = new Withdraw(request.get().requestId, portfolio.getAccountId(), 50L);
        bankProbe.expectMsg(Duration.ofSeconds(3), withdraw);
    }


    /*
     * This method tests whether when a BrokerActor receives a
     * SharePriceResponse message and the type of request is SELL,
     * then a Deposit message is sent to the BankActor
     * */
    @Test
    public void sharePriceResponseForSellDepositTest() throws PortfolioNotFoundException, InterruptedException {
        portFolioRepository.createPortfolio(marketId, userIds[0],usernames[0]);
        Portfolio portfolio =portFolioRepository.getPortfolio(marketId, userIds[0]);
        portfolio.addCompany(sectorId, companyId);
        portfolio.increaseShares(companyId, 100L);
        // sell message
        Sell sell = new Sell(marketId, userIds[0],companyId, 50L);
        brokerActor.tell(sell, ActorRef.noSender());
        Thread.sleep(3000);
        // get the set of Requests
        Map<String,Request> requestMap = requestRepository.getRequests();
        AtomicReference<Request> request = new AtomicReference<>();
        requestMap.forEach(
                (requestId, req) -> {
                    if(req.userId.equals(userIds[0])){
                        request.set(req);
                    }
                }
        );
        // sending message
        SharePriceResponse res = new SharePriceResponse(request.get().requestId, 10);
        brokerActor.tell(res, ActorRef.noSender());
        // expecting message
        Deposit deposit = new Deposit(request.get().requestId, portfolio.getAccountId(), 50L*10);
        bankProbe.expectMsg(Duration.ofSeconds(3), deposit);
    }

    /*
    * This method tests whether when an Ack message is
    * received by the BrokerActor from the Bank for a Request
    * of type BUY, then the state of the Request becomes COMPLETED
    * */
    @Test
    public void AckForBuyTest() throws PortfolioNotFoundException, InterruptedException {
        portFolioRepository.createPortfolio(marketId, userIds[0],usernames[0]);
        Portfolio portfolio =portFolioRepository.getPortfolio(marketId, userIds[0]);
        portfolio.addCompany(sectorId, companyId);
        // sell message
        Buy buy = new Buy(marketId, userIds[0],companyId, 50L);
        brokerActor.tell(buy, ActorRef.noSender());
        Thread.sleep(3000);
        // get the set of Requests
        Map<String,Request> requestMap = requestRepository.getRequests();
        AtomicReference<Request> request = new AtomicReference<>();
        requestMap.forEach(
                (requestId, req) -> {
                    if(req.userId.equals(userIds[0])){
                        request.set(req);
                    }
                }
        );
        SharePriceResponse res = new SharePriceResponse(request.get().requestId, 10);
        brokerActor.tell(res, ActorRef.noSender());
        Thread.sleep(1000);
        // sending message
        Ack ack = new Ack(request.get().requestId);
        brokerActor.tell(ack, ActorRef.noSender());
        Thread.sleep(3000);
        assertEquals(
                Request.COMPLETED,
                request.get().getState()
        );
    }

    /*
    * This method tests whether when an Ack message is
    * received by the BrokerActor from the Bank for a Request
    * of type SELL, then the state of the Request becomes COMPLETED
    * */
    @Test
    public void AckForSellTest() throws PortfolioNotFoundException, InterruptedException {
        portFolioRepository.createPortfolio(marketId, userIds[0],usernames[0]);
        Portfolio portfolio =portFolioRepository.getPortfolio(marketId, userIds[0]);
        portfolio.addCompany(sectorId, companyId);
        portfolio.increaseShares(companyId, 100L);
        // sell message
        Sell sell = new Sell(marketId, userIds[0],companyId, 50L);
        brokerActor.tell(sell, ActorRef.noSender());
        Thread.sleep(3000);
        // get the set of Requests
        Map<String,Request> requestMap = requestRepository.getRequests();
        AtomicReference<Request> request = new AtomicReference<>();
        requestMap.forEach(
                (requestId, req) -> {
                    if(req.userId.equals(userIds[0])){
                        request.set(req);
                    }
                }
        );
        // sending message
        SharePriceResponse res = new SharePriceResponse(request.get().requestId, 10);
        brokerActor.tell(res, ActorRef.noSender());
        Thread.sleep(1000);
        // sending message
        Ack ack = new Ack(request.get().requestId);
        brokerActor.tell(ack, ActorRef.noSender());
        Thread.sleep(3000);
        assertEquals(
                Request.COMPLETED,
                request.get().getState()
        );
    }

    /*
     * This method tests whether when an UnAck message is
     * received by the BrokerActor from the Bank for a Request
     * of type BUY, then the state of the Request becomes FAILED
     * */
    @Test
    public void UnAckForBuyTest() throws PortfolioNotFoundException, InterruptedException {
        portFolioRepository.createPortfolio(marketId, userIds[0],usernames[0]);
        Portfolio portfolio =portFolioRepository.getPortfolio(marketId, userIds[0]);
        portfolio.addCompany(sectorId, companyId);
        // sell message
        Buy buy = new Buy(marketId, userIds[0],companyId, 50L);
        brokerActor.tell(buy, ActorRef.noSender());
        Thread.sleep(3000);
        // get the set of Requests
        Map<String,Request> requestMap = requestRepository.getRequests();
        AtomicReference<Request> request = new AtomicReference<>();
        requestMap.forEach(
                (requestId, req) -> {
                    if(req.userId.equals(userIds[0])){
                        request.set(req);
                    }
                }
        );
        SharePriceResponse res = new SharePriceResponse(request.get().requestId, 10);
        brokerActor.tell(res, ActorRef.noSender());
        Thread.sleep(1000);
        // sending message
        UnAck unAck = new UnAck(request.get().requestId);
        brokerActor.tell(unAck, ActorRef.noSender());
        Thread.sleep(3000);
        assertEquals(
                Request.FAILED,
                request.get().getState()
        );
    }

    /*
     * This method is used to test whether when a
     * BrokerActor receives a CreatePortfolios message
     * then it sends a CompanyListRequest message to its
     * reference to the MarketActor
     * */
    @Test
    public void createAccountTest() throws InterruptedException, PortfolioNotFoundException {
        // message data
        Map<String, String> userDetails = new HashMap<>();
        for(int i = 0; i < 4; ++i){
            userDetails.put(userIds[i], usernames[i]);
        }
        // sending message
        CreatePortfolios msg = new CreatePortfolios(marketId, userDetails);
        // Testing BrokerActor
        brokerActor.tell(msg, ActorRef.noSender());
        bankProbe.expectMsg(Duration.ofSeconds(5), new CreateAccount(userIds[0], usernames[0], 1000));
    }

    /*
    * This method tests whether when a BrokerActor
    * receives an AccountCreated message for a previously
    * sent CreateAccount message, then it sets the accountId
    * in the user's Portfolio
    * */
    @Test
    public void accountCreatedTest() throws InterruptedException, PortfolioNotFoundException {
        // message data and data for account ids
        String accountIds[] = new String[4];
        AccountCreated created [] = new AccountCreated[4];
        Map<String, String> userDetails = new HashMap<>();
        for(int i = 0; i < 4; ++i){
            accountIds[i] = UUID.randomUUID().toString();
            created[i] = new AccountCreated(userIds[i], accountIds[i]);
            userDetails.put(userIds[i], usernames[i]);
        }
        CreatePortfolios msg = new CreatePortfolios(marketId, userDetails);
        // Testing BrokerActor
        brokerActor.tell(msg, ActorRef.noSender());
        Thread.sleep(3000);

        // sending message
        for (int i = 0; i < 4; ++i){
            brokerActor.tell(created[i], ActorRef.noSender());
        }

        Thread.sleep(3000);
        boolean areAllAccountIdsSet = true;

        for (int i = 0; i < 4; ++i){
            Portfolio portfolio = portFolioRepository.getPortfolio(marketId, userIds[i]);
            if(portfolio.getAccountId() == null){
                areAllAccountIdsSet = false;
                break;
            }
        }

        assertEquals(
                true,
                areAllAccountIdsSet
        );
    }

    /*
     * This method tests whether when a BrokerActor
     * receives an AccountNotCreated message for a previously
     * sent CreateAccount message, then it sends back a
     * CreateAccount message again
     * */
    @Test
    public void accountNotCreatedTest() throws InterruptedException, PortfolioNotFoundException {
        // message data and data for account ids
        String accountIds[] = new String[4];
        Map<String, String> userDetails = new HashMap<>();
        for(int i = 0; i < 4; ++i){
            accountIds[i] = UUID.randomUUID().toString();
            userDetails.put(userIds[i], usernames[i]);
        }
        CreatePortfolios msg = new CreatePortfolios(marketId, userDetails);
        // Testing BrokerActor
        brokerActor.tell(msg, ActorRef.noSender());
        Thread.sleep(3000);

        // sending message
        brokerActor.tell(new AccountNotCreated(userIds[0]), ActorRef.noSender());
        Thread.sleep(10000);
        bankProbe.expectMsg(Duration.ofSeconds(3),new CreateAccount(userIds[0], usernames[0],
                BrokerActor.INITIAL_DEPOSIT));
    }

    /*
    * This message tests whether when BrokerActor receives a
    * CompanyListResponse message, then it sets the company list
    * for each players in the particular Market
    * */
    @Test
    public void addPortfolioCompanyListTest() throws InterruptedException, IllegalArgumentException, MarketNotFoundException {
        // message data
        Map<String, String> userDetails = new HashMap<>();
        for(int i = 0; i < 4; ++i){
            userDetails.put(userIds[i], usernames[i]);
        }
        CreatePortfolios msg = new CreatePortfolios(marketId, userDetails);
        // Testing BrokerActor
        brokerActor.tell(msg, ActorRef.noSender());
        Thread.sleep(3000);
        MarketRepository repository = new MarketRepository();
        repository.createMarket(marketId, 100);
        List<CompanyDetails> companyDetails = repository.getCompanyDetailsList(marketId);
        brokerActor.tell(new CompanyListResponse(marketId, companyDetails), ActorRef.noSender());
        Thread.sleep(3000);

        AtomicBoolean allCompaniesFound = new AtomicBoolean(true);

        Map<String, Portfolio> portfolioMap = portFolioRepository.getPortfolios(marketId);
        portfolioMap.forEach(
                (userId, portfolio) -> {

                    for(CompanyDetails details: companyDetails){
                        try{
                            portfolio.getNumberOfShares(details.companyId);
                        }catch(CompanyNotFoundException e){
                            allCompaniesFound.set(false);
                        }
                    }
                }
        );

        assertEquals(
                true,
                allCompaniesFound.get()
        );

    }

    /* This method tests whether when an AllPortfoliosRequest
    * is received by the BrokerActor then an AllPortfoliosResponse
    * is sent back
    * */
    @Test
    public void allPortfoliosRequestResponse() throws InterruptedException {
        Map<String, String> userDetails = new HashMap<>();
        for(int i = 0; i < 4; ++i){
            userDetails.put(userIds[i], usernames[i]);
        }
        CreatePortfolios msg = new CreatePortfolios(marketId, userDetails);
        brokerActor.tell(msg, ActorRef.noSender());
        Thread.sleep(3000);
        // now, there is a collection of Portfolios for marketId
        TestKit probe = new TestKit(system);
        brokerActor.tell(
                new AllPortfoliosRequest(marketId),
                probe.getRef()
        );
        probe.expectMsg(
                Duration.ofSeconds(3),
                new AllPortFoliosResponse(marketId, null)
        );
    }

    /*
    * This method tests whether when a BrokerActor receives a
    * HandOverChit message and the Request is COMPLETED then
    * a ChitVerification message with state as COMPLETED is
    * sent back
    * */
    @Test
    public void handOverChitCompletedTest() throws PortfolioNotFoundException, InterruptedException {
        portFolioRepository.createPortfolio(marketId, userIds[0],usernames[0]);
        Portfolio portfolio =portFolioRepository.getPortfolio(marketId, userIds[0]);
        portfolio.addCompany(sectorId, companyId);
        portfolio.increaseShares(companyId, 100L);
        // sell message
        Sell sell = new Sell(marketId, userIds[0],companyId, 50L);
        brokerActor.tell(sell, ActorRef.noSender());
        Thread.sleep(3000);
        // get the set of Requests
        Map<String,Request> requestMap = requestRepository.getRequests();
        AtomicReference<Request> request = new AtomicReference<>();
        requestMap.forEach(
                (requestId, req) -> {
                    if(req.userId.equals(userIds[0])){
                        request.set(req);
                    }
                }
        );
        SharePriceResponse res = new SharePriceResponse(request.get().requestId, 10);
        brokerActor.tell(res, ActorRef.noSender());
        Thread.sleep(1000);
        Ack ack = new Ack(request.get().requestId);
        brokerActor.tell(ack, ActorRef.noSender());
        Thread.sleep(3000);
        // now, Request state is in COMPLETED
        // testing BrokerActor
        TestKit probe = new TestKit(system);
        // sending message
        HandOverChit handOverChit = new HandOverChit(request.get().requestId);
        brokerActor.tell(handOverChit, probe.getRef());

        probe.expectMsg(
                Duration.ofSeconds(3),
                new ChitVerification(ChitVerification.COMPLETED,"", request.get().requestId)
        );
    }

    /*
     * This method tests whether when a BrokerActor receives a
     * HandOverChit message and the Request is FAILED then
     * a ChitVerification message with state as FAILED is
     * sent back
     * */
    @Test
    public void handOverChitFailedRequestTest() throws PortfolioNotFoundException, InterruptedException {
        portFolioRepository.createPortfolio(marketId, userIds[0],usernames[0]);
        Portfolio portfolio =portFolioRepository.getPortfolio(marketId, userIds[0]);
        portfolio.addCompany(sectorId, companyId);
        portfolio.increaseShares(companyId, 100L);
        // sell message
        Sell sell = new Sell(marketId, userIds[0],companyId, 50L);
        brokerActor.tell(sell, ActorRef.noSender());
        Thread.sleep(3000);
        // get the set of Requests
        Map<String,Request> requestMap = requestRepository.getRequests();
        AtomicReference<Request> request = new AtomicReference<>();
        requestMap.forEach(
                (requestId, req) -> {
                    if(req.userId.equals(userIds[0])){
                        request.set(req);
                    }
                }
        );
        SharePriceResponse res = new SharePriceResponse(request.get().requestId, 10);
        brokerActor.tell(res, ActorRef.noSender());
        Thread.sleep(1000);
        UnAck unAck = new UnAck(request.get().requestId); // send UnAck to fail Request
        brokerActor.tell(unAck, ActorRef.noSender());
        Thread.sleep(3000);
        // now, Request state is in COMPLETED
        // testing BrokerActor
        TestKit probe = new TestKit(system);
        // sending message
        HandOverChit handOverChit = new HandOverChit(request.get().requestId);
        brokerActor.tell(handOverChit, probe.getRef());

        probe.expectMsg(
                Duration.ofSeconds(3),
                new ChitVerification(ChitVerification.FAILED,"", request.get().requestId)
        );
    }

    /*
    * This method tests whether when a BrokerActor receives a
    * HandOverChit message and the Request is in a state other than
    * FAILED or COMPLETED, then a ChitVerification with WAIT state
    * is sent back
    * */
    @Test
    public void handOverChitTest() throws PortfolioNotFoundException, InterruptedException {
        portFolioRepository.createPortfolio(marketId, userIds[0],usernames[0]);
        Portfolio portfolio =portFolioRepository.getPortfolio(marketId, userIds[0]);
        portfolio.addCompany(sectorId, companyId);
        portfolio.increaseShares(companyId, 100L);
        // sell message
        Buy buy = new Buy(marketId, userIds[0],companyId, 50L);

        brokerActor.tell(buy, ActorRef.noSender());
        Thread.sleep(3000);
        // get the set of Requests
        Map<String,Request> requestMap = requestRepository.getRequests();
        AtomicReference<Request> request = new AtomicReference<>(); // the Request
        requestMap.forEach(
                (requestId, req) -> {
                    if(req.userId.equals(userIds[0])){
                        request.set(req);
                    }
                }
        );

        TestKit probe = new TestKit(system);
        // sending message
        HandOverChit handOverChit = new HandOverChit(request.get().requestId);
        brokerActor.tell(handOverChit, probe.getRef());

        probe.expectMsg(
                Duration.ofSeconds(3),
                new ChitVerification(ChitVerification.WAIT,"", request.get().requestId)
        );

    }

}