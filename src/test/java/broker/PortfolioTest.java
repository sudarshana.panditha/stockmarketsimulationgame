package broker;

import broker.exception.CompanyNotFoundException;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.Test;

import java.util.Map;
import java.util.UUID;

import static org.junit.jupiter.api.Assertions.*;

class PortfolioTest {
    public static String marketId;
    public static String userId;
    public static String name;
    public static String sectorId;
    public static String companyId;

    @BeforeAll
    public static void setup(){
        marketId = UUID.randomUUID().toString();
        userId = UUID.randomUUID().toString();
        name = "Tony Stark";
        sectorId = UUID.randomUUID().toString();
        companyId = UUID.randomUUID().toString();
    }

    /*
    * This method is used to test whether when a new
    * Company is added to the Portfolio using the
    * addCompany method it is saved in the PortFolio
    * */
    @Test
    public void addCompanyTest(){
        Portfolio portfolio = new Portfolio(marketId,userId,name);
        portfolio.addCompany(sectorId, companyId);

        Map<String, Long> companySharesMap = portfolio.getSectorCompanyShares().get(sectorId);

        assertEquals(
                true,
                companySharesMap.containsKey(companyId)
        );

    }

    /*
     * This method is used to test whether when a new
     * Company is added to the Portfolio using the
     * addCompany method the initial number of shares
     * in the Company is zero
     * */
    @Test
    public void addCompanyInitialSharesTest(){
        Portfolio portfolio = new Portfolio(marketId,userId,name);
        portfolio.addCompany(sectorId, companyId);

        Map<String, Long> companySharesMap = portfolio.getSectorCompanyShares().get(sectorId);

        assertEquals(
                0L,
                companySharesMap.get(companyId).longValue()
        );

    }

    /*
     * This method is used to test whether when
     * getShares is called it returns the correct
     * number of shares of the given Company
     * */
    @Test
    public void getSharesTest() throws market.exception.CompanyNotFoundException, CompanyNotFoundException {
        Portfolio portfolio = new Portfolio(marketId,userId,name);
        portfolio.addCompany(sectorId, companyId);

        long numberOfShares = portfolio.getNumberOfShares(companyId);

        assertEquals(
                0L,
                numberOfShares
        );

    }

    /*
     * This method is used to test whether when a
     * call to increaseShares is called it increases
     * the the number of shares of the given Company by
     * the given amount
     * */
    @Test
    public void increaseSharesTest() throws market.exception.CompanyNotFoundException, CompanyNotFoundException {
        Portfolio portfolio = new Portfolio(marketId,userId,name);
        portfolio.addCompany(sectorId, companyId);

        portfolio.increaseShares(companyId, 100L);

        assertEquals(
                100L,
                portfolio.getNumberOfShares(companyId)
        );

    }

    /*
     * This method is used to test whether when a
     * call to decreaseShares is called it decreases
     * the the number of shares of the given Company by
     * the given amount
     * */
    @Test
    public void decreaseSharesTest() throws market.exception.CompanyNotFoundException, CompanyNotFoundException {
        Portfolio portfolio = new Portfolio(marketId,userId,name);
        portfolio.addCompany(sectorId, companyId);

        portfolio.increaseShares(companyId, 100L);
        portfolio.decreaseShares(companyId, 40L);

        assertEquals(
                60L,
                portfolio.getNumberOfShares(companyId)
        );

    }

    /*
    * This method tests whether when a call to
    * getShares is called with an invalid company
    * id, then a CompanyNotFoundException is called
    * */
    @Test
    public void companyNotFoundTest(){
        assertThrows(
                CompanyNotFoundException.class,
                () -> {
                    Portfolio portfolio = new Portfolio(marketId,userId,name);
                    portfolio.addCompany(sectorId, companyId); // so there is at least one company

                    portfolio.getNumberOfShares("fakeCompanyId");
                }
        );
    }
}