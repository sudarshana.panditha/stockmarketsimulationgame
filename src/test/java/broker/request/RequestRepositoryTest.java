package broker.request;

import broker.exception.RequestNotFoundException;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.Test;

import java.util.UUID;

import static org.junit.jupiter.api.Assertions.*;
/*
* This class contains the unit tests on
* RequestRepository class
* */
class RequestRepositoryTest {
    public static String fakeRequestId;
    public static String marketId;
    public static String userId;
    public static String companyId;
    public static String type;
    public static long shares;

    @BeforeAll
    public static void setup(){
        fakeRequestId = UUID.randomUUID().toString();
        marketId = UUID.randomUUID().toString();
        userId = UUID.randomUUID().toString();
        companyId = UUID.randomUUID().toString();
        type = Request.SELL;
        shares = 1000L;
    }

    /*
    * This method tests whether when createRequest
    * is called on a RequestRepository, then it creates
    * a Request and returns it, but not null
    * */
    @Test
    public void createRequestTest(){
        RequestRepository repository = new RequestRepository();
        Request request = repository.createRequest(
                marketId, userId, companyId, type, shares);
        assertNotNull(request);
    }

    /*
     * This method tests whether when getRequest
     * is called on a RequestRepository for a valid
     * request id then it returns the Request,
     * but not null
     * */
    @Test
    public void getRequestTest() throws RequestNotFoundException {
        RequestRepository repository = new RequestRepository();
        Request createdRequest = repository.createRequest(
                marketId, userId, companyId, type, shares);
        // the test
        Request request = repository.getRequest(createdRequest.requestId);
        assertNotNull(request);
    }

    /*
     * This method tests whether when getRequest
     * is called on a RequestRepository for an
     * invalid request id then it throws a
     * RequestNotFoundException
     * */
    @Test
    public void getRequestForInvalidRequestIdTest(){
        assertThrows(
                RequestNotFoundException.class,
                () -> {
                    RequestRepository repository = new RequestRepository();
                    // so that actual an request exist in repository
                    repository.createRequest(
                            marketId, userId, companyId, type, shares);
                    // the test
                    Request request = repository.getRequest(fakeRequestId);
                }
        );
    }
}