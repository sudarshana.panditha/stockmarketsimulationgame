package broker.request;

import broker.exception.InvalidStateTransitionException;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.Test;

import java.util.UUID;

import static org.junit.jupiter.api.Assertions.*;
/*
* This class contains the unit tests on
* the Request class
* */
class RequestTest {
    public static String requestId;
    public static String marketId;
    public static String userId;
    public static String companyId;
    public static String type;
    public static long shares;

    @BeforeAll
    public static void setup(){
        requestId = UUID.randomUUID().toString();
        marketId = UUID.randomUUID().toString();
        userId = UUID.randomUUID().toString();
        companyId = UUID.randomUUID().toString();
        type = Request.SELL;
        shares = 1000L;
    }

    /*
    * This method tests whether when a Request is created,
    * then its initial state equals Request.CREATED
    * */
    @Test
    public void initialCreatedStateTest(){
        Request request = new Request(
                requestId,
                marketId,
                userId,
                companyId,
                type,
                shares
        );

        assertEquals(
                Request.CREATED,
                request.getState()
        );
    }

    /*
    * This method is used to test whether when a
    * setState is called on a just created Request
    * with a state of Request.SHARE_PRICE_CHECK
    * then it does not throw any Exception
    * */
    @Test
    public void validTransitionFromCreatedTest(){
        assertDoesNotThrow(
                () -> {
                    Request request = new Request( // create new Request {CREATED}
                            requestId,
                            marketId,
                            userId,
                            companyId,
                            type,
                            shares
                    );
                }
        );
    }

    /*
     * This method is used to test whether when a
     * setState is called on a just created Request
     * with a state of Request.BANK_ACCOUNT_CHECK
     * then it throws an InvalidStateTransitionException
     * */
    @Test
    public void invalidTransitionFromCreatedToBankAccountCheckTest(){
        assertThrows(
                InvalidStateTransitionException.class,
                () -> {
                    Request request = new Request( // create new Request {CREATED}
                            requestId,
                            marketId,
                            userId,
                            companyId,
                            type,
                            shares
                    );
                    // set invalid state
                    request.setState(Request.BANK_ACCOUNT_CHECK);
                }
        );
    }

    /*
     * This method is used to test whether when a
     * setState is called on a just created Request
     * with a state of Request.COMPLETED
     * then it throws an InvalidStateTransitionException
     * */
    @Test
    public void invalidTransitionFromCreatedToCompletedTest(){
        assertThrows(
                InvalidStateTransitionException.class,
                () -> {
                    Request request = new Request( // create new Request {CREATED}
                            requestId,
                            marketId,
                            userId,
                            companyId,
                            type,
                            shares
                    );
                    // set invalid state
                    request.setState(Request.COMPLETED);
                }
        );
    }

    /*
     * This method is used to test whether when a
     * setState is called on a just created Request
     * with a state of Request.COMPLETED
     * then it throws an InvalidStateTransitionException
     * */
    @Test
    public void invalidTransitionFromCreatedToFailedTest(){
        assertThrows(
                InvalidStateTransitionException.class,
                () -> {
                    Request request = new Request( // create new Request {CREATED}
                            requestId,
                            marketId,
                            userId,
                            companyId,
                            type,
                            shares
                    );
                    // set invalid state
                    request.setState(Request.FAILED);
                }
        );
    }

    /*
     * This method is used to test whether when a
     * setState is called on a Request currently in
     * Request.SHARE_PRICE_CHECK state with a state of
     * Request.BANK_ACCOUNT_CHECK, the no Exception
     * is thrown
     * */
    @Test
    public void validTransitionFromSharePriceCheckToBankAccountCheckTest(){
        assertDoesNotThrow(
                () -> {
                    Request request = new Request( // create new Request {CREATED}
                            requestId,
                            marketId,
                            userId,
                            companyId,
                            type,
                            shares
                    );
                    // from created to share price check
                    request.setState(Request.SHARE_PRICE_CHECK);

                    // next valid state
                    request.setState(Request.BANK_ACCOUNT_CHECK);
                }
        );
    }

    /*
     * This method is used to test whether when a
     * setState is called on a Request currently in
     * Request.SHARE_PRICE_CHECK state with a state of
     * Request.COMPLETED then it throws an
     * InvalidStateTransitionException
     * */
    @Test
    public void invalidTransitionFromSharePriceCheckToCompletedTest(){
        assertThrows(
                InvalidStateTransitionException.class,
                () -> {
                    Request request = new Request( // create new Request {CREATED}
                            requestId,
                            marketId,
                            userId,
                            companyId,
                            type,
                            shares
                    );
                    // set valid state
                    request.setState(Request.SHARE_PRICE_CHECK);
                    // set invalid state
                    request.setState(Request.COMPLETED);
                }
        );
    }

    /*
     * This method is used to test whether when a
     * setState is called on a Request currently in
     * Request.SHARE_PRICE_CHECK state with a state of
     * Request.FAILED then it throws an
     * InvalidStateTransitionException
     * */
    @Test
    public void invalidTransitionFromSharePriceCheckToFailedTest(){
        assertThrows(
                InvalidStateTransitionException.class,
                () -> {
                    Request request = new Request( // create new Request {CREATED}
                            requestId,
                            marketId,
                            userId,
                            companyId,
                            type,
                            shares
                    );
                    // set valid state
                    request.setState(Request.SHARE_PRICE_CHECK);
                    // set invalid state
                    request.setState(Request.FAILED);
                }
        );
    }

    /*
     * This method is used to test whether when a
     * setState is called on a Request currently in
     * Request.BANK_ACCOUNT_CHECK state with a state of
     * Request.COMPLETED, the no Exception
     * is thrown
     * */
    @Test
    public void validTransitionFromBankAccountCheckToCompletedTest(){
        assertDoesNotThrow(
                () -> {
                    Request request = new Request( // create new Request {CREATED}
                            requestId,
                            marketId,
                            userId,
                            companyId,
                            type,
                            shares
                    );
                    // from created to share price check
                    request.setState(Request.SHARE_PRICE_CHECK);
                    // next valid state
                    request.setState(Request.BANK_ACCOUNT_CHECK);
                    // next valid state
                    request.setState(Request.COMPLETED);
                }
        );
    }

    /*
     * This method is used to test whether when a
     * setState is called on a Request currently in
     * Request.BANK_ACCOUNT_CHECK state with a state of
     * Request.FAILED, the no Exception
     * is thrown
     * */
    @Test
    public void validTransitionFromBankAccountCheckToFailedTest(){
        assertDoesNotThrow(
                () -> {
                    Request request = new Request( // create new Request {CREATED}
                            requestId,
                            marketId,
                            userId,
                            companyId,
                            type,
                            shares
                    );
                    // from created to share price check
                    request.setState(Request.SHARE_PRICE_CHECK);
                    // next valid state
                    request.setState(Request.BANK_ACCOUNT_CHECK);
                    // next valid state
                    request.setState(Request.FAILED);
                }
        );
    }
}