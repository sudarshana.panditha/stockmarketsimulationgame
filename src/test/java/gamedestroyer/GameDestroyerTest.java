package gamedestroyer;

import akka.actor.ActorRef;
import akka.actor.ActorSystem;
import akka.actor.Props;
import akka.testkit.javadsl.TestKit;
import bank.messages.AllAccountsBalanceRequest;
import bank.messages.AllAccountsBalanceResponse;
import broker.messages.AllPortFoliosResponse;
import broker.messages.AllPortfoliosRequest;
import gamedestroyer.exception.GameResultNotFoundException;
import gamedestroyer.messages.GameCompleted;
import gamedestroyer.messages.ResultsRequest;
import gamedestroyer.messages.ResultsResponse;
import market.messages.AllSharePricesRequest;
import market.messages.AllSharePricesResponse;
import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import static org.junit.jupiter.api.Assertions.*;

import java.time.Duration;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.UUID;

/*
* This class contains the unit tests on
* GameDestroyer Actor class
* */
class GameDestroyerTest {
    public static ActorSystem system;
    public static TestKit brokerProbe;
    public static TestKit marketProbe;
    public static TestKit bankProbe;
    public static GameResultRepository repository;
    public static String gameId;
    public static ActorRef gameEnder;

    @BeforeEach
    public void setup(){
        system = ActorSystem.create();
        brokerProbe = new TestKit(system);
        marketProbe = new TestKit(system);
        bankProbe = new TestKit(system);
        repository = new GameResultRepository();
        gameId = UUID.randomUUID().toString();
        Props props = Props.create(
                GameDestroyer.class,
                repository,
                brokerProbe.getRef(),
                marketProbe.getRef(),
                bankProbe.getRef()
        );
        gameEnder = system.actorOf(props);
    }

    @AfterEach
    public void tearDown(){
        system.terminate();
        system = null;
    }

    /*
    * This method tests whether when a GameCompleted
    * is received by GameDestroyer then the BrokerActor is sent
    * with a AllPortfoliosRequest with the game id of the
    * completed Game
     * */
    @Test
    public void allPortfoliosRequestTest(){
        // sending message
        GameCompleted gameCompleted = new GameCompleted(gameId);
        gameEnder.tell(gameCompleted, ActorRef.noSender());
        // expected message
        AllPortfoliosRequest request = new AllPortfoliosRequest(gameId);
        brokerProbe.expectMsg(Duration.ofSeconds(3), request);
    }

    /*
     * This method tests whether when a GameCompleted
     * is received by GameDestroyer then the MarketActor is sent
     * with a AllSharePricesRequest with the game id of the
     * completed Game
     * */
    @Test
    public void allSharePricesRequestTest(){
        // sending message
        GameCompleted gameCompleted = new GameCompleted(gameId);
        gameEnder.tell(gameCompleted, ActorRef.noSender());
        // expected message
        AllSharePricesRequest request = new AllSharePricesRequest(gameId);
        marketProbe.expectMsg(Duration.ofSeconds(3), request);
    }

    /*
    * This method tests whether when an GameCompleted
    * message is received by the GameDestroyer, then
    * a GameResult object is created in the GameResultRepository
    * */
    @Test
    public void gameCompletedGameResultCreatedTest() throws GameResultNotFoundException, InterruptedException {
        // sending message
        GameCompleted gameCompleted = new GameCompleted(gameId);
        gameEnder.tell(gameCompleted, ActorRef.noSender());
        // wait for message to be processed
        Thread.sleep(3000);
        assertNotNull(
                repository.getGameResult(gameId)
        );

    }

    /*
    * This method tests whether when AllPortfoliosResponse
    * message is received, then a AllAccountsBalanceRequest
    * is sent to BankActor
    * */
    @Test
    public void allAccountsBalanceRequestTest() throws InterruptedException {
        // preparing the actor
        GameCompleted gameCompleted = new GameCompleted(gameId);
        gameEnder.tell(gameCompleted, ActorRef.noSender());
        // wait for message to be processed
        Thread.sleep(3000);
        // sending message
        AllPortFoliosResponse response = new AllPortFoliosResponse(
                gameId,
                new HashMap<>()
        );
        // testing actor
        gameEnder.tell(response, ActorRef.noSender());
        // expecting message
        AllAccountsBalanceRequest request = new AllAccountsBalanceRequest(gameId, new ArrayList<>());
        bankProbe.expectMsg(Duration.ofSeconds(5), request);
    }

    /*
     * This method tests whether when AllPortfoliosResponse
     * message is received, then the Portfolios are set
     * in the GameResult object
     * */
    @Test
    public void setPortfoliosTest() throws InterruptedException, GameResultNotFoundException {
        // preparing the actor
        GameCompleted gameCompleted = new GameCompleted(gameId);
        gameEnder.tell(gameCompleted, ActorRef.noSender());
        // wait for message to be processed
        Thread.sleep(3000);
        // sending message
        AllPortFoliosResponse response = new AllPortFoliosResponse(
                gameId,
                new HashMap<>()
        );
        // testing actor
        gameEnder.tell(response, ActorRef.noSender());
        Thread.sleep(3000);
        assertEquals(
                true,
                repository.getGameResult(gameId).isPortfoliosReceived()
        );
    }

    /*
    * This method tests whether when a AllSharePricesResponse
    * is received by the GameDestroyer, then the corresponding
    * GameResult object is set with the share prices
    * */
    @Test
    public void setSharePricesTest() throws InterruptedException, GameResultNotFoundException {
        // preparing the actor
        GameCompleted gameCompleted = new GameCompleted(gameId);
        gameEnder.tell(gameCompleted, ActorRef.noSender());
        // wait for message to be processed
        Thread.sleep(3000);
        // sending message
        AllSharePricesResponse response = new AllSharePricesResponse(
                gameId,
                new HashMap<>()
        );
        // testing actor
        gameEnder.tell(response, ActorRef.noSender());
        Thread.sleep(3000);
        assertEquals(
                true,
                repository.getGameResult(gameId).isSharePricesReceived()
        );
    }

    /*
    * This method tests whether when an AllAccountsBalanceResponse
    * is received by the GameDestroyer then it sets the
    * account balances in the corresponding GameResult object
    * */
    @Test
    public void setUserAccountBalanceTest() throws InterruptedException, GameResultNotFoundException {
        // preparing the actor
        GameCompleted gameCompleted = new GameCompleted(gameId);
        gameEnder.tell(gameCompleted, ActorRef.noSender());
        // wait for message to be processed
        Thread.sleep(3000);
        AllPortFoliosResponse portFoliosResponse = new AllPortFoliosResponse(
                gameId,
                new HashMap<>()
        );
        gameEnder.tell(portFoliosResponse, ActorRef.noSender());

        Thread.sleep(3000);
        // sending message
        AllAccountsBalanceResponse response = new AllAccountsBalanceResponse(
                gameId,
                new HashMap<>()
        );
        // testing actor
        gameEnder.tell(response, ActorRef.noSender());
        Thread.sleep(3000);
        assertEquals(
                true,
                repository.getGameResult(gameId).isAccountBalancesReceived()
        );
    }

    /*
    * This method tests whether when ResultRequest is received
    * by the GameDestroyer and not all required data is set, then
    * a RequestResponse with state as WAIT is sent back
    * */
    @Test
    public void requestResponseWaitTest() throws InterruptedException {
        // preparing the actor
        GameCompleted gameCompleted = new GameCompleted(gameId);
        gameEnder.tell(gameCompleted, ActorRef.noSender());
        // wait for message to be processed
        Thread.sleep(3000);
        AllPortFoliosResponse portFoliosResponse = new AllPortFoliosResponse(
                gameId,
                new HashMap<>()
        );
        gameEnder.tell(portFoliosResponse, ActorRef.noSender());

        Thread.sleep(3000);
        AllAccountsBalanceResponse balanceResponse = new AllAccountsBalanceResponse(
                gameId,
                new HashMap<>()
        );
        gameEnder.tell(balanceResponse, ActorRef.noSender());
        Thread.sleep(3000);
        // testing actor
        TestKit probe = new TestKit(system);
        // sending message
        ResultsRequest request = new ResultsRequest(gameId);
        gameEnder.tell(request, probe.getRef());
        // expecting message
        ResultsResponse resultsResponse = new ResultsResponse(
                ResultsResponse.WAIT,
                null,
                null
        );
        probe.expectMsg(Duration.ofSeconds(3), resultsResponse);

    }

    /*
     * This method tests whether when ResultRequest is received
     * by the GameDestroyer and all required data is set, then
     * a RequestResponse with state as CALCULATED is sent back
     * */
    @Test
    public void requestResponseCalculatedTest() throws InterruptedException {
        // preparing the actor
        GameCompleted gameCompleted = new GameCompleted(gameId);
        gameEnder.tell(gameCompleted, ActorRef.noSender());
        // wait for message to be processed
        Thread.sleep(3000);
        AllPortFoliosResponse portFoliosResponse = new AllPortFoliosResponse(
                gameId,
                new HashMap<>()
        );
        gameEnder.tell(portFoliosResponse, ActorRef.noSender());

        Thread.sleep(3000);
        AllAccountsBalanceResponse balanceResponse = new AllAccountsBalanceResponse(
                gameId,
                new HashMap<>()
        );
        gameEnder.tell(balanceResponse, ActorRef.noSender());
        Thread.sleep(3000);
        AllSharePricesResponse response = new AllSharePricesResponse(
                gameId,
                new HashMap<>()
        );
        gameEnder.tell(response, ActorRef.noSender());
        Thread.sleep(3000);

        // testing actor
        TestKit probe = new TestKit(system);
        // sending message
        ResultsRequest resultsRequest = new ResultsRequest(gameId);
        gameEnder.tell(resultsRequest, probe.getRef());
        // expecting message
        ResultsResponse resultsResponse = new ResultsResponse(
                ResultsResponse.CALCULATED,
                null,
                null
        );
        probe.expectMsg(Duration.ofSeconds(3), resultsResponse);

    }
}