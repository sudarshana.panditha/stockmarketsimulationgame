package gamedestroyer;

import broker.Portfolio;
import gamedestroyer.exception.GameResultsNotCalculatedException;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.Test;

import java.util.HashMap;
import java.util.Map;
import java.util.UUID;

import static org.junit.jupiter.api.Assertions.*;
/*
* This class contains the unit tests on
* GameResult class
* */
class GameResultTest {
    public static String gameId;
    public static Map<String, Portfolio> portfolios;
    public static Map<String, Double> userAccountBalances;
    public static Map<String, Map<String, Double>> sectorSharePrices;
    public static String userIds[];
    public static String usernames[];

    @BeforeAll
    public static void setup(){
        gameId = UUID.randomUUID().toString();
        portfolios = new HashMap<>();
        userAccountBalances = new HashMap<>();
        sectorSharePrices = new HashMap<>();
        userIds = new String[4];
        usernames = new String[4];
        for(int i = 0; i < 4 ; ++i){
            userIds[i] = UUID.randomUUID().toString();
            usernames[i] = "username" + (i+1);
            Portfolio portfolio = new Portfolio(gameId, userIds[i], usernames[i]);
            portfolios.put(userIds[i], portfolio);
            userAccountBalances.put(usernames[i], new Double(100.0*(i+1)));
        }
    }

    /*
    * This method tests whether when a GameResult is
    * just created isPortfoliosReceived returns false
    * */
    @Test
    public void initialIsPortfoliosReceivedTest(){
        GameResult gameResult = new GameResult(gameId);
        assertEquals(
                false,
                gameResult.isPortfoliosReceived()
        );
    }

    /*
     * This method tests whether when a GameResult is
     * just created isSharePricesReceived returns false
     * */
    @Test
    public void initialIsSharePricesReceivedTest(){
        GameResult gameResult = new GameResult(gameId);
        assertEquals(
                false,
                gameResult.isSharePricesReceived()
        );
    }

    /*
     * This method tests whether when a GameResult is
     * just created isSharePricesReceived returns false
     * */
    @Test
    public void initialIsAccountBalancesReceivedTest(){
        GameResult gameResult = new GameResult(gameId);
        assertEquals(
                false,
                gameResult.isAccountBalancesReceived()
        );
    }

    /*
     * This method tests whether when a GameResult
     * is just created and getWinner is called
     * then a GameResultsNotCalculatedException
     * is thrown
     * */
    @Test
    public void getWinnerExceptionTest(){
        assertThrows(
                GameResultsNotCalculatedException.class,
                () -> {
                    GameResult gameResult = new GameResult(gameId);
                    gameResult.getWinner();
                }
        );
    }

    /*
     * This method tests whether when a GameResult
     * is just created and getWinner is called
     * then a GameResultsNotCalculatedException
     * is thrown
     * */
    @Test
    public void getGameResultsExceptionTest(){
        assertThrows(
                GameResultsNotCalculatedException.class,
                () -> {
                    GameResult gameResult = new GameResult(gameId);
                    gameResult.getGameResults();
                }
        );
    }
}