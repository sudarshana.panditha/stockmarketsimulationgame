package gamedestroyer;

import gamedestroyer.exception.GameResultNotFoundException;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.Test;

import java.util.UUID;

import static org.junit.jupiter.api.Assertions.*;
/*
* This class contains the unit tests on
* GameResultRepository class
* */
class GameResultRepositoryTest {
    public static String gameId;

    @BeforeAll
    public static void setup(){
        gameId = UUID.randomUUID().toString();
    }

    /*
    * This method tests whether when createGameResult is called
    * then it does not return null
    * */
    @Test
    public void createGameResultTest(){
        GameResultRepository repository = new GameResultRepository();
        assertNotNull(
                repository.createGameResult(gameId)
        );
    }

    /*
     * This method tests whether when getGameResult is called
     * and a GameResult exists for the Game, then it does not
     * return null
     * */
    @Test
    public void getGameResultTest() throws GameResultNotFoundException {
        GameResultRepository repository = new GameResultRepository();
        repository.createGameResult(gameId);
        assertNotNull(
                repository.getGameResult(gameId)
        );
    }

    /*
     * This method tests whether when getGameResult is called
     * and a GameResult does not exists for the Game, then it
     * throws a GameResultNotFoundException
     * */
    @Test
    public void gameResultNotFoundExceptionTest(){
        assertThrows(
                GameResultNotFoundException.class,
                () -> {
                    GameResultRepository repository = new GameResultRepository();
                    repository.getGameResult(gameId);
                }
        );
    }
}