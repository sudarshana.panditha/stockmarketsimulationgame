package bank;

import bank.exceptions.*;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.Test;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import static org.junit.jupiter.api.Assertions.*;

/*
* This class contains the unit tests
* on AccountRepositoryTest class
* */
public class AccountRepositoryTest {
    public static String accountId;
    public static String name;
    public static double amount;

    @BeforeAll
    public static void setUp(){
        name = "devni";
        amount = 1000;
    }

    /*
   * This method is used to test whether when
   * createAccount is called on AccountRepository
   * then a Bank Account is created with correct details
   * */
    @Test
    public void createAccountTest() throws AccountNotFoundException, InvalidAmountException, NegativeBalanceException, NotEnoughDepositException {
        AccountRepository accountRepository = new AccountRepository();
        Account account = accountRepository.createAccount(name, amount);
        assertNotNull(accountRepository.getAccount(account.getAccountId()));
    }

    @Test
    public void getAccountBalancesTest() throws AccountNotFoundException, InvalidAmountException, NotEnoughDepositException, NegativeBalanceException {
        AccountRepository accountRepository = new AccountRepository();
        List<String> accountIds = new ArrayList<>();

        for(int i= 0; i<4; i++){
            Account account = accountRepository.createAccount(name, amount);
            accountIds.add(account.getAccountId());
        }

        Map<String, Double> allAccountBalances = accountRepository.getAccountBalances(accountIds);
        assertIterableEquals(accountIds, allAccountBalances.keySet());
    }

    /*
   * This method is used to test whether when
   * getAccount is called on AccountRepository
   * and an Account is not found, an
   * AccountNotFoundException is thrown
   * */
    @Test
    public void accountNotFoundExceptionTest(){
        AccountRepository accountRepository = new AccountRepository();
        assertThrows(
                AccountNotFoundException.class,
                ()->{
                    accountRepository.getAccount(accountId);
                }
        );
    }

    /*
   * This method is used to test whether when
   * deposit is called on AccountRepository
   * and the given amount of deposit is a
   * negative value which is not valid,
   * an InvalidAmountException is thrown
   * */
    @Test
    public void invalidAmountExceptionOnNegativeDepositValueTest() throws AccountNotFoundException, InvalidAmountException, NotEnoughDepositException, NegativeBalanceException {
        AccountRepository accountRepository = new AccountRepository();
        Account newAccount = accountRepository.createAccount(name, amount);
        accountId = newAccount.getAccountId();
        assertThrows(
                InvalidAmountException.class,
                ()->{
                    accountRepository.deposit(accountId,-1000);
                }
        );
    }

    /*
  * This method is used to test whether when
  * withdraw is called on AccountRepository
  * and the given amount of withdrawal is a
  * negative value which is not valid,
  * an InvalidAmountException is thrown
  * */
    @Test
    public void invalidAmountExceptionOnNegativeWithdrawalValueTest() throws AccountNotFoundException, InvalidAmountException, NotEnoughDepositException, NegativeBalanceException {
        AccountRepository accountRepository = new AccountRepository();
        Account newAccount = accountRepository.createAccount(name, amount);
        accountId = newAccount.getAccountId();
        assertThrows(
                InvalidAmountException.class,
                ()->{
                    accountRepository.withdraw(accountId,-1000);
                }
        );
    }

    /*
   * This method is used to test whether when
   * withdraw is called on AccountRepository
   * and the given amount of withdrawal is greater
   * than the remaining balance in the account,
   * a NotEnoughBalanceException is thrown
   * */
    @Test
    public void notEnoughBalanceExceptionOnGreaterWithdrawalValueTest() throws AccountNotFoundException, InvalidAmountException, NotEnoughDepositException, NegativeBalanceException {
        AccountRepository accountRepository = new AccountRepository();
        Account newAccount = accountRepository.createAccount(name, amount);
        accountId = newAccount.getAccountId();
        assertThrows(
                NotEnoughBalanceException.class,
                ()->{
                    accountRepository.withdraw(accountId,5000);
                }
        );
    }

    /*
   * This method tests whether getAccount method does
   * not return null when there exist any Account for
   * a given account id
   * */
    @Test
    public void getAccountTest() throws Exception {
        AccountRepository accountRepository = new AccountRepository();
        Account account = accountRepository.createAccount(name, amount);
        assertNotNull(accountRepository.getAccount(account.getAccountId()));
    }

    /*
   * This method tests whether createAccount method does
   * not return null when a new bank account is newly
   * created
   * */
    @Test
    public void createAccountReturnsNotNullTest() throws Exception {
        AccountRepository accountRepository = new AccountRepository();
        Account newAccount = accountRepository.createAccount(name, amount);
        assertNotNull(newAccount);
    }

    /*
  * This method tests whether setBalance method calculates
  * the given amount of deposit correctly with the balance
  * and returns the correct value when getBalance method is called
  * */
    @Test
    public void depositTest() throws Exception {
        AccountRepository accountRepository = new AccountRepository();
        Account account = accountRepository.createAccount(name, amount);
        accountId = account.getAccountId();

        double deposit = 3000;

        account = accountRepository.getAccount(accountId);
        account.setBalance(amount+deposit);

        assertEquals(amount+deposit, account.getBalance());
    }

    /*
* This method tests whether setBalance method calculates
* the given amount of withdrawal correctly with the balance
* and returns the correct value when getBalance method is called
* */
    @Test
    public void withdrawTest() throws Exception {
        AccountRepository accountRepository = new AccountRepository();
        Account account = accountRepository.createAccount(name, amount);
        accountId = account.getAccountId();

        double withdraw = 500;

        account = accountRepository.getAccount(accountId);
        account.setBalance(amount-withdraw);

        assertEquals(amount-withdraw, account.getBalance());
    }
}