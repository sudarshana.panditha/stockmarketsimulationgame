package bank;

import bank.exceptions.AccountNotFoundException;
import bank.exceptions.NegativeBalanceException;
import bank.exceptions.NotEnoughDepositException;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.Test;

import java.util.UUID;

import static org.junit.jupiter.api.Assertions.*;

/*
* This class contains the unit tests
* on AccountTest class
* */
public class AccountTest {
    public static String accountId;
    public static String name;
    public static double balance;


    @BeforeAll
    public static void setup(){
        accountId = UUID.randomUUID().toString();
        name = "devni";
        balance = 1000;
    }

    /*
    * This method tests whether when a call to
    * initiate a new bank account is called with a
    * lesser amount of deposit, then a
    * NotEnoughDepositException is called
    * */
    @Test
    public void notEnoughDepositExceptionTest() throws Exception {
        assertThrows(
                NotEnoughDepositException.class,
                ()->{
                    new Account(accountId, name, 500);
                });
    }

    /*
    * This method tests whether when a call to
    * initiate a new bank account is called with a
    * negative value for deposit, then a
    * NegativeBalanceException is called
    * */
    @Test
    public void negativeBalanceExceptionsTest() throws AccountNotFoundException, NotEnoughDepositException, NegativeBalanceException {
        Account account = new Account(accountId, name, balance);
        assertThrows(
                NegativeBalanceException.class,
                ()->{
                    account.setBalance(-1000);
                }
        );
    }

    /*
     * This method is used to test whether when
     * getAccountId is called it returns the correct
     * account id of the given player
     * */
    @Test
    public void getAccountIdTest() throws AccountNotFoundException, NotEnoughDepositException, NegativeBalanceException {
        Account account = new Account(accountId, name, balance);
        assertEquals(this.accountId, account.getAccountId());
    }

    /*
    * This method is used to test whether when
    * getName is called it returns the correct
    * name of the given player
    * */
    @Test
    public void getNameTest() throws NotEnoughDepositException, AccountNotFoundException, NegativeBalanceException {
        Account account = new Account(accountId, name, balance);
        assertEquals(this.name, account.getName());
    }

    /*
   * This method is used to test when initiating
   * an account, when getBalance is called it
   * returns the correct balance of the given
   * player while checking whether the initial
   * deposit is less than the minimum amount an
   * account can have
   * */
    @Test
    public void balanceLessThan1000Test() throws Exception {
        Account account = new Account(name, accountId, balance);
        assertFalse(greaterThan(500, account.getBalance()));
    }

    /*
  * This method is used to test when initiating
  * an account, when getBalance is called it
  * returns the correct balance of the given
  * player while checking whether the initial
  * deposit is equal to the minimum amount an
  * account can have
  * */
    @Test
    public void balanceEqualsTo1000Test() throws Exception {
        Account account = new Account(name, accountId, balance);
        assertEquals(1000, account.getBalance());
    }

    /*
  * This method is used to test when initiating
  * an account, when getBalance is called it
  * returns the correct balance of the given
  * player while checking whether the initial
  * deposit is greater than the minimum amount an
  * account can have
  * */
    @Test
    public void balanceGreaterThan1000Test() throws Exception {
        Account account = new Account(name, accountId, balance);
        assertTrue(greaterThan(balance, account.getBalance()));
    }

    /*
    * This method is used to compare two double
    * values and return whether, given amount is
    * greater orlesser than the other
    * */
    public boolean greaterThan(double num1, double num2){
        if(num1 >= num2){
            return true;
        }
        else return false;
    }
}