package bank;

import akka.actor.ActorRef;
import akka.actor.ActorSystem;
import akka.actor.Props;
import org.junit.jupiter.api.BeforeAll;

import java.util.UUID;

public class BankActorTest {
    public static ActorSystem system;
    public static ActorRef bankActor;
    public static String requestId;
    public static String accountId;
    public static String name;
    public static double amount;
    public static AccountRepository accountRepository;

    @BeforeAll
    public void setUp() throws Exception {
        requestId = UUID.randomUUID().toString();
        accountId = UUID.randomUUID().toString();
        name = "devni";
        amount = 1000;

        accountRepository = new AccountRepository();
        system = ActorSystem.create();

        Props bankProps = Props.create(
                BankActor.class,
                accountRepository
        );
        bankActor = system.actorOf(bankProps);
    }
}
