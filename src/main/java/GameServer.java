/**
 * This class is the HTTP server that accepts client requests and provides services back
 * It contains the main() method as well as bootstrap all actors.
 * @author : W J Shasanka Fernando
 * @version : 2.0, 01-08-2019
 * */

import akka.actor.ActorSystem;
import akka.actor.ActorRef;
import akka.http.javadsl.ConnectHttp;
import akka.http.javadsl.Http;
import akka.http.javadsl.ServerBinding;
import akka.http.javadsl.model.HttpRequest;
import akka.http.javadsl.model.HttpResponse;
import akka.http.javadsl.server.AllDirectives;
import akka.http.javadsl.server.Route;
import akka.stream.ActorMaterializer;
import akka.stream.javadsl.Flow;
import akka.NotUsed;
import bank.BankActor;
import broker.BrokerActor;
import clock.ClockActor;
import clock.GameClockRepository;
import gamedestroyer.GameDestroyer;
import market.MarketActor;
import playerstatus.GameMakerActor;
import playerstatus.PlayerStatusActor;
import playerstatus.PlayerStatusRepository;
import playerstatus.TestActor;

import java.util.concurrent.CompletionStage;


public class GameServer extends AllDirectives{

    private final GameRoutes gameRoutes;
    public ActorRef playerStatusActor;
    public ActorRef gameMakerActor;
    public ActorRef brokerActor;
    public ActorRef clockActor;
    public ActorRef gameDestroyerActor;
    public PlayerStatusRepository playerStatusRepository;
    public GameClockRepository gameClockRepository;
    public ActorRef marketActor;
    public ActorRef bankActor;

    //server class constructor
    public GameServer (ActorSystem gameSystem) {
        playerStatusRepository = new PlayerStatusRepository(10);
        gameClockRepository = new GameClockRepository();
        marketActor = gameSystem.actorOf(MarketActor.props(), "marketActor");
        playerStatusActor = gameSystem.actorOf(PlayerStatusActor.props(playerStatusRepository), "playerstatusactor");
        gameMakerActor = gameSystem.actorOf(GameMakerActor.props(playerStatusActor), "gameMaker");
        bankActor = gameSystem.actorOf(BankActor.props());
        brokerActor = gameSystem.actorOf(BrokerActor.props(marketActor, bankActor), "broker");
        clockActor = gameSystem.actorOf(ClockActor.props(gameClockRepository, marketActor), "clock");
        gameDestroyerActor = gameSystem.actorOf(GameDestroyer.props(brokerActor, marketActor, bankActor), "gameDestroyer");
        gameRoutes = new GameRoutes (gameSystem, playerStatusActor, gameMakerActor, brokerActor, clockActor, gameDestroyerActor);
    }

    public static void main(String [] args) throws Exception{
        ActorSystem gameSystem = ActorSystem.create("GameServer");

        final Http http = Http.get(gameSystem);
        final ActorMaterializer materializer = ActorMaterializer.create(gameSystem);

        GameServer gameServer = new GameServer(gameSystem);
        final Flow<HttpRequest, HttpResponse, NotUsed> routeFlow = gameServer.createRoute().flow(gameSystem, materializer);
        final CompletionStage<ServerBinding> binding = http.bindAndHandle(routeFlow, ConnectHttp.toHost("localhost", 8080), materializer);

        System.out.println("Server listening at http://localhost:8080/\nPress RETURN to stop...");
        System.in.read();

        binding.thenCompose(ServerBinding::unbind).thenAccept(unbound -> gameSystem.terminate());
    }

    protected Route createRoute() {
        return gameRoutes.routes();
    }
}
