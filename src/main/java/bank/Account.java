package bank;

import bank.exceptions.NegativeBalanceException;
import bank.exceptions.NotEnoughDepositException;

public class Account {
    private final String accountId; //account ID of the player
    private final String name; //username of the player
    private double balance; //current balance of the player


    public Account(String accountId, String name, double initialDeposit)
            throws NotEnoughDepositException {
        this.accountId = accountId;
        this.name = name;
        this.balance = initialDeposit;
        if(initialDeposit < 1000)
            throw new NotEnoughDepositException();
    }

    public String getAccountId(){
        return accountId;
    }

    public String getName(){
        return name;
    }

    public double getBalance(){
        return balance;
    }

    public void setBalance(double amount) throws NegativeBalanceException {
        if(!(amount<=0))
            this.balance = amount;
        else
            throw new NegativeBalanceException();
    }
}
