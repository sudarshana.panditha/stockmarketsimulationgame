package bank;

import akka.actor.AbstractActor;
import akka.actor.Props;
import bank.exceptions.*;
import bank.messages.*;

import java.util.Map;

public class BankActor extends AbstractActor {
    public final AccountRepository accountRepository;

    public BankActor(){
        this.accountRepository = new AccountRepository();
    }

    public BankActor(AccountRepository accountRepository){
        this.accountRepository = accountRepository;
    }

    @Override
    public Receive createReceive() {
        return receiveBuilder()
                .match(
                        CreateAccount.class,
                        msg -> {
                            try{
                                accountRepository.createAccount(
                                        msg.name,
                                        msg.deposit
                                );
                                sender().tell(new Ack(msg.referenceId), getSelf());
                            }catch (NotEnoughDepositException e){
                                sender().tell(new UnAck(msg.referenceId), getSelf());
                            }
                        }
                )
                .match(//Message handle for received Withdraw message
                        Withdraw.class,
                        msg -> {
                            try {
                                accountRepository.withdraw(
                                        msg.accountId,
                                        msg.amount
                                );
                                sender().tell(new Ack(msg.requestId), getSelf());
                            }catch (AccountNotFoundException e){
                                sender().tell(new UnAck(msg.requestId), getSelf());
                            }catch (NegativeBalanceException e){
                                sender().tell(new UnAck(msg.requestId), getSelf());
                            }catch (InvalidAmountException e){
                                sender().tell(new UnAck(msg.requestId), getSelf());
                            }catch (NotEnoughBalanceException e){
                                sender().tell(new UnAck(msg.requestId), getSelf());
                            }
                        }
                )
                .match(//Message handle for received Deposit message
                        Deposit.class,
                        msg -> {
                            try{
                            accountRepository.deposit(
                                    msg.accountId,
                                    msg.amount
                            );
                            sender().tell(new Ack(msg.requestId), getSelf());
                        }catch (AccountNotFoundException e){
                                sender().tell(new UnAck(msg.requestId), sender());
                            }catch (InvalidAmountException e){
                                sender().tell(new UnAck(msg.requestId), sender());
                            }catch (NegativeBalanceException e){
                                sender().tell(new UnAck(msg.requestId), getSelf());
                            }
                        }
                )
                .match(
                        AllAccountsBalanceRequest.class,
                        msg -> {
                            Map<String, Double> accounts = accountRepository.getAccountBalances(
                                    msg.accountIds
                            );
                            AllAccountsBalanceResponse response = new AllAccountsBalanceResponse(
                                    msg.referenceId, accounts
                            );
                            getSender().tell(response, getSelf());
                        }
                )
                .build();
    }


    public static Props props() {
        return Props.create(BankActor.class, () -> new BankActor());

    }
}
