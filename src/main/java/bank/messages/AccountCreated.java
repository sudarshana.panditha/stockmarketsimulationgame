package bank.messages;

public class AccountCreated {
    public final String referenceId;
    public final String accountId;

    public AccountCreated(String refId, String accId){
        this.referenceId = refId;
        this.accountId = accId;
    }
}
