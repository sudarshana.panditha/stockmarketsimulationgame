package bank.messages;

public class Withdraw {
    public final String requestId;
    public final String accountId;
    public final double amount;

    public Withdraw(String reqId, String accId, double amount) {
        this.requestId = reqId;
        this.accountId = accId;
        this.amount = amount;
    }

    public String getRequestId(String reqId){
        return reqId;
    }

    public void setRequestId(){
    }

    @Override
    public boolean equals(Object obj) {
        if(this == obj) return true;
        if(obj == null) return false;
        if(getClass() != obj.getClass())
            return false;
        Withdraw withdraw = (Withdraw) obj;
        return this.requestId.equals(withdraw.requestId);
    }
}
