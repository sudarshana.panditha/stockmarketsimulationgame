package bank.messages;

public class Ack {
    public final String referenceId;

    public Ack(String referenceId){
        this.referenceId = referenceId;
    }
}
