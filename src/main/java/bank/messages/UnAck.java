package bank.messages;

public class UnAck {
    public final String referenceId;

    public UnAck(String referenceId){
        this.referenceId = referenceId;
    }

}
