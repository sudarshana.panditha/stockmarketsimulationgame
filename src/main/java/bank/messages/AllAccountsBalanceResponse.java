package bank.messages;

import java.util.Map;
/*
* This is a message class used to respond
* to AllAccountsBalanceRequests with a map of
* name of the user to their account balance
* */
public class AllAccountsBalanceResponse {
    public final String referenceId;
    public Map<String, Double> userAccountBalances;

    public AllAccountsBalanceResponse(String referenceId, Map<String, Double> userAccountBalances) {
        this.referenceId = referenceId;
        this.userAccountBalances = userAccountBalances;
    }
}
