package bank.messages;

public class AccountNotCreated {
    public final String referenceId;

    public AccountNotCreated(String refId){
        this.referenceId = refId;
    }
}
