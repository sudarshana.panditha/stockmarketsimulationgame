package bank.messages;

import java.util.List;

/*
* This message class is used to request the
* the current account balances of account id
* in the list from the BankActor
* */
public class AllAccountsBalanceRequest {
    public final String referenceId;
    public List<String> accountIds;

    /* constructor */
    public AllAccountsBalanceRequest(String referenceId, List<String> accountIds) {
        this.referenceId = referenceId;
        this.accountIds = accountIds;
    }

    @Override
    public boolean equals(Object obj) {
        if(this == obj) return true;
        if(obj == null) return false;
        if(getClass() != obj.getClass())
            return false;
        AllAccountsBalanceRequest req = (AllAccountsBalanceRequest) obj;
        return this.referenceId.equals(req.referenceId);
    }
}
