package bank.messages;

public class CreateAccount {
    public final String referenceId;
    public final String name;
    public final double deposit;

    public CreateAccount(String refId, String name, double deposit){
        this.referenceId = refId;
        this.name = name;
        this.deposit = deposit;
    }


    @Override
    public boolean equals(Object obj) {
        if(this == obj) return true;
        if(obj == null) return false;
        if(getClass() != obj.getClass())
            return false;
        CreateAccount msg = (CreateAccount) obj;
        return this.referenceId.equals(msg.referenceId);
    }
}
