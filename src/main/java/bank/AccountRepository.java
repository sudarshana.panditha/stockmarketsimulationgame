package bank;

import bank.exceptions.*;

import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.UUID;

public class AccountRepository {
    private Map<String, Account> accounts;

    public AccountRepository(){
        accounts = new HashMap<>();
    }

    public Map<String, Double> getAccountBalances(List<String> accountIds) throws AccountNotFoundException {
        Account account = null;
        Map<String, Double> allAccountBalances = new HashMap<>();

        for(String accountId : accountIds){
            account = accounts.get(accountId);

            allAccountBalances.put(account.getAccountId(), new Double(account.getBalance()));
        }
        return allAccountBalances;
    }

    public Account createAccount(String name, double deposit)
            throws AccountNotFoundException, NegativeBalanceException, NotEnoughDepositException, InvalidAmountException {
        final String accountId = UUID.randomUUID().toString();
        Account newAccount = new Account(accountId, name, deposit);
        accounts.put(newAccount.getAccountId(), newAccount);
        return newAccount;
    }

    public Account getAccount(String accountId) throws AccountNotFoundException {
        Account account = accounts.get(accountId);
        if(account == null){
            throw new AccountNotFoundException();
        }else
            return account;
    }

    public void deposit(String accountId, double amount)
            throws AccountNotFoundException, InvalidAmountException, NegativeBalanceException {
        if(accountId == null){
            throw new AccountNotFoundException();
        }
        if(amount <= 0){
            throw new InvalidAmountException();
        }

        Account account = getAccount(accountId);
        account.setBalance(amount+account.getBalance());
    }

    public void withdraw(String accountId, double amount)
            throws AccountNotFoundException, InvalidAmountException, NotEnoughBalanceException, NegativeBalanceException {
        if(accountId == null){
            throw new AccountNotFoundException();
        }
        if(amount <= 0){
            throw new InvalidAmountException();
        }
        if(getAccount(accountId).getBalance() < amount){
            throw new NotEnoughBalanceException();
        }

        Account account = getAccount(accountId);
        account.setBalance(account.getBalance()-amount);
    }
}
