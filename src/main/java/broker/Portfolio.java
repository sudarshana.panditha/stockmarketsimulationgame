package broker;

import broker.exception.CompanyNotFoundException;

import java.io.Serializable;
import java.util.Collections;
import java.util.HashMap;
import java.util.Map;
import java.util.concurrent.atomic.AtomicBoolean;
import java.util.concurrent.atomic.AtomicLong;

/*
* This is a domain class that is used to represent
* the Portfolio of an Investor which contains the
* details of Investor's number of shares in each
* Company of the Market
* */
public class Portfolio implements Serializable{
    /* investor details */
    public final String marketId;
    public final String userId;
    public final String name;
    private Map<String,Map<String, Long>> sectorCompanyShares;

    /* state and housekeeping variables*/
    private String accountId = null;

    /* constructor */
    public Portfolio(String marketId, String userId, String name) {
        this.marketId = marketId;
        this.userId = userId;
        this.name = name;
        sectorCompanyShares = new HashMap<>();
    }

    /* interface */

    /*
    * getter and setter for accountId
    */
    public String getAccountId() {
        return accountId;
    }
    public void setAccountId(String accountId) {
        this.accountId = accountId;
    }

    /*
    * This method returns the Map of Sectors to Company Market Shares Map
    * */

    public Map<String, Map<String, Long>> getSectorCompanyShares() {
        return Collections.unmodifiableMap(sectorCompanyShares);
    }

    /*
    * This method is used to add a Company
    * to the Investor's Portfolio
    * */
    public void addCompany(String sectorId, String companyId){
        if(!sectorCompanyShares.containsKey(sectorId)){
            sectorCompanyShares.put(sectorId, new HashMap<>());
        }

        sectorCompanyShares.get(sectorId)
                .put(companyId, new Long(0));
    }

    /*
     * This method is used to increase the number of shares
     * in a Company by the given amount
     * */
    public void increaseShares(String companyId, long shares){
        sectorCompanyShares.forEach(
                (sectorId, companySharesMap) -> {
                    if(companySharesMap.containsKey(companyId)){
                        Long currentShares = companySharesMap.get(companyId);
                        companySharesMap.put(companyId, (currentShares + shares));
                    }
                }
        );

    }

    /*
     * This method is used to decrease the number of shares
     * in a Company by the given amount
     * */
    public void decreaseShares(String companyId, long shares){
        sectorCompanyShares.forEach(
                (sectorId, companySharesMap) -> {
                    if(companySharesMap.containsKey(companyId)){
                        Long currentShares = companySharesMap.get(companyId);
                        companySharesMap.put(companyId, (currentShares - shares));
                    }
                }
        );
    }

    /*
     * This method is used to get the number of shares
     * in a given Company
     * */
    public long getNumberOfShares(String companyId) throws CompanyNotFoundException {
        AtomicLong numberOfShares = new AtomicLong(0L);
        AtomicBoolean companyFound = new AtomicBoolean(false);
        sectorCompanyShares.forEach(
                (sectorId, companySharesMap) -> {
                    if(companySharesMap.containsKey(companyId)){
                        companyFound.set(true);
                        numberOfShares.set(companySharesMap.get(companyId));
                    }
                }
        );
        if(!companyFound.get())
            throw new CompanyNotFoundException();
        return numberOfShares.get();
    }
}
