package broker;

import akka.actor.AbstractActor;
import akka.actor.ActorRef;
import akka.actor.ActorSystem;
import akka.actor.Props;
import bank.Account;
import bank.messages.*;
import broker.messages.*;
import broker.request.Request;
import broker.request.RequestRepository;
import market.CompanyDetails;
import market.messages.CompanyListRequest;
import market.messages.CompanyListResponse;
import market.messages.SharePriceRequest;
import market.messages.SharePriceResponse;

import java.time.Duration;
import java.util.Iterator;
import java.util.Map;

/*
 * This is the Akka actor that represents a Broker
 * in the Game that receives and responds to messages
 * regarding buying and selling of shares and managing
 * investor Portfolios
 * */
public class BrokerActor extends AbstractActor{
    /* constants */
    public static double INITIAL_DEPOSIT = 1000.0;

    /* state and house keeping variables */
    private ActorRef marketActor;
    private ActorRef bankActor;
    private PortFolioRepository portFolioRepository;
    private RequestRepository requestRepository;

    /* constructor */
    public BrokerActor(ActorRef marketActor, ActorRef bankActor) {
        this.marketActor = marketActor;
        this.bankActor = bankActor;
        portFolioRepository = new PortFolioRepository();
        requestRepository = new RequestRepository();
    }

    public BrokerActor(ActorRef marketActor, ActorRef bankActor,
                       PortFolioRepository portFolioRepository, RequestRepository requestRepository) {
        this.marketActor = marketActor;
        this.bankActor = bankActor;
        this.portFolioRepository = portFolioRepository;
        this.requestRepository = requestRepository;
    }

    @Override
    public Receive createReceive() {
        return receiveBuilder()
                .match(// message handle for received CreatePortfolio message
                        CreatePortfolios.class,
                        msg -> {
                            // create portfolios
                            msg.marketUsers.forEach(
                                    (userId, username) -> {
                                        portFolioRepository.createPortfolio(
                                                msg.marketId,
                                                userId,
                                                username
                                        );
                                    }
                            );
                            // get the list of companies
                            ActorSystem system = getContext().getSystem();
                            system.scheduler().scheduleOnce(
                                            Duration.ofSeconds(3),
                                            marketActor,
                                    new CompanyListRequest(msg.marketId),
                                            system.dispatcher(),
                                            getSelf()
                                    );
                            // tell bank to create account for investors
                            msg.marketUsers.forEach(
                                    (userId, username) -> {
                                        bankActor.tell(new CreateAccount(userId, username, INITIAL_DEPOSIT), getSelf());
                                    }
                            );
                        }
                )
                .match( // message handle for received Sell message
                        Sell.class,
                        msg -> {
                            // Check for enough shares from Portfolio
                            Portfolio portfolio = portFolioRepository.getPortfolio(
                                    msg.marketId,
                                    msg.userId
                            );
                            long currentNumOfShares = portfolio.getNumberOfShares(msg.companyId);
                            if(currentNumOfShares >= msg.shares){
                                // there are enough shares to sell, so create a Request
                                Request request = requestRepository.createRequest(
                                        msg.marketId,
                                        msg.userId,
                                        msg.companyId,
                                        Request.SELL,
                                        msg.shares
                                );
                                // requesting for current share price from market
                                // now, request is in the share price check state
                                request.setState(Request.SHARE_PRICE_CHECK);
                                // message to market actor
                                SharePriceRequest sharePriceRequest = new SharePriceRequest(
                                        request.requestId,
                                        request.marketId,
                                        request.companyId
                                );
                                marketActor.tell(sharePriceRequest, getSelf());
                                // sending a chit message to sender
                                Chit chit = new Chit(Chit.SUCCESS, request.requestId, "processing sell request");
                                getSender().tell(chit, getSelf());
                            }else {
                                // not enough shares to sell
                                // sending a chit message to sender
                                Chit chit = new Chit(Chit.ERROR, "", "not enough shares to sell");
                                getSender().tell(chit, getSelf());
                            }
                        }
                )
                .match(
                        Buy.class,
                        msg -> {
                            // create request for buy message
                            Request request = requestRepository.createRequest(
                                    msg.marketId,
                                    msg.userId,
                                    msg.companyId,
                                    Request.BUY,
                                    msg.shares
                            );
                            // requesting for current share price from market
                            // now, request is in the share price check state
                            request.setState(Request.SHARE_PRICE_CHECK);
                            // message to market actor
                            SharePriceRequest sharePriceRequest = new SharePriceRequest(
                                    request.requestId,
                                    request.marketId,
                                    request.companyId
                            );
                            marketActor.tell(sharePriceRequest, getSelf());
                            // sending a chit message to sender
                            Chit chit = new Chit(Chit.SUCCESS, request.requestId, "processing buy request");
                            getSender().tell(chit, getSelf());
                        }
                )
                .match( // message handle for received SharePriceResponse
                        SharePriceResponse.class,
                        msg -> {
                            Request request = requestRepository.getRequest(msg.requestId);
                            request.setState(Request.BANK_ACCOUNT_CHECK); // change of state
                            // amount to withdraw or deposit
                            double amount = msg.sharePrice * request.shares;
                            // bank account id of the user
                            String accountId = portFolioRepository.getPortfolio(
                                    request.marketId,
                                    request.userId
                            ).getAccountId();
                            if(request.type.equals(Request.BUY)){
                                // request is a buy, then needs to withdraw
                                Withdraw withdraw = new Withdraw(
                                        request.requestId,
                                        accountId,
                                        amount
                                );
                                bankActor.tell(withdraw, getSelf());
                            }else if(request.type.equals(Request.SELL)){
                                // request is a sell, then needs to deposit
                                Deposit deposit = new Deposit(
                                        request.requestId,
                                        accountId,
                                        amount
                                );
                                bankActor.tell(deposit, getSelf());
                            }
                        }
                )
                .match( // message handle for received Ack from Bank
                        Ack.class,
                        msg -> {
                            // the request
                            Request request = requestRepository.getRequest(msg.referenceId);
                            // portfolio to update
                            Portfolio portfolio = portFolioRepository.getPortfolio(
                                    request.marketId,
                                    request.userId
                            );
                            if(request.type.equals(Request.BUY)){
                                portfolio.increaseShares(
                                        request.companyId,
                                        request.shares
                                );
                                request.setState(Request.COMPLETED);
                            }else if(request.type.equals(Request.SELL)){
                                portfolio.decreaseShares(
                                        request.companyId,
                                        request.shares
                                );
                                request.setState(Request.COMPLETED);
                            }
                        }
                )
                .match( // message handle for received UnAck message
                        UnAck.class,
                        msg -> {
                            // the request
                            Request request = requestRepository.getRequest(msg.referenceId);
                            request.setState(Request.FAILED);
                        }
                )
                .match(// message handle for receiving AccountCreated
                        AccountCreated.class,
                        msg -> {
                            Portfolio portfolio = portFolioRepository.getPortfolio(msg.referenceId);
                            portfolio.setAccountId(msg.accountId);
                        }
                )
                .match( // message handle for receiving AccountNotCreated
                        AccountNotCreated.class,
                        msg -> {
                            Portfolio portfolio = portFolioRepository.getPortfolio(msg.referenceId);
                            bankActor.tell(
                                    new CreateAccount(msg.referenceId,portfolio.name, INITIAL_DEPOSIT),
                                    getSelf()
                            );
                        }
                )
                .match(// message handle for receiving CompanyListResponse
                        CompanyListResponse.class,
                        msg -> {
                            Map<String, Portfolio> marketPortfolios = portFolioRepository.getPortfolios(msg.marketId);
                            marketPortfolios.forEach(
                                    (userId, portfolio) -> {
                                        for(CompanyDetails company: msg.companyDetails){
                                            portfolio.addCompany(company.sectorId, company.companyId);
                                        }
                                    }
                            );
                        }
                )
                .match( // message handle for receiving AllPortfoliosRequest
                        AllPortfoliosRequest.class,
                        msg -> {
                            // portfolios of the given market
                            Map<String,Portfolio> portfolios = portFolioRepository
                                    .getPortfolios(msg.marketId);
                            // sending message
                            AllPortFoliosResponse response = new AllPortFoliosResponse(
                                    msg.marketId,
                                    portfolios
                            );
                            // send message
                            getSender().tell(response, getSelf());
                        }
                )
                .match(// message handle for receiving HandOverChit message
                        HandOverChit.class,
                        msg -> {
                            Request request = requestRepository.getRequest(msg.requestId);
                            if(request.getState().equals(Request.COMPLETED)){
                                getSender().tell(
                                        new ChitVerification(
                                                ChitVerification.COMPLETED,
                                                request.type + " request successful",
                                                request.requestId
                                        ),
                                        getSelf()
                                );
                            }else if(request.getState().equals(Request.FAILED)){
                                getSender().tell(
                                        new ChitVerification(
                                                ChitVerification.FAILED,
                                                request.type + " request unsuccessful",
                                                request.requestId
                                        ),
                                        getSelf()
                                );
                            }else {
                                getSender().tell(
                                        new ChitVerification(
                                                ChitVerification.WAIT,
                                                request.type + " request still in process",
                                                request.requestId
                                        ),
                                        getSelf()
                                );
                            }
                        }
                )
                .build();
    }

    public static Props props(ActorRef marketActor, ActorRef bankActor) {
            return Props.create(BrokerActor.class, () -> new BrokerActor(marketActor, bankActor));

    }
}
