package broker;

import broker.exception.MarketNotFoundException;
import broker.exception.PortfolioNotFoundException;

import java.util.HashMap;
import java.util.Map;
import java.util.concurrent.atomic.AtomicReference;

/*
* This class is used by the Broker actor to
* create and manage investor Portfolios
* */
public class PortFolioRepository {
    /* state and house keeping variables */
    private Map<String, Map <String, Portfolio>> portfolios;

    /* constructor */
    public PortFolioRepository() {
        portfolios = new HashMap<>();
    }

    /* interface */

    /*
    * This method is used to create a new Portfolio
    * under the given Market
    * */
    public void createPortfolio(String marketId, String userId, String name){
        if(!portfolios.containsKey(marketId)){
            portfolios.put(marketId, new HashMap<>());
        }
        portfolios.get(marketId).put(
                userId,
                new Portfolio(marketId, userId, name)
        );
    }

    /*
    * This method is used to get a Portfolio that belongs
    * to the given Investor of the given Market
    * */
    public Portfolio getPortfolio(String marketId, String userId) throws PortfolioNotFoundException {
        Portfolio portfolio = null;
        if(portfolios.containsKey(marketId)){
            portfolio = portfolios.get(marketId).get(userId);
        }
        if(portfolio == null)
            throw new PortfolioNotFoundException();
        return portfolio;
    }

    /*
    * This method is used to get a Portfolio of
    * a given Investor's id
    * */
    public Portfolio getPortfolio(String userId) throws PortfolioNotFoundException {
        AtomicReference<Portfolio> portfolio = new AtomicReference<>();
        portfolios.forEach(
                (marketId, portfolioMap) -> {
                    portfolioMap.forEach(
                            (uId, userPortfolio)-> {
                                if(uId.equals(userId)){
                                    portfolio.set(userPortfolio);
                                }
                            }
                    );
                }
        );
        if(portfolio.get() == null)
            throw new PortfolioNotFoundException();
        return portfolio.get();
    }

    /*
    * This method is used to get a Map of Portfolios
    * for the given Market
    * */
    public Map<String, Portfolio> getPortfolios(String marketId) throws MarketNotFoundException {
        Map<String, Portfolio> portfolioMap = null;
        if(portfolios.containsKey(marketId)){
            portfolioMap = portfolios.get(marketId);
        }
        if(portfolioMap == null)
            throw new MarketNotFoundException();
        return portfolioMap;
    }
}
