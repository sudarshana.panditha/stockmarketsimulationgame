package broker.messages;
/*
 * This is a message class used to tell the Broker
 * Actor to Buy the given amount of shares from the
 * given Company for the given investor of a given
 * Market
 * */
public class Buy {
    public final String marketId;
    public final String userId;
    public final String companyId;
    public final long shares;

    public Buy(String marketId, String userId, String companyId, long shares) {
        this.marketId = marketId;
        this.userId = userId;
        this.companyId = companyId;
        this.shares = shares;
    }
}
