package broker.messages;
/*
* This message class is used by the client to get
* confirmation on previously received Chit message,
* to know whether the Buy or Sell request is completed,
* failed, or whether the client needs to wait for
* processing
* */
public class HandOverChit {
    public final String requestId;

    /* constructor */
    public HandOverChit(String requestId) {
        this.requestId = requestId;
    }
}
