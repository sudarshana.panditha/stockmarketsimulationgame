package broker.messages;

import java.io.Serializable;
/*
* This class is used notify the investor whether a
* sent Buy or Sell is been processed or a error
* has occurred
* */
public class Chit implements Serializable{
    /* constants for message status */
    public static final String SUCCESS = "SUCCESS";
    public static final String ERROR = "ERROR";

    /* message state */
    public final String status;
    public final String requestId;
    public final String message;

    /* constructor */
    public Chit(String status, String requestId, String message) {
        this.status = status;
        this.requestId = requestId;
        this.message = message;
    }

    @Override
    public boolean equals(Object obj) {
        if(this == obj) return true;
        if(obj == null) return false;
        if(getClass() != obj.getClass())
            return false;
        Chit chit = (Chit) obj;
        return this.status.equals(chit.status);
    }
}
