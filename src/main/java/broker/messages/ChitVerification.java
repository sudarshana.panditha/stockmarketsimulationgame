package broker.messages;

import java.io.Serializable;

/*
* This class is used to respond to a received
* HandOverChit message by BrokerActor to tell
* the client whether the associated Buy or Sell
* request is completed or failed or is still in
* process
* */
public class ChitVerification implements Serializable{
    /* constants */
    public static final String COMPLETED = "COMPLETED";
    public static final String FAILED = "FAILED";
    public static final String WAIT = "WAIT";

    public final String state; // state of the associated request
    public final String message;
    public final String requestId;

    /* constructor */
    public ChitVerification(String state, String message, String requestId) {
        this.state = state;
        this.message = message;
        this.requestId = requestId;
    }

    @Override
    public boolean equals(Object obj) {
        if(this == obj) return true;
        if(obj == null) return false;
        if(getClass() != obj.getClass())
            return false;
        ChitVerification chitVerification = (ChitVerification) obj;
        return this.state.equals(chitVerification.state)
                && this.requestId.equals(chitVerification.requestId);
    }
}
