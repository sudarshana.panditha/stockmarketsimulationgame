package broker.messages;

import broker.Portfolio;

import java.io.Serializable;
import java.util.Map;
/*
* This message class is used to send a response to
* AllPortfoliosRequest by BrokerActor by sending
* all the Portfolios of the given Market
* */
public class AllPortFoliosResponse implements Serializable {
    public final String marketId;
    public Map<String,Portfolio> portfolios; // map of userId to portfolio

    public AllPortFoliosResponse(String marketId, Map<String,Portfolio> portfolios) {
        this.marketId = marketId;
        this.portfolios = portfolios;
    }

    @Override
    public boolean equals(Object obj) {
        if(this == obj) return true;
        if(obj == null) return false;
        if(getClass() != obj.getClass())
            return false;
        AllPortFoliosResponse res = (AllPortFoliosResponse) obj;
        return this.marketId.equals(res.marketId);
    }
}
