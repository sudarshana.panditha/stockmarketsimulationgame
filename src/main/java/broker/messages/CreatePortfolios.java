package broker.messages;

import java.util.Map;

/*
* This is a message class that is used to request
* the Broker actor to create Portfolios for the
* given investors of the given Market
* */
public class CreatePortfolios {
    public final String marketId;
    public final Map<String,String> marketUsers; // user id mapped into username

    public CreatePortfolios(String marketId, Map<String, String> marketUsers) {
        this.marketId = marketId;
        this.marketUsers = marketUsers;
    }

    @Override
    public boolean equals(Object obj) {
        if(this == obj) return true;
        if(obj == null) return false;
        if(getClass() != obj.getClass())
            return false;
        CreatePortfolios msg = (CreatePortfolios) obj;
        return this.marketId.equals(msg.marketId);
    }
}
