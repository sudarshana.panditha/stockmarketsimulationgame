package broker.messages;
/*
* This message class is used to request all the
* Investor Portfolios that belongs to the given
* Market
* */
public class AllPortfoliosRequest {
    public final String marketId;

    /* constructor */
    public AllPortfoliosRequest(String marketId) {
        this.marketId = marketId;
    }

    @Override
    public boolean equals(Object obj) {
        if(this == obj) return true;
        if(obj == null) return false;
        if(getClass() != obj.getClass())
            return false;
        AllPortfoliosRequest req = (AllPortfoliosRequest) obj;
        return this.marketId.equals(req.marketId);
    }
}
