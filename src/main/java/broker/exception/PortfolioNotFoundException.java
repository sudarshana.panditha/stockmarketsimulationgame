package broker.exception;
/*
* This exception class is thrown when
* PortfolioRepository can not find a Portfolio
* */
public class PortfolioNotFoundException extends Exception {
}
