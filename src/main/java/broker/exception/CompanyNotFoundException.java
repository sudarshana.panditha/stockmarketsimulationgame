package broker.exception;
/*
* This class is used to throw an Exception
* when getShares method of Portfolio class is called
* with an invalid company id
* */
public class CompanyNotFoundException extends Exception {
}
