package broker.exception;
/*
* This class is used to throw an Exception
* when there is no matching Market to get
* Portfolios from
* */
public class MarketNotFoundException extends Exception {
}
