package broker.exception;
/*
* This class is used to throw an Exception when
* calling getRequest on RequestRepository with an
* invalid request id
* */
public class RequestNotFoundException extends Exception {
}
