package broker.exception;
/*
* This class is used to throw an error when a state
* that contradicts the order of state transition of a
* Request object is used to set on a Request object
* */
public class InvalidStateTransitionException extends Exception{
}
