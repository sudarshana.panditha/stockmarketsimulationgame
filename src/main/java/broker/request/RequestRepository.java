package broker.request;

import broker.exception.RequestNotFoundException;

import java.util.Collections;
import java.util.HashMap;
import java.util.Map;
import java.util.UUID;

/*
* This class is used to create and manage
* Requests for Broker Actor
* */
public class RequestRepository {
    /* state and house keeping variables */
    private Map<String, Request> requests;

    /* constructor */
    public RequestRepository() {
        requests = new HashMap<>();
    }

    /* interface */
    /*
    * This method is used to create a new Request
    * for the BrokerActor and persist it in the
    * RequestRepository
    * */
    public Request createRequest(String marketId, String userId,
                                 String companyId, String type, long shares){
        Request request = new Request(
                UUID.randomUUID().toString(),
                marketId,
                userId,
                companyId,
                type,
                shares
        );
        requests.put(request.requestId, request);
        return request;
    }
    /*
    * This method is used to get the Request for
    * the given requestId
    * */
    public Request getRequest(String requestId) throws RequestNotFoundException {
        Request request = null;
        if(requests.containsKey(requestId)){
            request = requests.get(requestId);
        }
        if (request == null){
            throw new RequestNotFoundException();
        }
        return request;
    }

    /*
    * This method returns an unmodifiable Map of
    * the set of requests
    * */

    public Map<String, Request> getRequests() {
        return Collections.unmodifiableMap(requests);
    }
}
