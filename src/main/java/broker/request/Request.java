package broker.request;

import broker.exception.InvalidStateTransitionException;

/*
* This class is used by the Broker Actor to keep track of
* the state of a Sell or Buy request through its lifetime
* of being created, being checked for the current share
* price by the MarketActor , being checked by the BankActor
* for enough balance and either completing or failing
* to execute
* */
public class Request {
    /* constants for type of request */
    public static final String BUY = "BUY";
    public static final String SELL = "SELL";

    /* constants for state of request */
    public static final String CREATED = "CREATED";
    public static final String SHARE_PRICE_CHECK = "SHARE_PRICE_CHECK";
    public static final String BANK_ACCOUNT_CHECK = "BANK_ACCOUNT_CHECK";
    public static final String COMPLETED = "COMPLETED";
    public static final String FAILED = "FAILED";

    /* state and house keeping variables */
    public final String requestId;
    public final String marketId;
    public final String userId;
    public final String companyId;
    public final String type;
    public final long shares;
    private  String state; // current state of the Request

    /* constructor */
    public Request(String requestId, String marketId, String userId,
                   String companyId, String type, long shares) {
        this.requestId = requestId;
        this.marketId = marketId;
        this.userId = userId;
        this.companyId = companyId;
        this.type = type;
        this.shares = shares;
        // initialize the initial state
        state = Request.CREATED;
    }

    /* interface */
    /* getters and setter for state */
    public String getState() {
        return state;
    }

    public void setState(String state) throws InvalidStateTransitionException {
        if(this.state.equals(CREATED) && !state.equals(SHARE_PRICE_CHECK)){
            throw new InvalidStateTransitionException();
        } else if(this.state.equals(SHARE_PRICE_CHECK) && !state.equals(BANK_ACCOUNT_CHECK)){
            throw new InvalidStateTransitionException();
        } else if(this.state.equals(BANK_ACCOUNT_CHECK)
                && !(state.equals(COMPLETED) || state.equals(FAILED))){
            throw new InvalidStateTransitionException();
        }
        this.state = state;
    }
}
