package gamemaker.exception;
/*
* This class is used to throw an Exception
* when a getGame call on a GameRepository
* can not find a Game for the given game id
* */
public class GameNotFoundException extends Exception {
}
