package gamemaker.exception;
/*
* This class is used to throw an Exception when
* the added number of players exceeds the max
* number of players of four per game
* */
public class PlayersOutOfBoundException extends Exception {
}
