package gamemaker.exception;
/*
* This class is used throw an exception
* when an illegal state is set on a
* Game object that contradicts the order
* of state transition of a Game object
* */
public class InvalidGameStateException extends Exception {
}
