package gamemaker;

import akka.actor.AbstractActor;
import akka.actor.ActorRef;
import broker.messages.CreatePortfolios;
import clock.messages.CreateGameClock;
import gamemaker.exception.GameNotFoundException;
import gamemaker.game.Game;
import gamemaker.game.GameRepository;
import gamemaker.messages.Ack;
import gamemaker.messages.GameRequest;
import gamemaker.messages.GameResponse;
import gamemaker.messages.Play;
import market.messages.CreateMarket;

import java.util.ArrayList;

/*
 * This is an Akka actor that responds to Play requests,
 * create and add Players to Games and instantiate other
 * actors such as MarketActor, ClockActor, and BrokerActor
 * with data of newly created Games
 * */
public class GameMaker extends AbstractActor{
    /* constants */
    public static final int MAX_NUM_OF_TURNS = 120;

    /* state and housekeeping variables */
    private GameRepository gameRepository;
    private ActorRef clockActor;
    private ActorRef marketActor;
    private ActorRef brokerActor;

    /* constructor */
    public GameMaker(GameRepository gameRepository, ActorRef clockActor,
                     ActorRef marketActor, ActorRef brokerActor) {
        this.gameRepository = gameRepository;
        this.clockActor = clockActor;
        this.marketActor = marketActor;
        this.brokerActor = brokerActor;
    }

    public GameMaker(ActorRef clockActor, ActorRef marketActor, ActorRef brokerActor) {
        this.clockActor = clockActor;
        this.marketActor = marketActor;
        this.brokerActor = brokerActor;
        this.gameRepository = new GameRepository();
    }

    @Override
    public Receive createReceive() {
        return receiveBuilder()
                .match( // message handle for received Play message
                        Play.class,
                        msg -> {
                            Game game = null;
                            try{
                                game = gameRepository.getNextGame();
                            }catch (GameNotFoundException e){
                                game = gameRepository.createGame();
                            }
                            game.addPlayer(msg.userId, msg.username);
                            // sending message
                            Ack ack = new Ack(game.gameId);
                            getSender().tell(ack, getSelf());
                        }
                )
                .match( // message handle for received GameRequest message
                        GameRequest.class,
                        msg -> {
                            Game game = gameRepository.getGame(msg.gameId);

                            if(game.getState().equals(Game.NOT_READY)){
                                getSender().tell(
                                        new GameResponse(GameResponse.WAIT),
                                        getSelf()
                                );
                            }else if(game.getState().equals(Game.READY)){
                                getSender().tell(
                                        new GameResponse(GameResponse.CREATED),
                                        getSelf()
                                );
                                // sending message to market
                                marketActor.tell(
                                        new CreateMarket(game.gameId,MAX_NUM_OF_TURNS),
                                        getSelf()
                                );
                                // sending message to clock
                                clockActor.tell(
                                        new CreateGameClock(
                                                game.gameId,
                                                new ArrayList<>(game.getPlayers().keySet()),
                                                MAX_NUM_OF_TURNS),
                                        getSelf()
                                );
                                // sending message to broker
                                brokerActor.tell(
                                        new CreatePortfolios(msg.gameId, game.getPlayers()),
                                        getSelf()
                                );
                            }else if(game.getState().equals(Game.CREATED)){
                                getSender().tell(
                                        new GameResponse(GameResponse.CREATED),
                                        getSelf()
                                );
                            }
                        }
                )
                .build();
    }
}
