package gamemaker.game;

import gamemaker.exception.GameNotFoundException;

import java.util.HashMap;
import java.util.Map;
import java.util.UUID;

/*
* This class is used to create, manage, and
* persist different Game objects of different
* Games and generate ids for Games
* */
public class GameRepository {
    /* state and housekeeping variables */
    private Map<String, Game> games;
    private Game nextGame;

    /* constructor */
    public GameRepository() {
        games = new HashMap<>();
        nextGame = null;
    }

    /* interface */
    /*
    * This method is used to create a Game
    * in the repository and return it
    * */
    public Game createGame(){
        final String gameId = UUID.randomUUID().toString();
        Game game = new Game(gameId);
        games.put(gameId, game);
        // set next game
        nextGame = game;
        return game;
    }

    /*
    * This method is used to get the Game with
    * the given gameId
    * */
    public Game getGame(String gameId) throws GameNotFoundException {
        Game game = null;
        if(games.containsKey(gameId))
            game = games.get(gameId);
        if(game == null)
            throw new GameNotFoundException();
        return game;
    }

    /*
    * This method returns the next Game that is
    * in a NOT_READY state
    * */

    public Game getNextGame() throws GameNotFoundException {
        if(nextGame == null || !nextGame.getState().equals(Game.NOT_READY))
            throw new GameNotFoundException();
        return nextGame;
    }
}
