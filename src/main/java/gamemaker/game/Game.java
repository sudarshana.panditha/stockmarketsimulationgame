package gamemaker.game;

import gamemaker.exception.InvalidGameStateException;
import gamemaker.exception.PlayersOutOfBoundException;

import java.util.Collections;
import java.util.HashMap;
import java.util.Map;

/*
* This class is used to keep track of necessary
* data to create a Game such the Player details,
* the current state of the Game, i.e., whether it
* is in a not ready, ready or created, and etch.
* */
public class Game {
    /* constants for state */
    public static final String NOT_READY = "NOT_READY";
    public static final String READY = "READY";
    public static final String CREATED = "CREATED";

    /* state and housekeeping variables */
    public final String gameId;
    private Map<String, String> players;
    private String state;

    /* constructor */
    public Game(String gameId) {
        this.gameId = gameId;
        players = new HashMap<>();
        this.state = Game.NOT_READY;
    }

    /* interface */
    // getter for state
    public String getState() {
        return state;
    }
    // setter for state
    public void setState(String state) throws InvalidGameStateException {
        if(state.equals(READY) && players.size() != 4)
            throw new InvalidGameStateException();

        if(this.state.equals(NOT_READY) && !state.equals(READY))
            throw new InvalidGameStateException();
        if(this.state.equals(READY) && !state.equals(CREATED))
            throw new InvalidGameStateException();
        if(this.state.equals(CREATED))
            throw new InvalidGameStateException();
        this.state = state;
    }

    // getter for players
    public Map<String, String> getPlayers() {
        return Collections.unmodifiableMap(players);
    }

    /*
    * This method is used to add a player
    * to this Game and only four players are
    * added to a Game
    * */
    public void addPlayer(final String userId,final String username) throws PlayersOutOfBoundException {
        if(players.size() == 4)
            throw new PlayersOutOfBoundException();
        players.put(userId, username);
        if(players.size() == 4){
            try{
                setState(Game.READY);
            }catch (InvalidGameStateException e){

            }
        }
    }
}
