package gamemaker.messages;

import java.io.Serializable;
/*
* This message class is used to respond the
* player whether the Game is created to start or
* whether the player needs to wait for the Game
* to setup
* */
public class GameResponse implements Serializable {
    /* constants for state */
    public static final String WAIT = "WAIT";
    public static final String CREATED = "CREATED";

    /* instance variables*/
    public final String state;

    /* constructor */

    public GameResponse(String state) {
        this.state = state;
    }

    @Override
    public boolean equals(Object obj) {
        if(this == obj) return true;
        if(obj == null) return false;
        if(getClass() != obj.getClass())
            return false;
        GameResponse res = (GameResponse) obj;
        return this.state.equals(res.state);
    }
}
