package gamemaker.messages;

import java.io.Serializable;

/*
* This is a message class used to reply to
* a Player who previously sent a Play request
* with the Game to which the player was added
* */
public class Ack implements Serializable {
    public final String gameId;

    /* constructor */
    public Ack(String gameId) {
        this.gameId = gameId;
    }

    @Override
    public boolean equals(Object obj) {
        if(this == obj) return true;
        if(obj == null) return false;
        if(getClass() != obj.getClass())
            return false;
        Ack ack = (Ack) obj;
        return this.gameId.equals(ack.gameId);
    }
}
