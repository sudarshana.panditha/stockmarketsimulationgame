package gamemaker.messages;
/*
* This is a message class used to request
* GameMaker to add the sending Player to
* a Game
* */
public class Play {
    public final String userId; // id of the player
    public final String username; // username of the player

    /* constructor */
    public Play(String userId, String username) {
        this.userId = userId;
        this.username = username;
    }
}
