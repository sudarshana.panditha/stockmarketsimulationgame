package gamemaker.messages;
/*
* This message class is used to get the Game for the
* game id received from an Ack message from the Broker
* */
public class GameRequest {
    public final String gameId;

    /* constructor */
    public GameRequest(String gameId) {
        this.gameId = gameId;
    }
}
