package market;

import akka.actor.AbstractActor;
import akka.actor.Props;
import market.messages.*;

import java.util.List;
import java.util.Map;

/*
* This is the Akka actor that represents a Market
* in the Game that receives and responds to messages
* regarding the Market
* */
public class MarketActor extends AbstractActor{
    @Override
    public Receive createReceive() {
        return receiveBuilder()
                .match( // message handle for received CreateMarket message
                        CreateMarket.class,
                        msg -> {
                            marketRepository.createMarket(
                                    msg.marketId,
                                    msg.maxNumOfTurns
                            );
                        }
                )
                .match(
                        // message handle for received SharePriceRequest
                        SharePriceRequest.class,
                        msg -> {
                            double sharePrice = marketRepository.getSharePrice(
                                    msg.marketId,
                                    msg.companyId
                            );

                            getSender().tell(
                                    new SharePriceResponse(msg.requestId,sharePrice),
                                    getSelf()
                            );
                        }
                )
                .match(
                        // message handle for received CurrentMarketTurn
                        CurrentMarketTurn.class,
                        msg -> {
                            marketRepository.setCurrentTurn(
                                    msg.marketId,
                                    msg.currentTurn
                            );
                        }
                )
                .match(
                        // message handle for received CompanyListRequest
                        CompanyListRequest.class,
                        msg -> {
                            List<CompanyDetails> companyDetails = marketRepository.getCompanyDetailsList(
                                    msg.marketId
                            );
                            getSender().tell(
                                    new CompanyListResponse(msg.marketId,companyDetails),
                                    getSelf()
                            );
                        }
                )
                .match(
                        // message handle for received AllSharePricesRequest
                        AllSharePricesRequest.class,
                        msg -> {
                            Map<String, Map<String, Double>> allSharePrices = marketRepository
                                    .getAllSharePrices(msg.marketId);
                            getSender().tell(new AllSharePricesResponse(msg.marketId, allSharePrices), getSelf());
                        }
                )
                .match(
                        // message handle for AllTrendsRequest
                        AllTrendsRequest.class,
                        msg -> {
                            // get trend data
                            Map<String, int [] > sectorTrends = marketRepository.getSectorTrends(msg.marketId);
                            Map<String, int [] > randomTrends = marketRepository.getRandomTrends(msg.marketId);
                            int marketTrend []  = marketRepository.getMarketTrend(msg.marketId);
                            Event eventTrend [] = marketRepository.getEventTrend(msg.marketId);

                            getSender().tell(
                                    new AllTrendsResponse( // send AllTrendsResponse
                                            sectorTrends,
                                            randomTrends,
                                            marketTrend,
                                            eventTrend
                                    ),
                                    getSelf()
                            );
                        }
                )
                .match(
                        // message handle for received MarketMetaDataRequest
                        MarketMetaDataRequest.class,
                        msg -> {
                            getSender().tell(
                                    new MarketMetaDataResponse(
                                            marketRepository.getSectors(msg.marketId),
                                            marketRepository.getSectorCompanyDetails(msg.marketId)
                                    ),
                                    getSelf()
                            );
                        }
                )
                .build();
    }

    /* state and house keeping variables */
    MarketRepository marketRepository;

    /* constructors */
    public MarketActor() {
        marketRepository = new MarketRepository();
    }

    public MarketActor(MarketRepository marketRepository) {
        this.marketRepository = marketRepository;
    }

    public static Props props() {
        return Props.create(MarketActor.class, () -> new MarketActor());
    }
}
