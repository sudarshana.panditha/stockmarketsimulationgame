package market.exception;
/*
* This class is used to throw an Exception when illegal
* arguments are passed at the Market constructor
* */
public class IllegalArgumentException extends Exception{
}
