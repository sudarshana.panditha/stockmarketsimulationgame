package market.exception;
/*
* This class is used to throw an Exception when the
* Market can not find a Company for a method call
* */
public class CompanyNotFoundException extends Exception{
}
