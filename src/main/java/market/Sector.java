package market;

import java.io.Serializable;

public class Sector implements Serializable{
    public final String sectorId; // id of the sector
    public final String name; // name of the sector

    public Sector(String sectorId, String name) {
        this.sectorId = sectorId;
        this.name = name;
    }
}
