package market;

import market.exception.DurationNotPositiveException;

import java.io.Serializable;

public class StockEvent extends Event implements Serializable{
    public final String companyId; // the company to which the event applies

    public StockEvent(String companyId, int value, int duration) throws DurationNotPositiveException {
        super(value, duration);
        this.companyId = companyId;
    }
}
