package market;

public class Company {
    public final String companyId; // id of the company
    public final String sectorId; // sector of company
    public final String name; // name of the company
    private double sharePrice; // current share price

    public Company(String companyId, String sectorId, String name, double sharePrice) {
        this.companyId = companyId;
        this.sectorId = sectorId;
        this.name = name;
        this.sharePrice = sharePrice;
    }

    public double getSharePrice() {
        return sharePrice;
    }

    public void setSharePrice(double sharePrice) {
        this.sharePrice = sharePrice;
    }
}
