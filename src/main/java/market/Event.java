package market;

import market.exception.DurationNotPositiveException;

import java.io.Serializable;

public abstract class Event implements Serializable{
    public final int value; // percentage value of the event
    public final int duration; // how long the event lasts

    public Event(int value, int duration) throws DurationNotPositiveException {
        this.value = value;
        this.duration = duration;
        if(this.duration <= 0){
            throw new DurationNotPositiveException();
        }
    }
}
