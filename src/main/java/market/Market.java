package market;

import market.exception.CompanyNotFoundException;
import market.exception.IllegalArgumentException;
import market.trendmodels.EventTrendModel;
import market.trendmodels.MarketTrendModel;
import market.trendmodels.RandomTrendModel;
import market.trendmodels.SectorTrendModel;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

/*
* This class represents a Market in the Game
* */
public class Market {
    /* state and house keeping variables */
    public final String marketId;
    public final int maxNumOfTurns;
    private final MarketModel marketModel;
    private int currentTurn;
    // Event Trend component of the Value Change Algorithm
    private EventTrendModel eventTrendModel;
    // Market Trend component of the Value Change Algorithm
    private MarketTrendModel marketTrendModel;
    // Sector Trend component of the Value Change Algorithm
    private SectorTrendModel sectorTrendModel;
    // Random Trend component of the Value Change Algorithm
    private RandomTrendModel randomTrendModel;

    /* constructor */
    public Market(String marketId, int maxNumOfTurns, MarketModel marketModel) throws IllegalArgumentException {
        this.marketId = marketId;
        this.maxNumOfTurns = maxNumOfTurns;
        this.marketModel = marketModel;
        if(this.marketId == null)
            throw new IllegalArgumentException();
        if (this.marketModel == null)
            throw new IllegalArgumentException();
        if(maxNumOfTurns <= 0)
            throw new IllegalArgumentException();
        currentTurn = 1; // initial turn
        // initializing trend models
        eventTrendModel = new EventTrendModel(marketModel,maxNumOfTurns);
        marketTrendModel = new MarketTrendModel(maxNumOfTurns);
        sectorTrendModel = new SectorTrendModel(maxNumOfTurns,marketModel);
        randomTrendModel = new RandomTrendModel(maxNumOfTurns, marketModel);
    }

    /* interface */
    /*
    * This method is used to get the current share price of a
    * given company
    * */
    public double getSharePrice(String companyId) throws CompanyNotFoundException {
        List<Company> companies = marketModel.getCompanies();
        Company requestedCompany = null;

        for(Company company: companies){
            if(company.companyId.equals(companyId)){
                requestedCompany = company;
            }
        }
        if(requestedCompany == null){
            throw new CompanyNotFoundException();
        }
        return requestedCompany.getSharePrice();
    }

    /*
    * This method is used to get the events trend data of
    * this Market
    * */
    public Event [] getEventTrend(){
        Event events [] = new Event[currentTurn];
        for (int i = 0; i < currentTurn; ++i){
            events[i] = eventTrendModel.getEvent(i);
        }
        return events;
    }

    /*
    * This method is used to get the market trend data of
    * this Market
    * */
    public int [] getMarketTrend(){
        int marketTrend [] = new int[currentTurn];
        for (int i = 0 ; i < currentTurn; ++i){
            marketTrend[i] = marketTrendModel.getValue(i);
        }
        return marketTrend;
    }

    /*
    * This method is used to get a map of the sector
    * trends of this Market
    * */
    public Map<String, int []> getSectorTrends(){
        List<Sector> sectors = getSectors();
        Map<String, int []> sectorTrends = new HashMap<>();

        for(Sector sector: sectors){
            int sectorTrend[] = new int[currentTurn];
            for(int i = 0; i < currentTurn; ++i){
                sectorTrend[i] = sectorTrendModel.getValue(sector.sectorId,i);
            }
            sectorTrends.put(sector.sectorId, sectorTrend);
        }

        return sectorTrends;
    }

    /*
     * This method is used to get a map of the Random
     * trends of this Market
     * */
    public Map<String, int []> getRandomTrends(){
        List<Company> companies = getCompanies();
        Map<String, int []> randomTrends = new HashMap<>();

        for(Company company: companies){
            int randomTrend[] = new int[currentTurn];
            for(int i = 0; i < currentTurn; ++i){
                randomTrend[i] = randomTrendModel.getValue(company.companyId,i);
            }
            randomTrends.put(company.companyId, randomTrend);
        }

        return randomTrends;
    }


    public int getCurrentTurn() {
        return currentTurn;
    }

    /*
    * This method is used to set the new current turn of the Market
    * and calculate the new share prices of each company
    * */
    public void setCurrentTurn(int currentTurn) {
        this.currentTurn = currentTurn;
        calculateSharePrices();
    }

    public List<Company> getCompanies(){
        return marketModel.getCompanies();
    }

    public List<Sector> getSectors(){
        return marketModel.getSectors();
    }

    /* helper methods */
    /*
    * This helper method is used to calculate the new share prices
    * when the new current turn is set using the setCurrentTurn method
    * */
    private void calculateSharePrices(){
        List<Company> companies = getCompanies();
        for(Company company: companies){
            int stockValueChange = 0; // value change percentage
            stockValueChange += randomTrendModel
                    .getValue(company.companyId,currentTurn); // random value change
            stockValueChange += sectorTrendModel
                    .getValue(company.sectorId, currentTurn); // sector value change
            stockValueChange += marketTrendModel
                    .getValue(currentTurn); // market value change
            Event event = eventTrendModel
                    .getEvent(currentTurn); // event at the current turn
            if(event != null){ // an event is in effect
                stockValueChange += event.value; // event value change
            }
            if(stockValueChange < 0){ // negative value change set to zero
                stockValueChange = 0;
            }
            double currentSharePrice = company.getSharePrice();
            double newSharePrice = currentSharePrice + (1.0* stockValueChange * currentSharePrice)/100.0;
            company.setSharePrice(newSharePrice);
        }
    }
}
