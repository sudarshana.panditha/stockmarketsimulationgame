package market;

import java.io.Serializable;

/*
* A value object used to send necessary
* details of a Company
* */
public class CompanyDetails implements Serializable{
    public final String sectorId;
    public final String companyId;
    public final String name;

    public CompanyDetails(String sectorId,String companyId, String name) {
        this.sectorId = sectorId;
        this.companyId = companyId;
        this.name = name;
    }

    @Override
    public boolean equals(Object obj) {
        if(this == obj) return true;
        if(obj == null) return false;
        if(getClass() != obj.getClass())
            return false;
        CompanyDetails companyDetails = (CompanyDetails) obj;
        return this.sectorId.equals(companyDetails.sectorId)
                && this.companyId.equals(companyDetails.companyId)
                && this.name.equals(companyDetails.name);
    }
}
