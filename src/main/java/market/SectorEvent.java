package market;

import market.exception.DurationNotPositiveException;

import java.io.Serializable;

public class SectorEvent extends Event implements Serializable{
    public final String sectorId; // the sector to which the event applies

    public SectorEvent(String sectorId, int value, int duration) throws DurationNotPositiveException {
        super(value, duration);
        this.sectorId = sectorId;
    }
}
