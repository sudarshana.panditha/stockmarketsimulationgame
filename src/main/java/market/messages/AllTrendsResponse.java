package market.messages;

import market.Event;

import java.io.Serializable;
import java.util.Map;

/*
* This message class is used to respond with all
* the value trend data of the requested Market
* */
public class AllTrendsResponse implements Serializable{
    public final Map<String, int [] > sectorTrends; // set of sector trends
    public final Map<String, int [] > randomTrends; // set of random trends
    public final int marketTrend []; // the market trend
    public final Event eventTrend []; // the event trend

    /* constructor */
    public AllTrendsResponse(
            Map<String, int[]> sectorTrends,
            Map<String, int[]> randomTrends,
            int[] marketTrend,
            Event[] eventTrend) {
        this.sectorTrends = sectorTrends;
        this.randomTrends = randomTrends;
        this.marketTrend = marketTrend;
        this.eventTrend = eventTrend;
    }

    @Override
    public boolean equals(Object obj) {
        return obj instanceof AllTrendsResponse;
    }
}
