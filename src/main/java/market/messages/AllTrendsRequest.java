package market.messages;
/*
* This message is used to request all the
* value trends of a given Market
* */
public class AllTrendsRequest {
    public final String marketId;
    /* constructor */
    public AllTrendsRequest(String marketId) {
        this.marketId = marketId;
    }
}
