package market.messages;
/*
* This message class is used to request the
* Meta data of a Market, i.e., the details of
* Sectors and their corresponding Companies
* */
public class MarketMetaDataRequest {
    public final String marketId;

    public MarketMetaDataRequest(String marketId) {
        this.marketId = marketId;
    }
}
