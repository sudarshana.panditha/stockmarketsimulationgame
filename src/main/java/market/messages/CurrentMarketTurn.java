package market.messages;

/*
* This message is used to notify the Market actor
* that the turn is complete and a new turn of the Market
* has begun
* */
public class CurrentMarketTurn {
    public final String marketId; // market that has ended a turn
    public final int currentTurn; // the new turn of the market

    public CurrentMarketTurn(String marketId, int currentTurn) {
        this.marketId = marketId;
        this.currentTurn = currentTurn;
    }

    @Override
    public boolean equals(Object obj) {
        if(this == obj) return true;
        if(obj == null) return false;
        if(getClass() != obj.getClass())
            return false;
        CurrentMarketTurn msg = (CurrentMarketTurn) obj;
        return this.marketId.equals(msg.marketId)
                && this.currentTurn == msg.currentTurn;
    }
}
