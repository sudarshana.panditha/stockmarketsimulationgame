package market.messages;
/*
* This message is used to request the Market actor
* the current share price of a Company. This is used
* by the Broker actor to get share prices when Buying
* or Selling shares for an investor
* */
public class SharePriceRequest {
    public final String requestId; // used by Broker to identify the request
    public final String marketId; // the Market to which the company belongs to
    public final String companyId; // the Company

    public SharePriceRequest(String requestId, String marketId, String companyId) {
        this.requestId = requestId;
        this.marketId = marketId;
        this.companyId = companyId;
    }

    @Override
    public boolean equals(Object obj) {
        return obj instanceof SharePriceRequest;
    }
}
