package market.messages;
/*
* This message represents a request for all the
* share prices of the Companies in each Sector
* of the Market
* */
public class AllSharePricesRequest {
    public final String marketId; // the Market

    /* constructor */
    public AllSharePricesRequest(String marketId) {
        this.marketId = marketId;
    }

    @Override
    public boolean equals(Object obj) {
        if(this == obj) return true;
        if(obj == null) return false;
        if(getClass() != obj.getClass())
            return false;
        AllSharePricesRequest req = (AllSharePricesRequest) obj;
        return this.marketId.equals(req.marketId);
    }
}
