package market.messages;

import market.CompanyDetails;
import market.Sector;

import java.io.Serializable;
import java.util.List;
import java.util.Map;

/*
* This message class is used to respond to
* a MarketMetaDataRequest with the Meta data
* of the requested Market
* */
public class MarketMetaDataResponse implements Serializable{
    public final List<Sector> sectors;
    public final Map<String, List<CompanyDetails>> companies;

    public MarketMetaDataResponse(List<Sector> sectors, Map<String, List<CompanyDetails>> companies) {
        this.sectors = sectors;
        this.companies = companies;
    }

    @Override
    public boolean equals(Object obj) {
        return obj instanceof MarketMetaDataResponse;
    }
}
