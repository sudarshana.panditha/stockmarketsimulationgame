package market.messages;

/*
* This message is used to notify the Market Actor
* to create a new Market for a Game
* */
public class CreateMarket {
    public final String marketId; // i.e., the gameId
    public final int maxNumOfTurns; // max number of turns in the Market

    public CreateMarket(String marketId, int maxNumOfTurns) {
        this.marketId = marketId;
        this.maxNumOfTurns = maxNumOfTurns;
    }

    @Override
    public boolean equals(Object obj) {
        if(this == obj) return true;
        if(obj == null) return false;
        if(getClass() != obj.getClass())
            return false;
        CreateMarket msg = (CreateMarket) obj;
        return this.marketId.equals(msg.marketId)
                && this.maxNumOfTurns == msg.maxNumOfTurns;
    }
}
