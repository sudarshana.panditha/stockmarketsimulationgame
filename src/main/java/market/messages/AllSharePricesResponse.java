package market.messages;

import java.io.Serializable;
import java.util.Map;

/*
* This message is used to send the client current
* share prices of each Company organized on Sectors
* */
public class AllSharePricesResponse implements Serializable{
    public final String marketId;
    // Read as Map<sectorId, Map<companyId, sharePrice>>
    public final Map<String, Map<String, Double>> sectorSharePricesMap;

    public AllSharePricesResponse(String marketId, Map<String, Map<String, Double>> sectorSharePricesMap) {
        this.marketId = marketId;
        this.sectorSharePricesMap = sectorSharePricesMap;
    }

    @Override
    public boolean equals(Object obj) {
        return obj instanceof AllSharePricesResponse;
    }
}
