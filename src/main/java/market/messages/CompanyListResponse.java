package market.messages;

import market.CompanyDetails;

import java.util.List;

/*
* This message is used to respond to a
* CompanyListRequest from the Broker
* actor
* */
public class CompanyListResponse {
    public final String marketId;
    public List<CompanyDetails> companyDetails;

    public CompanyListResponse(String marketId, List<CompanyDetails> companyDetails) {
        this.marketId = marketId;
        this.companyDetails = companyDetails;
    }

    @Override
    public boolean equals(Object obj) {
        if(this == obj) return true;
        if(obj == null) return false;
        if(getClass() != obj.getClass())
            return false;
        CompanyListResponse res = (CompanyListResponse) obj;
        return this.marketId.equals(res.marketId)
                && this.companyDetails.equals(res.companyDetails);
    }
}
