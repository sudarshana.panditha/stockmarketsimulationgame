package market.messages;
/*
 * This message is used to request the Market actor
 * the list of details of Companies of a Market. This
 * is used by the Broker actor when initializing
 * Investor Portfolios
 * */
public class CompanyListRequest {
    public final String marketId; // Market to which companies belong to

    public CompanyListRequest(String marketId) {
        this.marketId = marketId;
    }

    @Override
    public boolean equals(Object obj) {
        if(this == obj) return true;
        if(obj == null) return false;
        if(getClass() != obj.getClass())
            return false;
        CompanyListRequest req = (CompanyListRequest) obj;
        return this.marketId.equals(req.marketId);
    }
}
