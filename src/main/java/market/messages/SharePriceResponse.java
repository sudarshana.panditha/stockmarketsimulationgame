package market.messages;
/*
 * This message is used to respond to a received
 * SharePriceRequest by the Broker
 * */
public class SharePriceResponse {
    public final String requestId; // request id of SharePriceRequest
    public final double sharePrice; // share price of the requested company

    public SharePriceResponse(String requestId, double sharePrice) {
        this.requestId = requestId;
        this.sharePrice = sharePrice;
    }

    @Override
    public boolean equals(Object obj) {
        if(this == obj) return true;
        if(obj == null) return false;
        if(getClass() != obj.getClass())
            return false;
        SharePriceResponse res = (SharePriceResponse) obj;
        return this.requestId.equals(res.requestId)
                && this.sharePrice == res.sharePrice;
    }
}
