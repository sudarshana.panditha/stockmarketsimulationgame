package market;

import java.util.ArrayList;
import java.util.List;
import java.util.Random;
import java.util.UUID;

public class MarketModel {
    /* constants for initial share price range */
    private final double leftLimit = 5L;
    private final double rightLimit = 20L;

    private String sectorNames []; // pseudo sector names
    private String companyNames []; // pseudo company names

    private List<Sector> sectors; // sectors of the Market
    private List<Company> companies; // companies of the Market

    /* MarketModel constructor */
    public MarketModel(){
        initializePseudoData();
        initializeSectors();
        initializeCompanies();
    }

    /* Returns a randomly selected sector from sectors */
    public Sector getRandomSector(){
        Random random = new Random();
        return sectors.get(random.nextInt(sectors.size()));
    }

    /* Returns a randomly selected company from company */
    public Company getRandomCompany(){
        Random random = new Random();
        return companies.get(random.nextInt(companies.size()));
    }

    public List<Sector> getSectors() {
        return sectors;
    }

    public List<Company> getCompanies() {
        return companies;
    }

    /* Helper methods */

    /* initializes sectors */
    private void initializeSectors() {
        sectors = new ArrayList<>();
        for (String name: sectorNames){
            Sector sector = new Sector(UUID.randomUUID().toString(),name);
            sectors.add(sector);
        }
    }

    /* initializes companies */
    private void initializeCompanies(){
        companies = new ArrayList<>();
        int i = 0;
        int j = 0;
        for (Sector sector: sectors){
            for(j = i ; j < i+3; ++j){
                Company company = new Company(
                        UUID.randomUUID().toString(),
                        sector.sectorId,
                        companyNames[j],
                        getRandomSharePriceWithinRange(leftLimit, rightLimit)
                );
                companies.add(company);
            }
            i = j;
        }
    }

    private double getRandomSharePriceWithinRange(double leftLimit, double rightLimit) {
        return leftLimit + new Random().nextDouble() * (rightLimit - leftLimit);
    }

    /* Initializes pseudo data for sectors and companies */
    private void initializePseudoData() {
        sectorNames = new String[]{ // initialize sector names
                "Finance",
                "Technology",
                "Consumer Services",
                "Manufacturing"
        };

        companyNames = new String[] {
                "Golden Bank of Dwarves", // Finance
                "Bank of Blood Elves",
                "People's Bank of Azeroth",
                "Tinkertown Corp.", // Technology
                "Frosthold Technologies",
                "Aerie Peak Engineering",
                "OrcDash company", // Consumer services
                "ElvesFlare",
                "AzerothSpace Inc.",
                "Ironforge Company", // Manufacturing
                "Stormwind Manufacturing",
                "Dalaran Corp."
        };
    }
}
