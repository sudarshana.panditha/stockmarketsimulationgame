package market;

import market.exception.CompanyNotFoundException;
import market.exception.IllegalArgumentException;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/*
* This class creates, contains, and manages all the different
* Markets for each game and acts as a system boundary on the
* the operations over Markets
**/
public class MarketRepository {
    /* state of the repository */
    private Map<String, Market> markets; // set of Markets

    /* constructor */
    public MarketRepository() {
        markets = new HashMap<>();
    }

    /* interface */
    // This method is used by the Market Actor to create
    // a new Market in the MarketRepository
    public void createMarket(String marketId, int maxNumOfTurns) throws IllegalArgumentException {
        Market market = new Market(
                marketId,
                maxNumOfTurns,
                new MarketModel()
        );

        markets.put(marketId, market);
    }

    // This method is used to get the Market
    // for a given Market id
    public Market getMarket(String marketId){
        return markets.get(marketId);
    }

    // This method returns the share price of the given company
    // in the given Market
    public double getSharePrice(String marketId, String companyId) throws CompanyNotFoundException {
        Market market = getMarket(marketId);
        return market.getSharePrice(companyId);
    }

    // This method is used to set the current turn
    // for the given Market
    public void setCurrentTurn(String marketId, final int currentTurn){
        Market market = getMarket(marketId);
        market.setCurrentTurn(currentTurn);
    }

    // This method is used to get the necessary details
    // of the Companies of a Market
    public List<CompanyDetails> getCompanyDetailsList(String marketId){
        List<Company> companies = getMarket(marketId).getCompanies();
        List<CompanyDetails> companyDetails = new ArrayList<>();
        for (Company company: companies){
            companyDetails.add(
                    new CompanyDetails(company.sectorId,company.companyId, company.name)
            );
        }
        return companyDetails;
    }

    // This method is used to get the share prices of
    // each company organized into sectors
    public Map<String, Map<String, Double>> getAllSharePrices(String marketId){
        List<Company> companies = getMarket(marketId).getCompanies();
        List<Sector> sectors = getMarket(marketId).getSectors();
        Map<String, Map<String, Double>> allSharePrices = new HashMap<>();
        for (Sector sector: sectors){
            allSharePrices.put(sector.sectorId, new HashMap<>());
        }
        for(Company company: companies){
            Map<String, Double> sectorSharePrices = allSharePrices.get(company.sectorId);
            sectorSharePrices.put(company.companyId, new Double(company.getSharePrice()));
        }
        return allSharePrices;
    }

    // This method is used to get the event trend data
    // of a given market
    public Event [] getEventTrend(String marketId){
        return getMarket(marketId).getEventTrend();
    }

    // This method is used to get the market trend data
    // of a given market
    public int [] getMarketTrend(String marketId){
        return getMarket(marketId).getMarketTrend();
    }

    // This method is used to get the sector trends data
    // of a given market
    public Map<String, int []> getSectorTrends(String marketId){
        return getMarket(marketId).getSectorTrends();
    }

    // This method is used to get the random trends data
    // of a given market
    public Map<String, int []> getRandomTrends(String marketId){
        return getMarket(marketId).getRandomTrends();
    }

    // This method is used to get the sectors of a given
    // Market
    public List<Sector> getSectors(String marketId){
        return getMarket(marketId).getSectors();
    }

    // This method is used to get the CompanyDetails
    // organized into different sectors
    public Map<String, List<CompanyDetails>> getSectorCompanyDetails(String marketId){
        List<CompanyDetails> companies = getCompanyDetailsList(marketId);
        List<Sector> sectors = getSectors(marketId);
        Map<String, List<CompanyDetails>> sectorCompaniesMap = new HashMap<>();

        for(Sector sector: sectors){
            sectorCompaniesMap.put(sector.sectorId, new ArrayList<>());
        }

        for(CompanyDetails company: companies){
            List<CompanyDetails> companyDetails = sectorCompaniesMap
                    .get(company.sectorId);
            companyDetails.add(company);
        }

        return sectorCompaniesMap;
    }
}
