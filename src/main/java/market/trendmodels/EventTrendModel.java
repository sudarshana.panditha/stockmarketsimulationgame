package market.trendmodels;

import market.*;
import market.exception.DurationNotPositiveException;

/*
*  The EventTrendModel class represents the Event
*  component of the Value Change Algorithm
* */
public class EventTrendModel {
    /* probabilities of the Value Change Algorithm */
    // accumulative frequencies of event occurrence
    private int eventPrefix [] = {0, 100};
    private final int EVENT = 0;
    private final int NOT_EVENT = 1;
    private int eventOccurrence[] = {EVENT, NOT_EVENT};

    // sector and stock event probabilities as frequencies
    // sector and stock accumulative frequencies
    private int sectorStockPrefix [] = {33, 100}; // {SECTOR, SECTOR+STOCK}
    // constants for type of event
    private final int SECTOR = 0;
    private final int STOCK = 1;
    // set of events
    private int events [] = {SECTOR, STOCK};

    // sector event probabilities as frequencies
    // sector event accumulative frequencies
    private int sectorEventPrefix [] = {50, 100}; // {BOOM, BOOM+BUST}
    // sector event value ranges
    private MinMax sectorEventValues[] = {new MinMax(+1,+5), new MinMax(-5,-1)};
    private MinMax sectorEventDuration = new MinMax(2,5); // duration of a sector event
    // constants for type of event
    private final int BOOM = 0;
    private final int BUST = 1;
    // set of sector events
    private int sectorEvents [] = {BOOM, BUST};

    // stock event probabilities as frequencies
    // stock event accumulative frequencies
    private int stockEventPrefix [] = {50, 75, 100};
    // stock event value ranges
    private MinMax stockEventValues[] = {
            new MinMax(+2,+3), new MinMax(-5,-1), new MinMax(-6, -3)};
    private MinMax stockEventDuration = new MinMax(1,7); // duration of a stock event
    // constants for type of event
    private final int PROFIT_WARNING = 0;
    private final int TAKE_OVER = 1;
    private final int SCANDAL = 2;
    // set of stock events
    private int stockEvents [] = {PROFIT_WARNING, TAKE_OVER, SCANDAL};

    /* computing the event stream */
    private void computeEventStream(final int index){
        if(index < maxNumOfTurns){
            int event = Utilities.getRandomEvent(eventOccurrence, eventPrefix);
            if(event == EVENT){
                event = Utilities.getRandomEvent(events, sectorStockPrefix);
                if(event == SECTOR){
                    event = Utilities.getRandomEvent(sectorEvents, sectorEventPrefix);
                    if(event == BOOM){
                        Sector sector = marketModel.getRandomSector();
                        int duration = Utilities.getIntegerWithin(
                                sectorEventDuration.min,
                                sectorEventDuration.max
                        );
                        int value = Utilities.getIntegerWithin(
                                sectorEventValues[BOOM].min,
                                sectorEventValues[BOOM].max
                        );
                        try {
                            SectorEvent sectorEvent = new SectorEvent(
                                    sector.sectorId,
                                    value,
                                    duration
                            );
                            setEvents(sectorEvent, index, index + sectorEvent.duration);
                            setEventOccurrenceFrequencies();
                            computeEventStream(index + sectorEvent.duration + 1);
                        } catch (DurationNotPositiveException e) {
                            e.printStackTrace();
                        }
                    }else if(event == BUST){
                        Sector sector = marketModel.getRandomSector();
                        int duration = Utilities.getIntegerWithin(
                                sectorEventDuration.min,
                                sectorEventDuration.max
                        );
                        int value = Utilities.getIntegerWithin(
                                sectorEventValues[BUST].min,
                                sectorEventValues[BUST].max
                        );
                        try {
                            SectorEvent sectorEvent = new SectorEvent(
                                    sector.sectorId,
                                    value,
                                    duration
                            );
                            setEvents(sectorEvent, index, index + sectorEvent.duration);
                            setEventOccurrenceFrequencies();
                            computeEventStream(index + sectorEvent.duration + 1);
                        } catch (DurationNotPositiveException e) {
                            e.printStackTrace();
                        }
                    }
                }else if(event == STOCK){
                    event = Utilities.getRandomEvent(stockEvents, stockEventPrefix);
                    if(event == PROFIT_WARNING){
                        Company company = marketModel.getRandomCompany();
                        int duration = Utilities.getIntegerWithin(
                                stockEventDuration.min,
                                stockEventDuration.max
                        );
                        int value = Utilities.getIntegerWithin(
                                stockEventValues[PROFIT_WARNING].min,
                                stockEventValues[PROFIT_WARNING].max
                        );
                        try {
                            StockEvent stockEvent = new StockEvent(
                                    company.companyId,
                                    value,
                                    duration
                            );
                            setEvents(stockEvent, index, index + stockEvent.duration);
                            setEventOccurrenceFrequencies();
                            computeEventStream(index + stockEvent.duration + 1);
                        } catch (DurationNotPositiveException e) {
                            e.printStackTrace();
                        }

                    }else if(event == TAKE_OVER){
                        Company company = marketModel.getRandomCompany();
                        int duration = Utilities.getIntegerWithin(
                                stockEventDuration.min,
                                stockEventDuration.max
                        );
                        int value = Utilities.getIntegerWithin(
                                stockEventValues[TAKE_OVER].min,
                                stockEventValues[TAKE_OVER].max
                        );
                        try {
                            StockEvent stockEvent = new StockEvent(
                                    company.companyId,
                                    value,
                                    duration
                            );
                            setEvents(stockEvent, index, index + stockEvent.duration);
                            setEventOccurrenceFrequencies();
                            computeEventStream(index + stockEvent.duration + 1);
                        } catch (DurationNotPositiveException e) {
                            e.printStackTrace();
                        }

                    }else if(event == SCANDAL){
                        Company company = marketModel.getRandomCompany();
                        int duration = Utilities.getIntegerWithin(
                                stockEventDuration.min,
                                stockEventDuration.max
                        );
                        int value = Utilities.getIntegerWithin(
                                stockEventValues[SCANDAL].min,
                                stockEventValues[SCANDAL].max
                        );
                        try {
                            StockEvent stockEvent = new StockEvent(
                                    company.companyId,
                                    value,
                                    duration
                            );
                            setEvents(stockEvent, index, index + stockEvent.duration);
                            setEventOccurrenceFrequencies();
                            computeEventStream(index + stockEvent.duration + 1);
                        } catch (DurationNotPositiveException e) {
                            e.printStackTrace();
                        }
                    }
                }
            }else if(event == NOT_EVENT){
                eventStream[index] = null;
                setEventOccurrenceFrequencies();
                computeEventStream(index + 1);
            }
        }
    }

    /* helper functions */
    // This methods sets events in the event stream for
    // the given range
    private void setEvents(Event event, int start, int end) {
        if(end >= maxNumOfTurns){
            end = maxNumOfTurns - 1;
        }
        while(start <= end){
            eventStream[start] = event;
            start++;
        }
    }

    private void setEventOccurrenceFrequencies() {
        if(eventPrefix[0] == 100){
            eventPrefix[0] = 0;
        }else {
            eventPrefix[0] = eventPrefix[0] + 10;
        }
    }

    /* state and house keeping variables */
    private final MarketModel marketModel;
    private final int maxNumOfTurns;
    private Event eventStream [];

    /* constructor */
    public EventTrendModel(MarketModel marketModel, int maxNumOfTurns) {
        this.marketModel = marketModel;
        this.maxNumOfTurns = maxNumOfTurns;
        eventStream = new Event[this.maxNumOfTurns];
        computeEventStream(0);
    }

    /* interface */
    public Event getEvent(final int turn){
        return eventStream[turn];
    }
}
