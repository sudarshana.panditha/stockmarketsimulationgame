package market.trendmodels;

import market.Company;
import market.MarketModel;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

/*
 *  The RandomTrendModel class represents the
 *  Random Market component of the Value
 *  Change Algorithm
 * */
public class RandomTrendModel {
    /* probabilities as frequencies */
    private int valueChangesPrefix [] = {25,50,100}; // Accumulative frequencies
    public final int INCREASE = 0;
    public final int DECREASE = 1;
    public final int NO_CHANGE = 2;
    public int valueChangeEvents[] = {INCREASE, DECREASE, NO_CHANGE};

    /* constants for values */
    private final int PLUS_ONE = 1;
    private final int MINUS_ONE = -1;
    private final int ZERO = 0;

    /* state and house keeping variables */
    private final int maxNumOfTurns;
    private final MarketModel marketModel;
    private Map<String, int []> companyTrends;

    /* helper methods */
    /* This method computes the valueStream for a given valueStream */
    private void computeCompanyValueStream(int valueStream []){
        valueStream[0] = ZERO;
        for (int i = 1; i < maxNumOfTurns; ++i){
            if(valueStream[i] == NO_CHANGE){
                valueStream[i] = valueStream[i-1];
            }else if(valueStream[i] == INCREASE){
                valueStream[i] = valueStream[i-1] + PLUS_ONE;
            }else if(valueStream[i] == DECREASE){
                valueStream[i] = valueStream[i-1] + MINUS_ONE;
            }
        }
    }

    // method to recursively initialize the events of value stream
    private void initializeCompanyValueStreamEvents(int index, int valueStream []){
        if(index < maxNumOfTurns){
            int event = Utilities.getRandomEvent(valueChangeEvents, valueChangesPrefix);
            if(event == INCREASE){
                valueStream[index] = INCREASE;
                initializeCompanyValueStreamEvents(index+1, valueStream);
            }else if(event == DECREASE){
                valueStream[index] = DECREASE;
                initializeCompanyValueStreamEvents(index+1, valueStream);
            }else if(event == NO_CHANGE){
                valueStream[index] = NO_CHANGE;
                initializeCompanyValueStreamEvents(index+1, valueStream);
            }
        }
    }
    // This method filters the valueStream to maintain the value range
    // between -2 and +2
    private void filterCompanyValueStreamEvents(int valueStream[]){
        int increaseCount = 0;
        int decreaseCount = 0;

        for(int i = 0; i <maxNumOfTurns; i++){
            if(valueStream[i] == INCREASE){
                increaseCount++;
            }else if(valueStream[i] == DECREASE){
                decreaseCount++;
            }
            // Do not let the difference between number of INCREASE
            // and DECREASE be more than 1
            if(increaseCount-decreaseCount > 1){
                boolean isReplacementFound = false;
                for (int j = i; j < maxNumOfTurns; ++j){
                    if(valueStream[j] == DECREASE){
                        valueStream[i] = DECREASE;
                        valueStream[j] = INCREASE;
                        isReplacementFound = true;
                        break;
                    }
                }
                if(!isReplacementFound){
                    valueStream[i] = NO_CHANGE;
                }
                // needs to recount because event was swapped
                increaseCount--;
                i--;
            }else if(decreaseCount - increaseCount > 1){
                boolean isReplacementFound = false;
                for (int j = i; j < maxNumOfTurns; ++j){
                    if(valueStream[j] == INCREASE){
                        valueStream[i] = INCREASE;
                        valueStream[j] = DECREASE;
                        isReplacementFound = true;
                        break;
                    }
                }
                if(!isReplacementFound){
                    valueStream[i] = NO_CHANGE;
                }
                // needs to recount because event was swapped
                decreaseCount--;
                i--;
            }

        }

    }

    // initializes the sector trends
    private void initializeSectorTrends(){
        companyTrends = new HashMap<>();
        List<Company> companies = marketModel.getCompanies();
        companies.forEach(
                company -> {
                    companyTrends.put(company.companyId, new int[this.maxNumOfTurns]);
                }
        );
    }

    // computes the value stream for each company
    private void computeCompanyValueStreams(){
        for(String companyId:companyTrends.keySet()){
            int valueStream[] = companyTrends.get(companyId);
            initializeCompanyValueStreamEvents(0, valueStream);
            filterCompanyValueStreamEvents(valueStream);
            computeCompanyValueStream(valueStream);
        }
    }

    /* constructor */
    public RandomTrendModel(int maxNumOfTurns, MarketModel marketModel) {
        this.maxNumOfTurns = maxNumOfTurns;
        this.marketModel = marketModel;
        initializeSectorTrends();
        computeCompanyValueStreams();
    }

    /* interface */
    public int getValue(final String companyId, final int turn){
        int valueStream[] = companyTrends.get(companyId);
        return valueStream[turn];
    }
}
