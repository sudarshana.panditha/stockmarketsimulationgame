package market.trendmodels;

import java.util.Random;

public class Utilities {
    // Method to get random index given a probability distribution
    public static int getRandomEvent(int events[], int prefix []){
        int leftLimit = 1;
        int rightLimit = prefix[events.length-1];
        int random = leftLimit
                + (int) (new Random().nextFloat() * (rightLimit - leftLimit));
        int index = Utilities.findCeil(prefix, random, 0, events.length-1);
        return events[index];
    }

    // Method to find ceiling of a frequency in a accumulative
    // frequency distribution
    public static int findCeil(int prefix[], int random, int lowest, int highest){
        int middle;
        while(lowest < highest){
            middle = (lowest + highest) / 2;
            if(random > prefix[middle]){
                lowest = middle + 1;
            } else {
                highest = middle;
            }
        }
        if(prefix[lowest] >= random){
            return lowest;
        }else {
            return -1;
        }
    }

    // Method returns an integer in the given range
    public static int getIntegerWithin(int min, int max){
        return min + (int) (new Random().nextFloat() * (max - min));
    }
}
