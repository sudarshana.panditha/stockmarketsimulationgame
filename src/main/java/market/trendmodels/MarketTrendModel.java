package market.trendmodels;
/*
 *  The MarketTrendModel class represents the
 *  General Trend Market component of the Value
 *  Change Algorithm
 * */
public class MarketTrendModel {
    /* probabilities as frequencies */
    private int valueChangesPrefix [] = {50,100}; // {INCREASE, INCREASE+DECREASE}
    public final int INCREASE = 0;
    public final int DECREASE = 1;
    public int valueChangeEvents[] = {INCREASE, DECREASE};

    /* constants for values */
    private final int PLUS_ONE = 1;
    private final int MINUS_ONE = -1;
    private final int ZERO = 0;

    /* Helper functions */
    /* This method computes the valueStream for a given valueStream */
    private void computeValueStream(){
        valueStream[0] = ZERO;
        for (int i = 1; i < maxNumOfTurns; ++i){
            if(valueStream[i] == INCREASE){
                valueStream[i] = valueStream[i-1] + PLUS_ONE;
            }else if(valueStream[i] == DECREASE){
                valueStream[i] = valueStream[i-1] + MINUS_ONE;
            }
        }
    }

    // method to recursively initialize the events of value stream
    private void initializeValueStreamEvents(int index){
        if(index < maxNumOfTurns){
            int event = Utilities.getRandomEvent(valueChangeEvents, valueChangesPrefix);
            if(event == INCREASE){
                valueStream[index] = INCREASE;
                initializeValueStreamEvents(index+1);
            }else if(event == DECREASE){
                valueStream[index] = DECREASE;
                initializeValueStreamEvents(index+1);
            }
        }
    }
    // This method filters the valueStream to maintain the value range
    // between -3 and +3
    private void filterValueStreamEvents(){
        int increaseCount = 0;
        int decreaseCount = 0;

        for(int i = 0; i <maxNumOfTurns; i++){
            if(valueStream[i] == INCREASE){
                increaseCount++;
            }else if(valueStream[i] == DECREASE){
                decreaseCount++;
            }
            // Do not let the difference between number of INCREASE
            // and DECREASE be more than 2
            if(increaseCount-decreaseCount > 2){
                boolean isReplacementFound = false;
                for (int j = i; j < maxNumOfTurns; ++j){
                    if(valueStream[j] == DECREASE){
                        valueStream[i] = DECREASE;
                        valueStream[j] = INCREASE;
                        isReplacementFound = true;
                        break;
                    }
                }
                if(!isReplacementFound){
                    valueStream[i] = DECREASE;
                }
                // needs to recount because event was swapped
                increaseCount--;
                i--;
            }else if(decreaseCount - increaseCount > 2){
                boolean isReplacementFound = false;
                for (int j = i; j < maxNumOfTurns; ++j){
                    if(valueStream[j] == INCREASE){
                        valueStream[i] = INCREASE;
                        valueStream[j] = DECREASE;
                        isReplacementFound = true;
                        break;
                    }
                }
                if(!isReplacementFound){
                    valueStream[i] = INCREASE;
                }
                // needs to recount because event was swapped
                decreaseCount--;
                i--;
            }

        }

    }

    /* state and house keeping variables*/
    private final int maxNumOfTurns;
    private int valueStream [];

    /* interface */
    public int getValue(final int turn){
        return valueStream[turn];
    }

    /* constructor */
    public MarketTrendModel(int maxNumOfTurns) {
        this.maxNumOfTurns = maxNumOfTurns;
        valueStream = new int[this.maxNumOfTurns];
        initializeValueStreamEvents(0);
        filterValueStreamEvents();
        computeValueStream(); // compute the value stream
    }
}
