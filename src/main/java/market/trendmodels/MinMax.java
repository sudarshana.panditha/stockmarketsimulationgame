package market.trendmodels;
/*
* The MinMax class is used to represent pairs of
* values that have a minimum and maximum. For example,
* a minimum and a maximum duration of an Event,
* a minimum and maximum value of an Event, and etc.
* */
public class MinMax {
    public final int min;
    public final int max;

    public MinMax(int min, int max) {
        this.min = min;
        this.max = max;
    }
}
