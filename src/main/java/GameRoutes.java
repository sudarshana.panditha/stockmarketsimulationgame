
import java.time.Duration;
import java.util.concurrent.CompletableFuture;
import java.util.concurrent.CompletionStage;
import akka.Done;
import bank.messages.Ack;
import bank.messages.AllAccountsBalanceRequest;
import bank.messages.AllAccountsBalanceResponse;
import broker.messages.*;
import clock.messages.ClockMetaDataRequest;
import clock.messages.ClockMetaDataResponse;
import clock.messages.TurnOver;
import gamedestroyer.messages.ResultsRequest;
import gamedestroyer.messages.ResultsResponse;
import gamemaker.messages.GameRequest;
import gamemaker.messages.GameResponse;
import gamemaker.messages.Play;
import market.messages.*;
import playerstatus.Player;
import playerstatus.messages.*;
import akka.actor.ActorSystem;
import akka.actor.ActorRef;
import akka.event.Logging;
import akka.event.LoggingAdapter;
import akka.http.javadsl.marshallers.jackson.Jackson;
import akka.http.javadsl.model.StatusCodes;
import akka.http.javadsl.server.AllDirectives;
import akka.http.javadsl.server.Route;
import akka.pattern.Patterns;

public class GameRoutes extends AllDirectives{
    final private ActorRef playerStatusActor;
    final private ActorRef gameMakerActor;
    final private ActorRef brokerActor;
    final private ActorRef clockActor;
    final private ActorRef gameDestroyerActor;
    final private LoggingAdapter log;

    public static Player player;


    public GameRoutes (ActorSystem system, ActorRef playerStatusActor, ActorRef gameMakerActor, ActorRef brokerActor, ActorRef clockActor, ActorRef gameDestroyerActor) {
        this.playerStatusActor = playerStatusActor;
        this.brokerActor = brokerActor;
        this.clockActor = clockActor;
        this.gameMakerActor = gameMakerActor;
        this.gameDestroyerActor = gameDestroyerActor;
        log = Logging.getLogger(system, this);
    }


    public Route routes() {
        return concat(
                post(() ->
                        path("signin", () ->
                                        entity(Jackson.unmarshaller(Player.class), player -> {
                                            CompletionStage<Done> futureSaved = sendSignInRequest(playerStatusActor, player);
                                            return onSuccess(futureSaved, done ->
                                                    complete("player signed in")
                                            );
                                        })
                                )),

                post(() ->
                        path("live", () ->
                                entity(Jackson.unmarshaller(Player.class), player -> {
                                    CompletionStage<Done> futureSaved = sendLiveRequest(player);
                                    return onSuccess(futureSaved, done ->
                                            complete("order created")
                                    );
                                })))
        );
    }

    private CompletionStage<Done> sendLiveRequest(Player player) {
        playerStatusActor.tell(new Live(player.userId), ActorRef.noSender());
        return CompletableFuture.completedFuture(Done.getInstance());
    }

    private CompletionStage<Done> sendSignInRequest(ActorRef playerStatusActor, Player player) {
        playerStatusActor.tell(new SignInRequest(player.userName), ActorRef.noSender());
        return CompletableFuture.completedFuture(Done.getInstance());
    }


    /* all the game routes */
    private Route allGameRoutes(){
        return cookie("userId", userId ->
                cookie("gameId", gameId ->
                        cookie("username", username -> {
                            return concat(
                                    getBrokerRoutes(gameId.value(), userId.value()),
                                    getMarketRoutes(gameId.value()),
                                    getClockRoutes(gameId.value(), userId.value()),
                                    getGameMakerRoutes(gameId.value(),userId.value(),username.value()),
                                    getGameDestroyerRoutes(gameId.value())
                            );
                        })
                )
        );
    }

    /* All broker routes */
    private Route getBrokerRoutes(String marketId, String userId){
        return route(
                pathPrefix("broker", () ->
                        route(
                                pathPrefix("portfolios", ()-> allPortfoliosRequestRoute(marketId)),
                                pathPrefix("sell", () -> sellRoute(marketId,userId)),
                                pathPrefix("buy", () -> buyRoute(marketId, userId)),
                                pathPrefix("chit", () -> handOverChitRoute()),
                                pathPrefix("account", () -> allAccountBalancesRoute(marketId))
                        )
                )
        );
    }

    /* route for portfolios */
    private Route allPortfoliosRequestRoute(String marketId){
        return get(()-> {
            CompletionStage<AllPortFoliosResponse> response = Patterns
                    .ask(brokerActor, new AllPortfoliosRequest(marketId), Duration.ofMillis(3000))
                    .thenApply(AllPortFoliosResponse.class::cast);

            return onSuccess(() -> response,
                    msg -> {
                        return complete(StatusCodes.OK, msg, Jackson.marshaller());
                    }
            );
        });
    }

    /* route for sell */
    private Route sellRoute(String marketId, String userId){
        return post(() -> formField("companyId", companyId ->
                formField("shares", shares -> {
                    CompletionStage<Chit> chit = Patterns
                            .ask(brokerActor, new Sell(marketId, userId, companyId,Long.parseLong(shares)), Duration.ofMillis(3000))
                            .thenApply(Chit.class::cast);
                    return onSuccess(() -> chit, msg -> complete(StatusCodes.OK, msg, Jackson.marshaller()));
                })
        ));
    }
    /* route for buy */
    private Route buyRoute(String marketId, String userId){
        return post(() -> formField("companyId", companyId ->
                formField("shares", shares -> {
                    CompletionStage<Chit> chit = Patterns
                            .ask(brokerActor, new Buy(marketId, userId, companyId,Long.parseLong(shares)), Duration.ofMillis(3000))
                            .thenApply(Chit.class::cast);
                    return onSuccess(() -> chit, msg -> complete(StatusCodes.OK, msg, Jackson.marshaller()));
                })
        ));
    }
    /* route for hand over chit */
    private Route handOverChitRoute(){
        return post(() -> formField("requestId", requestId ->{
                    CompletionStage<ChitVerification> chitVerification = Patterns
                            .ask(brokerActor, new HandOverChit(requestId), Duration.ofMillis(3000))
                            .thenApply(ChitVerification.class::cast);
                    return onSuccess(() -> chitVerification, msg -> complete(StatusCodes.OK, msg, Jackson.marshaller()));
                })
        );
    }
    /* route for all account balances */
    private Route allAccountBalancesRoute(String marketId){
        return get(()-> {
            CompletionStage<AllAccountsBalanceResponse> response = Patterns
                    .ask(brokerActor, new AllAccountsBalanceRequest(marketId, null), Duration.ofMillis(3000))
                    .thenApply(AllAccountsBalanceResponse.class::cast);

            return onSuccess(() -> response,
                    msg -> {
                        return complete(StatusCodes.OK, msg, Jackson.marshaller());
                    }
            );
        });
    }

    /* market routes */
    private Route getMarketRoutes(String marketId){
        return route(
                pathPrefix("market", () ->
                        route(
                                pathPrefix("trends", ()-> marketTrendsRequestRoute(marketId)),
                                pathPrefix("metadata", () -> marketMetaDataRequestRoute(marketId)),
                                pathPrefix("shareprices", () -> allSharePricesRequestRoute(marketId))
                        )
                )
        );
    }

    private Route marketMetaDataRequestRoute(String marketId){
        return get(() -> {
            CompletionStage<MarketMetaDataResponse> response = Patterns
                    .ask(clockActor, new MarketMetaDataRequest(marketId), Duration.ofMillis(3000))
                    .thenApply(MarketMetaDataResponse.class::cast);
            return onSuccess(() -> response, msg -> complete(StatusCodes.OK, msg, Jackson.marshaller()));
        });
    }

    private Route marketTrendsRequestRoute(String marketId){
        return get(() -> {
            CompletionStage<AllTrendsResponse> response = Patterns
                    .ask(clockActor, new AllTrendsRequest(marketId), Duration.ofMillis(3000))
                    .thenApply(AllTrendsResponse.class::cast);
            return onSuccess(() -> response, msg -> complete(StatusCodes.OK, msg, Jackson.marshaller()));
        });
    }

    private Route allSharePricesRequestRoute(String marketId){
        return get(() -> {
            CompletionStage<AllSharePricesResponse> response = Patterns
                    .ask(clockActor, new AllSharePricesRequest(marketId), Duration.ofMillis(3000))
                    .thenApply(AllSharePricesResponse.class::cast);
            return onSuccess(() -> response, msg -> complete(StatusCodes.OK, msg, Jackson.marshaller()));
        });
    }

    /* clock routes */
    private Route getClockRoutes(String gameId, String userId){
        return route(
                pathPrefix("clock", () ->
                        route(
                                turnOverRoute(gameId, userId),
                                clockMetaDataRequestRoute(gameId)
                        )
                )
        );
    }
    /* clock meta data route */
    private Route clockMetaDataRequestRoute(String gameId){
        return get(() -> {
            CompletionStage<ClockMetaDataResponse> response = Patterns
                    .ask(clockActor, new ClockMetaDataRequest(gameId), Duration.ofMillis(3000))
                    .thenApply(ClockMetaDataResponse.class::cast);
            return onSuccess(() -> response, msg -> complete(StatusCodes.OK, msg, Jackson.marshaller()));
        });
    }
    /* turn over route */
    private Route turnOverRoute(String gameId, String userId){
        return post(() -> formField("turn", turn -> {
            CompletionStage<Ack> ack = Patterns
                    .ask(clockActor, new TurnOver(gameId, userId, Integer.parseInt(turn)), Duration.ofMillis(3000))
                    .thenApply(Ack.class::cast);
            return onSuccess(() -> ack, msg -> complete(StatusCodes.OK, msg, Jackson.marshaller()));
        }));
    }

    /* game maker routes */
    private Route getGameMakerRoutes(String gameId, String userId, String username){
        return route(
                pathPrefix("game", () ->
                        gameRoute(gameId)
                ),
                pathPrefix("play", () -> playRoute(userId,username))
        );
    }

    private Route gameRoute(String gameId){
        return get(() -> {
            CompletionStage<GameResponse> response = Patterns
                    .ask(gameMakerActor, new GameRequest(gameId), Duration.ofMillis(3000))
                    .thenApply(GameResponse.class::cast);
            return onSuccess(() -> response, msg -> complete(StatusCodes.OK, msg, Jackson.marshaller()));
        });
    }

    private Route playRoute(String userId, String username){
        return post(() -> {
            CompletionStage<gamemaker.messages.Ack> ack = Patterns
                    .ask(gameMakerActor, new Play(userId, username), Duration.ofMillis(3000))
                    .thenApply(gamemaker.messages.Ack.class::cast);
            return onSuccess(() -> ack, msg -> complete(StatusCodes.OK, msg, Jackson.marshaller()));
        });
    }

    /* game destroyer routes */
    private Route getGameDestroyerRoutes(String gameId){
        return get(() -> {
            CompletionStage<ResultsResponse> response = Patterns
                    .ask(gameDestroyerActor, new ResultsRequest(gameId), Duration.ofMillis(3000))
                    .thenApply(ResultsResponse.class::cast);
            return onSuccess(() -> response, msg -> complete(StatusCodes.OK, msg, Jackson.marshaller()));
        });
    }

}

