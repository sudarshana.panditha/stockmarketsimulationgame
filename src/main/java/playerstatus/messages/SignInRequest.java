package playerstatus.messages;

import java.io.Serializable;

public class SignInRequest implements Serializable {
    public String userName;

    public SignInRequest(String username) {
        userName = username;
    }

    public String getUserName() {
        return userName;
    }
}
