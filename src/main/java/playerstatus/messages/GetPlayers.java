package playerstatus.messages;

import com.fasterxml.jackson.annotation.JsonCreator;
import com.fasterxml.jackson.annotation.JsonProperty;

import java.util.List;

public class GetPlayers {
    public List<String> p;

    @JsonCreator
    public GetPlayers(@JsonProperty("strings") List<String>p2) {
        p = p2;
    }

    public List<String> getUserName() {
        return p;
    }
}
