package playerstatus.messages;
import com.fasterxml.jackson.annotation.JsonCreator;
import com.fasterxml.jackson.annotation.JsonProperty;
import playerstatus.Player;

import java.util.List;
import java.util.Map;

public class OnlinePlayers {
    //public List<String> players;
    public Map<String, Player> playerSet;

    public OnlinePlayers(@JsonProperty("playerset") Map<String, Player> players) {
        this.playerSet = players;
    }

    public Map<String, Player> getPlayers() {
        return playerSet;
    }
}
