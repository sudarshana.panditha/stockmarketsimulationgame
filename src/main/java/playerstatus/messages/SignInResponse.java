package playerstatus.messages;

import java.io.Serializable;

public class SignInResponse implements Serializable {
    public String userName;
    public String userId;

    public String getUserName() {
        return userName;
    }

    public String getUserId() {
        return userId;
    }

}
