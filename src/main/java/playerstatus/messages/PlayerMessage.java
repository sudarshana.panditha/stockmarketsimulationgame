package playerstatus.messages;

import playerstatus.Player;

public class PlayerMessage {
    public Player player;


    public PlayerMessage(Player player) {
        this.player = player;
    }

    public Player getPlayer() {
        return player;
    }
}

