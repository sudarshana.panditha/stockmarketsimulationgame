package playerstatus;

import  akka.actor.Props;
import akka.actor.AbstractActor;
import akka.actor.ActorRef;
import  akka.event.Logging;
import akka.event.LoggingAdapter;
import java.util.*;


public class GameMakerActor extends AbstractActor {

    private ActorRef playerStatusActor;
    private String userName;

    public static Props props (ActorRef playerStatusActor){
        return Props.create(GameMakerActor.class, () -> new GameMakerActor(playerStatusActor));
    }

    public GameMakerActor (ActorRef playerStatusActor){
        this.playerStatusActor = playerStatusActor;
    }

    //method for user ID generation
    private String generateId() {
        return "";
    }

    @Override
    public Receive createReceive() {
        return receiveBuilder()

                .match(Playing.class, x -> {
                    playerStatusActor.tell(new playerstatus.messages.Playing(generateId()), getSelf());
                })
                .build();
    }

}
