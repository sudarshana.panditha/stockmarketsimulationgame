package playerstatus;

import akka.actor.AbstractActor;
import akka.actor.ActorRef;
import akka.actor.Props;

import java.util.HashMap;
import java.util.Map;

import com.fasterxml.jackson.annotation.JsonCreator;
import playerstatus.exceptions.InvalidStatusException;

public class PlayerStatusRepository {

    private final long timeDifference;
    private Player player;
    private Map<String, Player> players;

    public PlayerStatusRepository (long timeDifference){
        this.timeDifference = timeDifference;
        players = new HashMap<>();
    }

    public void removeOfflinePlayers(){

    }

    public void updateLiveStatus(String userId) {

    }

    public void updateLivePlayingStatus(String userId) {

    }

    public void addPlayer(String userName, String userId) throws InvalidStatusException {
        player =  new Player(userId, userName);
        player.setStatus(player.LIVE);
        players.put(player.LIVE, player);
    }

    public Map<String , Player> getPlayers() {
        return players;
    }

}
