package playerstatus;

import akka.actor.AbstractActor;
import akka.actor.ActorRef;
import akka.actor.Props;
import com.fasterxml.jackson.annotation.JsonCreator;
import com.fasterxml.jackson.annotation.JsonProperty;
import playerstatus.messages.GetPlayers;
import playerstatus.messages.OnlinePlayers;
import playerstatus.messages.SignInRequest;

import java.security.SecureRandom;
import java.util.List;
import java.util.Map;

public class PlayerStatusActor extends AbstractActor {

    private String userId;
    private String userName = "";
    static final String charSet = "0123456789ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz"; //character set for the generation of a random user ID.
    static SecureRandom random = new SecureRandom();

    private PlayerStatusRepository playerStatusRepository;
    //public List<String> strings;

    static public Props props(PlayerStatusRepository playerStatusRepository) {
        return Props.create(PlayerStatusActor.class, () -> new PlayerStatusActor(playerStatusRepository));
    }

    public PlayerStatusActor(PlayerStatusRepository playerStatusRepository){
        this.playerStatusRepository = playerStatusRepository;
    }

    //method for user ID generation
    private String generateId(int length) {
        StringBuilder sb = new StringBuilder(length);
        for(int i = 0; i < length; i++ )
            sb.append(charSet.charAt(random.nextInt(charSet.length())));
        return sb.toString();
    }

    @Override
    public Receive createReceive() {
        return receiveBuilder()
                .match(SignInRequest.class, signInRequest -> {
                    this.userName = signInRequest.getUserName();
                    playerStatusRepository.addPlayer(userName, generateId(8));

                })

                .match(GetPlayers.class, playerList -> {
                    sender().tell(new OnlinePlayers(playerStatusRepository.getPlayers()), self());

                })

                .match(playerstatus.messages.Playing.class, playing -> {
                    this.userId = playing.userId;
                })
                .build();
    }

}
