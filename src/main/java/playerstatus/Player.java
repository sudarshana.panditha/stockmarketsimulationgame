package playerstatus;

import playerstatus.exceptions.InvalidStatusException;
import com.fasterxml.jackson.annotation.JsonCreator;
import com.fasterxml.jackson.annotation.JsonProperty;

import java.time.LocalTime;
import java.time.temporal.ChronoField;

public class Player {
    public final String userName;
    public final String userId;
    public static final String LIVE = "LIVE";
    public static final String PLAYING = "PLAYING";
    private String status;
    private long timeStamp;

    @JsonCreator
    public Player(@JsonProperty("userId") String userId, @JsonProperty("userName") String userName){
        this.userId = userId;
        this.userName = userName;
        timeStamp = LocalTime.now().getLong(ChronoField.SECOND_OF_DAY);
    }

    public void setStatus (String status) throws InvalidStatusException {
        this.status = status;
    }

    public long getTimeStamp() {
        return timeStamp;
    }

    public String getStatus() {
        return status;
    }

}
