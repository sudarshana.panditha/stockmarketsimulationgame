package clock;

import clock.exception.ExpiredTurnException;
import clock.exception.InvalidMaxNumOfTurnsException;
import clock.exception.InvalidTurnException;

import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.concurrent.atomic.AtomicBoolean;

public class GameClock {
    /* state and housekeeping variables */
    public final String gameId;
    private final int maxNumOfTurns;

    private Map<String, Boolean> currentTurnStatus;
    private int currentTurn;
    private boolean isTurnCompleted;

    public GameClock(String gameId, List<String> playerIds, int maxNumOfTurns) throws InvalidMaxNumOfTurnsException {
        this.gameId = gameId; // set game id of the clock
        this.maxNumOfTurns = maxNumOfTurns; // set maximum number of turns for game
        this.currentTurn = 1; // set initial current turn

        if(this.maxNumOfTurns <= 0){
            throw new InvalidMaxNumOfTurnsException();
        }

        // set player turn status for current turn
        currentTurnStatus = new HashMap<>();
        for(String playerId: playerIds){
            currentTurnStatus.put(playerId, Boolean.FALSE);
        }
    }

    public boolean isGameClockExpired(){
        return (currentTurn == maxNumOfTurns) && haveAllPlayersCompleted();
    }

    public int getCurrentTurn(){
        return currentTurn;
    }

    public void turnCompleted(String playerId, int turn) throws ExpiredTurnException, InvalidTurnException {
        if (turn < currentTurn){
            throw new ExpiredTurnException();
        }else if(turn > currentTurn){
            throw new InvalidTurnException();
        }
        if(isTurnCompleted)
            isTurnCompleted = false;
        currentTurnStatus.put(playerId, Boolean.TRUE);
        if(haveAllPlayersCompleted()){
            if(currentTurn != maxNumOfTurns){
                System.out.println("");
                resetCurrentTurnStatus();
                currentTurn++;
            }
            isTurnCompleted = true;
        }
    }
    // getter to maxNumOfTurns
    public int getMaxNumOfTurns() {
        return maxNumOfTurns;
    }

    /*
     * This method is used to get whether the turn of the clock
     * is completed or not
     * */
    public boolean isTurnCompleted() {
        return isTurnCompleted;
    }

    private void resetCurrentTurnStatus() {
        currentTurnStatus.replaceAll((playerId, turnStatus) -> Boolean.FALSE);
    }

    private boolean haveAllPlayersCompleted(){
        AtomicBoolean haveCompleted = new AtomicBoolean(true);
        currentTurnStatus.forEach((playerId, isTurnCompleted) -> {
            if(!isTurnCompleted.booleanValue()){
                haveCompleted.set(false);
            }
        });
        return haveCompleted.get();
    }
}
