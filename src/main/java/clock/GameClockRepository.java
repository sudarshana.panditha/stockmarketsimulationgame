package clock;

import clock.exception.GameClockNotFoundException;
import clock.exception.InvalidMaxNumOfTurnsException;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class GameClockRepository {
    private Map<String, GameClock> gameClocks; // Map of Game Clocks

    public GameClockRepository(){
        gameClocks = new HashMap<>();
    }

    public void createGameClock(String gameId, List<String> playerIds, final int maxNumOfTurns) throws InvalidMaxNumOfTurnsException {
        GameClock gameClock = new GameClock(gameId, playerIds, maxNumOfTurns);
        gameClocks.put(gameClock.gameId, gameClock);
    }

    public GameClock getGameClock(String gameId) throws GameClockNotFoundException {
        GameClock gameClock = gameClocks.get(gameId);
        if(gameClock == null){
            throw new GameClockNotFoundException();
        }
        return gameClock;
    }
}
