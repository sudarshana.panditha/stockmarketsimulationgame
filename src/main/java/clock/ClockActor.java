package clock;

import akka.actor.AbstractActor;
import akka.actor.ActorRef;
import akka.actor.Props;
import clock.exception.ExpiredTurnException;
import clock.exception.InvalidTurnException;
import clock.messages.*;
import market.messages.CurrentMarketTurn;

public class ClockActor extends AbstractActor{
    /* constant variables */
    public final static int TURN_INTERVAL = 30; // 30 seconds time interval

    /* state and housekeeping variables */
    private ActorRef marketActor;
    public final GameClockRepository gameClockRepository;

    /* constructors */
    public ClockActor(GameClockRepository gameClockRepository, ActorRef marketActor) {
        this.gameClockRepository = gameClockRepository;
        this.marketActor = marketActor;
    }

    public ClockActor(ActorRef marketActor){
        this.gameClockRepository = new GameClockRepository();
        this.marketActor = marketActor;
    }

    @Override
    public Receive createReceive() {
        return receiveBuilder()
                .match(
                        CreateGameClock.class, // Create new Game Clock
                        msg -> {
                            gameClockRepository.createGameClock(
                                    msg.gameId,
                                    msg.playerIds,
                                    msg.maxNumOfTurns
                            );
                        }
                )
                .match(
                        TurnOver.class,
                        msg -> {
                            GameClock gameClock = gameClockRepository.getGameClock(msg.gameId);
                            try {
                                gameClock.turnCompleted(msg.playerId, msg.turn);
                                if(gameClock.isTurnCompleted()){
                                    marketActor.tell(
                                            new CurrentMarketTurn(gameClock.gameId,gameClock.getCurrentTurn()), getSelf()
                                    );
                                }
                                if(gameClock.isGameClockExpired()){
                                    sender().tell(new Ack(Ack.GAME_COMPLETE), getSelf());
                                }else {
                                    sender().tell(new Ack(Ack.WAIT, gameClock.getCurrentTurn()), getSelf());
                                }
                            } catch (ExpiredTurnException e){
                                sender().tell(new Ack(Ack.TURN_COMPLETE, gameClock.getCurrentTurn()), getSelf());
                            } catch (InvalidTurnException e){
                                sender().tell(new Ack(Ack.ERROR, gameClock.getCurrentTurn()), getSelf());
                            }
                        }
                )
                .match( // message handle for received ClockMetaDataRequest message
                        ClockMetaDataRequest.class,
                        msg -> {
                            GameClock gameClock = gameClockRepository.getGameClock(msg.gameId);
                            // response message
                            ClockMetaDataResponse response = new ClockMetaDataResponse(
                                    TURN_INTERVAL, gameClock.getMaxNumOfTurns(),gameClock.getCurrentTurn()
                            );
                            // sending response
                            getSender().tell(response, getSelf());
                        }
                )
                .build();
    }

    public static Props props(GameClockRepository gameClockRepository, ActorRef marketActor) {
        return Props.create(ClockActor.class, () -> new ClockActor(gameClockRepository, marketActor));
    }
}
