package clock.messages;

public class TurnOver {
    public final String gameId;
    public final String playerId;
    public final int turn;

    public TurnOver(String gameId, String playerId, int turn) {
        this.gameId = gameId;
        this.playerId = playerId;
        this.turn = turn;
    }
}
