package clock.messages;

import java.io.Serializable;

/*
* This is a message class that is used to request
* meta data about the GameClock so that the client
* can instantiate variables such as the time interval
* of a turn, the number of turns in a game, and etch.
* */
public class ClockMetaDataRequest{
    public final String gameId; // requesting game clock's meta data

    /* constructor */
    public ClockMetaDataRequest(String gameId) {
        this.gameId = gameId;
    }
}
