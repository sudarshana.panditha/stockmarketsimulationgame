package clock.messages;

import java.io.Serializable;

/*
 * This is a message class that is used to send
 * meta data about the GameClock when a ClockMetaDataRequest
 * is received so that the client can instantiate
 * variables such as the time interval of a turn,
 * the number of turns in a game, and etch.
 * */
public class ClockMetaDataResponse implements Serializable{
    public final int turnInterval; // the duration of a turn
    public final int maxNumOfTurns; // number of turns in the game
    public final int currentTurn; // the current turn of the game

    /* constructor */
    public ClockMetaDataResponse(int turnInterval, int maxNumOfTurns, int currentTurn) {
        this.turnInterval = turnInterval;
        this.maxNumOfTurns = maxNumOfTurns;
        this.currentTurn = currentTurn;
    }

    @Override
    public boolean equals(Object obj) {
        if(this == obj) return true;
        if(obj == null) return false;
        if(getClass() != obj.getClass())
            return false;
        ClockMetaDataResponse res = (ClockMetaDataResponse) obj;
        return this.turnInterval == res.turnInterval
                && this.maxNumOfTurns == res.maxNumOfTurns
                && this.currentTurn == res.currentTurn;
    }
}
