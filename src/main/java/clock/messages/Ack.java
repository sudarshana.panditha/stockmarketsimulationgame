package clock.messages;

import java.io.Serializable;

public class Ack implements Serializable{
    /* String constants for status */
    public static final String TURN_COMPLETE = "TURN_COMPLETE";
    public static final String ERROR = "ERROR";
    public static final String GAME_COMPLETE = "GAME_COMPLETE";
    public static final String WAIT = "WAIT";

    public final String status; // Ack status
    public final int currentTurn; // current turn of the game

    public Ack(String status, int currentTurn) {
        this.status = status;
        this.currentTurn = currentTurn;
    }

    public Ack(String status) {
        this(status,0);
    }

    @Override
    public boolean equals(Object obj) {
        if(this == obj) return true;
        if(obj == null) return false;
        if(getClass() != obj.getClass())
            return false;
        Ack ack = (Ack) obj;
        return this.status.equals(ack.status)
                && this.currentTurn == ack.currentTurn;
    }
}
