package clock.messages;

import java.util.List;

public class CreateGameClock {
    public final String gameId; // game id of the clock
    public List<String> playerIds; // list of players of the game clock
    public final int maxNumOfTurns; // max number of turns of the game

    public CreateGameClock(String gameId, List<String> playerIds, int maxNumOfTurns) {
        this.gameId = gameId;
        this.playerIds = playerIds;
        this.maxNumOfTurns = maxNumOfTurns;
    }

    @Override
    public boolean equals(Object obj) {
        if(this == obj) return true;
        if(obj == null) return false;
        if(getClass() != obj.getClass())
            return false;
        CreateGameClock msg = (CreateGameClock) obj;

        return this.gameId.equals(msg.gameId);
    }
}
