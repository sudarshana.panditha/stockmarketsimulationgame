package gamedestroyer;
/*
* This class contains the result information
* of a Single player
* */
public class GameResultValue {
    public final String name;
    public final double sharesProfit;
    public final double accountBalance;
    public final double totalProfit;

    public GameResultValue(String name, double sharesProfit, double accountBalance) {
        this.name = name;
        this.sharesProfit = sharesProfit;
        this.accountBalance = accountBalance;
        totalProfit = this.sharesProfit + this.accountBalance;
    }
}
