package gamedestroyer;

import gamedestroyer.exception.GameResultNotFoundException;

import java.util.HashMap;
import java.util.Map;

/*
* This class contains the GameResults of each
* completed Games
* */
public class GameResultRepository {
    /* state and housekeeping variables */
    private Map<String, GameResult> gameresults;

    /* constructor */

    public GameResultRepository() {
        gameresults = new HashMap<>();
    }
    /* interface */

    /*
    * This method is used to create a new GameResult
    * object and return it
    * */
    public GameResult createGameResult(String gameId){
        GameResult gameResult = new GameResult(gameId);
        gameresults.put(gameResult.gameId, gameResult);
        return gameResult;
    }

    /*
    * This method is used to get the GameResult
    * of the given Game
    * */
    public GameResult getGameResult(String gameId) throws GameResultNotFoundException {
        GameResult gameResult = null;
        if(gameresults.containsKey(gameId))
            gameResult = gameresults.get(gameId);
        if(gameResult == null)
            throw new GameResultNotFoundException();
        return gameResult;
    }
}
