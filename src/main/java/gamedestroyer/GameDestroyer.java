package gamedestroyer;

import akka.actor.AbstractActor;
import akka.actor.ActorRef;
import akka.actor.Props;
import bank.messages.AllAccountsBalanceRequest;
import bank.messages.AllAccountsBalanceResponse;
import broker.messages.AllPortFoliosResponse;
import broker.messages.AllPortfoliosRequest;
import gamedestroyer.exception.GameResultsNotCalculatedException;
import gamedestroyer.messages.GameCompleted;
import gamedestroyer.messages.ResultsRequest;
import gamedestroyer.messages.ResultsResponse;
import market.messages.AllSharePricesRequest;
import market.messages.AllSharePricesResponse;

import java.util.ArrayList;
import java.util.List;

/*
 * This is an Akka actor that responds to ResultsRequest
 * messages from players by calculating results and sending
 * them in ResultsResponse messages. Also, this actor is
 * responsible for notifying other actors of ended Games
 * so that they can clean up data related to a Game
 * */
public class GameDestroyer extends AbstractActor{
    /* state and housekeeping variables */
    private GameResultRepository gameResultRepository;
    private ActorRef brokerActor;
    private ActorRef marketActor;
    private ActorRef bankActor;

    /* constructor */

    public GameDestroyer(GameResultRepository gameResultRepository, ActorRef brokerActor,
                         ActorRef marketActor, ActorRef bankActor) {
        this.gameResultRepository = gameResultRepository;
        this.brokerActor = brokerActor;
        this.marketActor = marketActor;
        this.bankActor = bankActor;
    }

    public GameDestroyer(ActorRef brokerActor, ActorRef marketActor, ActorRef bankActor) {
        this.gameResultRepository = new GameResultRepository();
        this.brokerActor = brokerActor;
        this.marketActor = marketActor;
        this.bankActor = bankActor;
    }

    @Override
    public Receive createReceive() {
        return receiveBuilder()
                .match(// message handle for receiving GameCompleted message
                        GameCompleted.class,
                        msg -> {
                            // create a new GameResult
                            GameResult gameResult = gameResultRepository.createGameResult(
                                    msg.gameId
                            );

                            // message for BrokerActor
                            AllPortfoliosRequest portfoliosRequest = new AllPortfoliosRequest(
                                    gameResult.gameId
                            );
                            // send message to broker
                            brokerActor.tell(portfoliosRequest, getSelf());

                            // message for MarketActor
                            AllSharePricesRequest sharePricesRequest = new AllSharePricesRequest(
                                    gameResult.gameId
                            );
                            // send message to market
                            marketActor.tell(sharePricesRequest, getSelf());
                        }
                )
                .match(// message handle for  receiving AllPortfoliosResponse
                        AllPortFoliosResponse.class,
                        msg -> {
                            // set portfolios inGameResult
                            GameResult gameResult = gameResultRepository.getGameResult(msg.marketId);
                            gameResult.setPortfolios(msg.portfolios);

                            // account ids to get bank account details
                            List<String> accountIds = new ArrayList<>();
                            msg.portfolios.forEach(
                                    (userId, portfolio) -> {
                                        accountIds.add(portfolio.getAccountId());
                                    }
                            );

                            // message to bank
                            AllAccountsBalanceRequest request = new AllAccountsBalanceRequest(msg.marketId, accountIds);
                            // sending message to bank
                            bankActor.tell(request, getSelf());
                        }
                )
                .match(// message handle for receiving AllSharePricesResponse
                        AllSharePricesResponse.class,
                        msg -> {
                            // set share prices in GameResult object
                            GameResult gameResult = gameResultRepository.getGameResult(msg.marketId);
                            gameResult.setSectorSharePrices(msg.sectorSharePricesMap);
                        }
                )
                .match(// message handle for AllAccountsBalanceResponse
                        AllAccountsBalanceResponse.class,
                        msg -> {
                            // set user account balances
                            GameResult gameResult = gameResultRepository.getGameResult(msg.referenceId);
                            gameResult.setUserAccountBalances(msg.userAccountBalances);
                        }
                )
                .match( // message handle for receiving ResultsRequest
                        ResultsRequest.class,
                        msg -> {
                            GameResult gameResult = gameResultRepository.getGameResult(msg.gameId);
                            ResultsResponse response = null;
                            try{
                                String winner = gameResult.getWinner();
                                List<GameResultValue> resultValues = gameResult.getGameResults();
                                // sending message
                                response = new ResultsResponse(
                                        ResultsResponse.CALCULATED,
                                        winner,
                                        resultValues
                                );
                            }catch (GameResultsNotCalculatedException e){
                                // sending message
                                response = new ResultsResponse(
                                        ResultsResponse.WAIT,
                                        null,
                                        null
                                );
                            }
                            // send message
                            getSender().tell(response, getSelf());
                        }
                )
                .build();
    }

    public static Props props(ActorRef brokerActor, ActorRef marketActor, ActorRef bankActor) {
        return Props.create(GameDestroyer.class, () -> new GameDestroyer(brokerActor, marketActor, bankActor));
    }
}
