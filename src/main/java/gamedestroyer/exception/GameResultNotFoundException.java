package gamedestroyer.exception;
/*
* This class is used by the GameResultRepository
* when calls getGameResult method can not find
* a GameResult for the given Game
* */
public class GameResultNotFoundException extends Exception {
}
