package gamedestroyer.exception;
/*
* This class is used to throw an Exception
* when the getWinner or getGameResults is called
* on a GameResult that has not yet calculated
* results to completion
* */
public class GameResultsNotCalculatedException extends Exception {
}
