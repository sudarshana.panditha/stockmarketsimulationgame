package gamedestroyer;

import broker.Portfolio;
import broker.exception.CompanyNotFoundException;
import gamedestroyer.exception.GameResultsNotCalculatedException;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.concurrent.atomic.AtomicReference;
import java.util.stream.Collectors;

/*
* This class is used to store different data needed
* for creating the Game's result and produce a
* data structure that contains all those data
* for the client
* */
public class GameResult {
    /* state and housekeeping data */
    public final String gameId;
    private List<Portfolio> portfolios; // <userId, Portfolio>
    private Map<String, Double> companySharePrices; // Map<companyId, sharePrice>
    private Map<String, Double> userAccountBalances; // <userId, account balances>
    private String winner;
    private List<GameResultValue> gameResultValues;
    private boolean isGameResultCalculated;

    /* constructor */
    public GameResult(String gameId) {
        this.gameId = gameId;
        this.portfolios = null;
        this.companySharePrices = null;
        this.userAccountBalances = null;
        this.winner = null;
        this.gameResultValues = null;
        this.isGameResultCalculated = false;
    }

    /* interface*/

    /*
    * This method is used to get the name of the Winner of this Game
    * */
    public String getWinner() throws GameResultsNotCalculatedException {
        if(!isGameResultCalculated)
            computeGameResults();
        if(winner == null)
            throw new GameResultsNotCalculatedException();
        return winner;
    }

    public List<GameResultValue> getGameResults() throws GameResultsNotCalculatedException {
        if(!isGameResultCalculated)
            computeGameResults();
        if(gameResultValues == null)
            throw new GameResultsNotCalculatedException();
        return gameResultValues;
    }

    /* getters and setters */

    public void setPortfolios(Map<String, Portfolio> portfolios) {
        this.portfolios = portfolios.values().stream().collect(Collectors.toList());
    }

    public void setSectorSharePrices(Map<String, Map<String, Double>> sectorSharePrices) {
        List<Map<String, Double>> companySharePricesList = sectorSharePrices.values().stream().collect(Collectors.toList());
        Map<String, Double> companySharePrices = new HashMap<>();
        companySharePricesList.forEach(companySharePrice -> {
            companySharePrice.forEach((companyId, sharePrice) -> companySharePrices.put(companyId, sharePrice));
        });
        this.companySharePrices = companySharePrices;
    }

    public void setUserAccountBalances(Map<String, Double> userAccountBalances) {
        this.userAccountBalances = userAccountBalances;
    }

    public boolean isSharePricesReceived() {
        return companySharePrices!= null;
    }


    public boolean isPortfoliosReceived() {
        return portfolios != null;
    }


    public boolean isAccountBalancesReceived() {
        return userAccountBalances != null;
    }

    /* helper methods */
    /*
     * This helper is used to calculate Game Result and determine the
     * winner if all the needed data are set
     * */
    public void computeGameResults(){
        boolean areAllDataReceived = isPortfoliosReceived()
                && isSharePricesReceived()
                && isAccountBalancesReceived();

        if(areAllDataReceived){
            computeGameResultValues();
            findWinner();
            isGameResultCalculated = true;
        }
    }

    /*
    * This helper method is used to determine the winner of the
    * Game
    * */
    private void findWinner(){
        String winner = "";
        if(gameResultValues.size() > 0){
            winner = gameResultValues.get(0).name;
            for(int i = 1; i < gameResultValues.size() ; ++i){
                double previousPlayerProfit = gameResultValues.get(i-1).totalProfit;
                double currentPlayerProfit = gameResultValues.get(i).totalProfit;
                if(currentPlayerProfit > previousPlayerProfit){
                    winner = gameResultValues.get(i).name;
                }
            }
        }

        this.winner = winner;
    }

    /*
    * This helper method is used to calculate the GameResultValue
    * of all the players
    * */
    private void computeGameResultValues(){
        List<GameResultValue> resultValues = new ArrayList<>();

        for(Portfolio portfolio: portfolios){
            resultValues.add(computePlayerGameResultValue(portfolio));
        }

        this.gameResultValues = resultValues;
    }

    /*
    * This helper method is used to calculate the GameResultValue
    * of a Single player
    * */
    private GameResultValue computePlayerGameResultValue( Portfolio portfolio){;

        // get profits from shares
        AtomicReference<Double> sharesProfit = new AtomicReference<>((double) 0);
        companySharePrices.forEach(
                (companyId, sharePrice) -> {
                    try {
                        long numberOfShares = portfolio.getNumberOfShares(companyId);
                        sharesProfit.updateAndGet(v -> new Double((double) (v + numberOfShares * sharePrice)));
                    } catch (CompanyNotFoundException e) {
                        e.printStackTrace();
                    }
                }
        );
        // get account balance
        double accountBalance = userAccountBalances.get(portfolio.getAccountId());

        return new GameResultValue(portfolio.name,sharesProfit.get(),accountBalance);
    }

}
