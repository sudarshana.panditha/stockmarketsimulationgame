package gamedestroyer.messages;
/*
* This message class is used to request the results of
* of a completed Game
* */
public class ResultsRequest {
    public final String gameId; // of the Game for which the results requested

    /* constructor */
    public ResultsRequest(String gameId) {
        this.gameId = gameId;
    }
}
