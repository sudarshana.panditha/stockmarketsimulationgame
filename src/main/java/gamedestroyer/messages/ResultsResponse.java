package gamedestroyer.messages;

import gamedestroyer.GameResultValue;

import java.io.Serializable;
import java.util.List;
/*
* This class is used to respond to a ResultRequest
* received by the GameDestroyer Actor
* */
public class ResultsResponse implements Serializable{
    /* constants */
    public static final String CALCULATED = "CALCULATED";
    public static final String WAIT = "WAIT";

    /* message state */
    public final String state;
    public final String winner;
    public List<GameResultValue> results;

    /* constructor */
    public ResultsResponse(String state, String winner, List<GameResultValue> results) {
        this.state = state;
        this.winner = winner;
        this.results = results;
    }

    @Override
    public boolean equals(Object obj) {
        if(this == obj) return true;
        if(obj == null) return false;
        if(getClass() != obj.getClass())
            return false;
        ResultsResponse res = (ResultsResponse) obj;
        return this.state.equals(res.state);
    }
}
