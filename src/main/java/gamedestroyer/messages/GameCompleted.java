package gamedestroyer.messages;
/*
* This class is used to notify the GameDestroyer Actor
* that the given Game has ended all of its turns
* */
public class GameCompleted {
    public final String gameId; // game id of the completed game

    /* constructor */
    public GameCompleted(String gameId) {
        this.gameId = gameId;
    }
}
