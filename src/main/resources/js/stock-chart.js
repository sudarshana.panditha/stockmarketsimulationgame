const loadData = d3.json('sample-data.json').then(data => {
  const chartResultsData = data['chart']['result'][0];
  const quoteData = chartResultsData['indicators']['quote'][0];

  return chartResultsData['timestamp'].map((time, index) => ({
    date: new Date(time*0.001),
    close: quoteData['close'][index]
  }));
});

loadData.then(data => {
  initialiseChart(data);
});


const initialiseChart = data => {
  data = data.filter(
    row => row['close'] 
  );

  timeRange = new Date(0, 0, 0);

  // filter out data based on time period
  data = data.filter(row => {
    if (row['date']) {
      return row['date'] >= timeRange;
    }
  });

  const margin = { top: 5, right: 50, bottom: 30, left: 50 };
  const width = 1100 - margin.left - margin.right; 
  const height = 150 - margin.top - margin.bottom; 

  const xMin = d3.min(data, d => {
    return d['date'];
  });

  const xMax = d3.max(data, d => {
    return d['date'];
  });

  const yMin = d3.min(data, d => {
    return d['close'];
  });

  const yMax = d3.max(data, d => {
    return d['close'];
  });

  const xScale = d3
    .scaleTime()
    .domain([xMin, xMax])
    .range([0, width]);

  const yScale = d3
    .scaleLinear()
    .domain([yMin - 5, yMax])
    .range([height, 0]);

  const svg1 = d3.select('#svg1')
    .append('svg')
    .attr('width', width + margin['left'] + margin['right'])
    .attr('height', height + margin['top'] + margin['bottom'])
    .append('g')
    .attr('transform', `translate(${margin['left']}, ${margin['top']})`);

  svg1
    .append('g')
    .attr('id', 'xAxis')
    .attr('transform', `translate(0, ${height})`)
    .call(d3.axisBottom(xScale));

  svg1
    .append('g')
    .attr('id', 'yAxis')
    .attr('transform', `translate(${width}, 0)`)
    .call(d3.axisRight(yScale));

  const line = d3
    .line()
    .x(d => {
      return xScale(d['date']);
    })
    .y(d => {
      return yScale(d['close']);
    });

  svg1
    .append('path')
    .data([data]) 
    .style('fill', 'none')
    .attr('id', 'priceChart')
    .attr('stroke', 'green')
    .attr('stroke-width', '2.9')
    .attr('d', line);


    const svg2 = d3
    .select('#svg2')
    .append('svg')
    .attr('width', width + margin['left'] + margin['right'])
    .attr('height', height + margin['top'] + margin['bottom'])
    .append('g')
    .attr('transform', `translate(${margin['left']}, ${margin['top']})`);

  svg2
    .append('g')
    .attr('id', 'xAxis')
    .attr('transform', `translate(0, ${height})`)
    .call(d3.axisBottom(xScale));

  svg2
    .append('g')
    .attr('id', 'yAxis')
    .attr('transform', `translate(${width}, 0)`)
    .call(d3.axisRight(yScale));

  svg2
    .append('path')
    .data([data]) 
    .style('fill', 'none')
    .attr('id', 'priceChart')
    .attr('stroke', 'green')
    .attr('stroke-width', '2.9')
    .attr('d', line);
    
  const svg3 = d3
    .select('#svg3')
    .append('svg')
    .attr('width', width + margin['left'] + margin['right'])
    .attr('height', height + margin['top'] + margin['bottom'])
    .append('g')
    .attr('transform', `translate(${margin['left']}, ${margin['top']})`);

  svg3
    .append('g')
    .attr('id', 'xAxis')
    .attr('transform', `translate(0, ${height})`)
    .call(d3.axisBottom(xScale));

  svg3
    .append('g')
    .attr('id', 'yAxis')
    .attr('transform', `translate(${width}, 0)`)
    .call(d3.axisRight(yScale));

  svg3
    .append('path')
    .data([data]) 
    .style('fill', 'none')
    .attr('id', 'priceChart')
    .attr('stroke', 'green')
    .attr('stroke-width', '2.9')
    .attr('d', line);

  const svg4 = d3
    .select('#svg4')
    .append('svg')
    .attr('width', width + margin['left'] + margin['right'])
    .attr('height', height + margin['top'] + margin['bottom'])
    .append('g')
    .attr('transform', `translate(${margin['left']}, ${margin['top']})`);

  svg4
    .append('g')
    .attr('id', 'xAxis')
    .attr('transform', `translate(0, ${height})`)
    .call(d3.axisBottom(xScale));

  svg4
    .append('g')
    .attr('id', 'yAxis')
    .attr('transform', `translate(${width}, 0)`)
    .call(d3.axisRight(yScale));

  svg4
    .append('path')
    .data([data]) 
    .style('fill', 'none')
    .attr('id', 'priceChart')
    .attr('stroke', 'green')
    .attr('stroke-width', '2.9')
    .attr('d', line);
};




