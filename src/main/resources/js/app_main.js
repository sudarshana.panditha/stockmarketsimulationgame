
// function to control slider nav bar when width </= 800px
const navSlide = () => {
    const burger = document.querySelector('.burger');
    const nav = document.querySelector('.nav-buttons');
    const navLinks = document.querySelectorAll('.nav-buttons button');
    
    burger.addEventListener('click', () => {
        //toggle nav
        nav.classList.toggle('nav-active');
        
        //animate links
        navLinks.forEach((link, index) => {
            if(link.style.animation){
                link.style.animation = '';
            }
            else {
                link.style.animation=`navLinkFade 0.5s ease forwards ${index / 7 + 0}s`;
            }
        });
        //burger animation
        burger.classList.toggle('toggle');
    });
}


// funtion to control modal behavior
const modalAnimate = () => {
    var login_button = document.querySelector('#login-btn');
    var modal_close  = document.querySelector('.modal-close');
    var modal_bg = document.querySelector('.modal-background');

    login_button.addEventListener('click', function(){
       modal_bg.classList.add('dialog-active'); 
    });

    modal_close.addEventListener('click', function(){
       modal_bg.classList.remove('dialog-active'); 
    });
}

// function for type writer text animation
const typeWriter = () => {
    const texts = ['Human Players.', 'AI Traders.'];
    var count     = 0;
    var index  = 0;
    var currentText = '';
    var letter = '';
    
    (function type() {
        if(count === texts.length){
           count = 0;
       }
        currentText = texts[count];
        letter = currentText.slice(0, ++index);
        
        document.querySelector('.typewriter').textContent =  letter;
        if(letter.length === currentText.length){
          count++;
          index = 0;
        }
        setTimeout(type, 400);
    })();
}

// Functions for the parallax effects
function parallax (element, distance, speed) {
    const item = document.querySelector(element);
    item.style.transform = `translateY(${(distance) * speed}px)`;
}

function parallaxcontroller() {
window.addEventListener('scroll', function(){
    parallax("nav", window.scrollY, 1); 
    parallax(".intro_content", window.scrollY, 1);
    parallax(".intro-video", window.scrollY, 1);
    parallax(".logo-img", window.scrollY, 1);
    parallax(".money-sack", window.scrollY, 1);
    parallax(".notes", window.scrollY, 1.08);
    parallax(".coins", window.scrollY, 1.02);
    parallax(".coins2", window.scrollY, 1.02); 
    parallax(".coins3", window.scrollY, 1.02); 
    parallax(".coins5", window.scrollY, 1.02); 
    parallax(".coin1", window.scrollY, 1.01); 
    parallax(".coin2", window.scrollY, 1.03); 
    parallax(".coin3", window.scrollY, 1.06); 
    parallax(".coin4", window.scrollY, 1.04); 
    parallax(".coin5", window.scrollY, 1.08); 
    parallax(".coin6", window.scrollY, 1.02); 
    parallax(".coin7", window.scrollY, 1.03); 
    parallax(".coin8", window.scrollY, 1.05); 
    parallax(".coin9", window.scrollY, 1.07); 
    parallax(".coin10", window.scrollY, 1.02); 
    parallax(".coin11", window.scrollY, 1.03);
    parallax(".coin12", window.scrollY, 1.04);
    parallax(".coin13", window.scrollY, 1.06);
    parallax(".coin14", window.scrollY, 1.07);
});
}

// distortion effect
var myAnimation = new hoverEffect({
        parent: document.querySelector('.distortion'),
        intensity: 0.3,
        image1: 'img/ai.png',
        image2: 'img/human.jpg',
        displacementImage: 'img/dis.png'
    });


navSlide();
modalAnimate();
typeWriter();
parallaxcontroller();